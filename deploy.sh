#!/bin/bash

#################################################

# General deploy script.  Depends on

# environment-specific script in deploy/ dir

#################################################

set -e



APP_NAME=watnapp

GIT_ROOT=bitbucket.org:watnapahpong

GIT_USER=git

APP_REPO=$GIT_ROOT/$APP_NAME.git



# Detect exactly 1 argument

if (($# == 1)); then

# Include .sh from the deploy folder

DEPLOY_ENV=$1

DEPLOY_FILE=deploy/$DEPLOY_ENV.sh

if [ -f $DEPLOY_FILE ]; then

source $DEPLOY_FILE

else

echo "Could not find deploy file for $DEPLOY_ENV environment,

  it should be located in $DEPLOY_FILE"

exit 1

fi

echo "Deploying $APP_NAME to $DEPLOY_ENV environment."

else

echo "Usage: deploy.sh "

exit 1

fi

CURRENT_DIR=$DEPLOY_PATH/$APP_NAME/current

RELEASE_NAME=`date +"%Y-%m-%d-%H%M%S"`

CURRENT_RELEASE=$DEPLOY_PATH/$APP_NAME/releases/$RELEASE_NAME

# From local machine, get hash of the head of the desired branch

# Required to checkout the branch - is there a better way to do this?

APP_HASH=`git ls-remote $APP_REPO $BRANCH | awk -F "\t" '{print $1}'`

for SERVER in ${DEPLOY_SERVER[@]}

do

echo "Deploying on $SERVER"

RM_ADMIN="mv $CURRENT_RELEASE/bonfire/application/controllers/admin $DEPLOY_PATH/$APP_NAME/admin-`date +%Y%m%d%H%M%S` && find $CURRENT_RELEASE/bonfire/modules -name content.php | xargs -I '{}' rm -f '{}'"
if [ "$NOADMIN" = "YES" ]; then
    RM_ADMIN_CMD="$RM_ADMIN &&"
else
    RM_ADMIN_CMD=""
fi

if [ "$DONOTCOUNT" = "YES" ]; then
    DONOTCOUNT_CMD="sh $CURRENT_DIR/donotcount.sh $CURRENT_DIR &&"
else
    DONOTCOUNT_CMD=""
fi

ssh -v -t $DEPLOY_USER@$SERVER "mkdir -p $DEPLOY_PATH/$APP_NAME/releases &&
            cd $DEPLOY_PATH/$APP_NAME/releases &&
            git clone --depth 2 -q $GIT_USER@$APP_REPO $RELEASE_NAME &&
            cd $RELEASE_NAME &&
            git checkout -q -b deploy $APP_HASH &&
            $RM_ADMIN_CMD
            chmod 777 -R $CURRENT_RELEASE/media &&
            ln -nfs $CURRENT_RELEASE $CURRENT_DIR &&
            $DONOTCOUNT_CMD
            sh $CURRENT_DIR/production.sh $CURRENT_DIR"
done

echo "Finished successfully"
