<?php
  require "./ip_info.php";
  function is_thailand_ip($ip) {
    $val = 0;
    foreach(preg_split('/\./', $ip) as $v) {
      $val = $val << 8;
      $val = $val + intval($v);
    }
    $is_in_thailand = false;
    $ip_mask_list = ip_info();
    foreach($ip_mask_list as $ip_range) {
      if($val >= $ip_range['from'] && $val <= $ip_range['to']) {
        $is_in_thailand = true;
        break;
      }
    }
    return $is_in_thailand;
  }
  header("Content-Type:text/plain"); 
  echo is_thailand_ip($_GET['ip']) ? "YES" : "NO";
?>
