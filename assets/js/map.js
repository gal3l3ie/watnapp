function init_map() {
  var thailand = new google.maps.LatLng(14.01012, 100.82302);        
  
  gmap = new google.maps.Map(document.getElementById('id_gmap'), {
    center: thailand,
    zoom: 6,
    mapTypeId: google.maps.MapTypeId.HYBRID
  });        
  
  layer = new google.maps.FusionTablesLayer({
    query: {
      select: 'Location',
      from: '1VG6U5eP4W4Y0ludGKRnl2cGQsbiiPIdcMSrNIJY'
    },
    suppressInfoWindows: true,
    styles: [
    {
      where: 'Organization = 0 AND Status > 2',
      markerOptions: {
        iconName: "man"
      }
    },
    {
      where: 'Organization = 1 AND Status > 2',
      markerOptions: {
        iconName: "road_shield3"
      }
    },
    {
      where: 'Organization = 0 AND Status < 3',
      markerOptions: {
        iconName: "orange_blank"
      },
    },
    {
      where: 'Organization = 1 AND Status < 3',
      markerOptions: {
        iconName: "schools"
      }
    }        
    ]
  });
  
  layer.setMap(gmap);
    
  google.maps.event.addListener(layer, 'click', function(event) {
    console.log(event.row.Number.value);

    $.ajax({
      url: "/buddha_net/info_content.php?pk="+event.row.Number.value
    }).done(function(response) {
      console.log(response.result);
      $('#myModal').html(response.result);
      $('#myModal').reveal();      
    });
  });
}

