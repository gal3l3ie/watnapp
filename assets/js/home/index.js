(function($) {

  var cssmenu = $("#cssmenu"),
      hires_button = $("#hires_button"),
      textstream = $("#textstream"),
      live_iframe = $("iframe#live_iframe");

  $.getJSON("<?= site_url("home/liveVideoInfo"); ?>", function(data) {
    jwplayer('mediastream').setup({
      id: 'playerID',
      autostart: true,
      width: 640,
      height: 360,
      stretching: 'fill',
      primary: 'flash',
      image: data.imagestream,
      sources: [
        {file: data.html5stream}
      ]});

    if (data.filestream === "livestream.stream") {
      cssmenu.show();
      if (data.ctscan === 1)
        hires_button.show();
    }

    textstream.text(data.textstream);

    var iframe_src = "http://live.watnapp.com/live/iframe.php?c=" + data.cname + "&p=watnapp.com&s=";
    iframe_src += data.livemode === "mobile" ? "mobile" : "livestream";

    live_iframe.attr("src", iframe_src);
  });
})(jQuery);