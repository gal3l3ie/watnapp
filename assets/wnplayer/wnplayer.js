var wnplayer = function(container_id) {
  this.container_id = container_id;
};

wnplayer.prototype.setup = function(config) {
  var ua = navigator.userAgent;
  var ios = /(iPad|iPhone|iPod)/g.test(ua);
  
  var android = ua.indexOf("Android") > -1;
  var androidversion = parseFloat(ua.slice(ua.indexOf("Android")+8)); 
  
  var autostart = config.autostart === undefined ? true : config.autostart;
  var poster = config.poster === undefined ? '' : config.poster;

  var width = config.width;
  var height = config.height;
  
  if (config.width === undefined || config.height === undefined) {
    width = '800';
    height = '600';
  }

  if (android && androidversion <= 4.1) {
    var container = $('#'+this.container_id);
    container.css({
      background: 'url(' + poster + ') no-repeat center center',
      'background-size': 'cover',
      width: width + 'px',
      height: height + 'px',
      position: 'relative'
    });
    
    var wrapper = $('<a/>', { href: config.rtsp }).css({
      opacity: '0.8',
      transition: 'opacity 0.1s linear',
      '-moz-transition': 'opacity 0.1s linear',
      '-webkit-transition': 'opacity 0.1s linear'
    });
        
    wrapper.mousedown(function() {
      $(this).css({
        opacity: '1'
      });      
    });
    
    wrapper.mouseup(function() {
      $(this).css({
        opacity: '0.8'
      });            
    });
    
    $('<img/>', {
      src: config.playbutton      
    }).css({
      position: 'absolute',
      margin: 'auto',
      top: '0',
      left: '0',
      right: '0',
      bottom: '0'
    }).appendTo(wrapper);
    wrapper.appendTo(container);
  } else {
    var container = $('#'+this.container_id);
    var video = $('<video/>', {
        id: 'buddhawajana-video',
        width: '100%',
        height: '100%',
        allowfullscreen: true,
        'class': 'video-js vjs-default-skin',
        controls: true,
        poster: poster
    });    
    $('<source/>', {
        src: config.hls,
        type: 'application/x-mpegURL'
    }).appendTo(video);
    video.appendTo(container);
    var player = videojs('buddhawajana-video');
    if (autostart) {
        player.play();
    }
  }    
};
