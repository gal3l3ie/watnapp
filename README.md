# watnapp

The main web site of Watnapahpong

## Coding standard

### indent
* Use soft tab (space)
* indent width = 2

### Control structure

    if ($a == $b) {
      ...
    } else {
      ...
    }

### Class definition

    class TheNameOfClass 
    {
      ...
    }

### Function definition

    function theNameOfFunction(params) 
    {
      ...
    }

## Todo(s)


## Docker

### MariaDB

````
sudo docker run --name watnadb -e MYSQL_ROOT_PASSWORD=onmodao -v db:/var/lib/mysql -p 3306:3306 -d mariadb:latest --character-set-server=utf8
````

### PHP

````
sudo docker run -d -p 4715:80 -v /path/to/watnapp:/var/www bylexus/apache-php53
````

## docker-compose

TODO
