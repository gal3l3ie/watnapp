#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: `basename $0` <working directory>"
  exit 1
fi

TARGET=$1/bonfire/application/config/production/constants.php

cat $TARGET | sed "s/define('DONOTCOUNT.*/define('DONOTCOUNT', True);/g" > /tmp/index_$$
cp -f /tmp/index_$$ $TARGET && rm -f /tmp/index_$$
