#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: `basename $0` <working directory>"
  exit 1
fi

cat $1/index.php | sed "s/define('ENV.*/define('ENVIRONMENT', 'production');/g" > /tmp/index_$$
cp -f /tmp/index_$$ $1/index.php && rm -f /tmp/index_$$

cat $1/api/environment.php | sed "s/define('ENV.*/define('ENVIRONMENT', 'production');/g" > /tmp/env_$$
cp -f /tmp/env_$$ $1/api/environment.php && rm -f /tmp/env_$$
