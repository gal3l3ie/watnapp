<?php $o=array();



############### GET ###############

$o['GET']=array();

#==== GET category

$o['GET']['category']=array (
	  'class_name' => 'Category',
	  'method_name' => 'get',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);

#==== GET category/:id

$o['GET']['category/:id']=array (
	  'class_name' => 'Category',
	  'method_name' => 'get',
	  'arguments' => 
	  array (
	    'id' => 0,
	  ),
	  'defaults' => 
	  array (
	    0 => NULL,
	  ),
	  'metadata' => 
	  array (
	  ),
	  'method_flag' => 0,
	);
return $o;