<?php

require_once 'db_pdo_mysql.php';

class Category
{
    public $dp;
    public $restler;
    
    function __construct()
    {
        $this->dp = new DB_PDO_MySQL();
    }
    
    function get($id=NULL)
    {
        if (is_null($id)) {
            return $this->dp->getCategories();
        } else {
            return $this->dp->getAudios($id);
        }
    }
}

?>