<?php
require_once 'environment.php';
if(ENVIRONMENT == 'development') {
  require_once 'development.php';
} else {
  require_once 'production.php';
}

class DB_PDO_MySQL
{
    private $db;
    function __construct()
    {
        try {
            $this->db = new PDO(
                'mysql:host=localhost;dbname='.DBNAME.';charset=utf8', DBUSER, DBPASSWORD, 
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());                
        }
    }    
        
    function getCategories()
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = 'SELECT * FROM bf_categories';
            return $this->id2int($this->db->query($sql)->fetchAll());
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());                
        }        
    }
    
    function getAudios($category_id)
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = 'SELECT * FROM bf_songs WHERE category_id = ' . mysql_escape_string($category_id);
            return $this->id2int($this->db->query($sql)->fetchAll());
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());                
        }    
    }
    
    private function id2int ($r)
    {
        if (is_array($r)) {
            if (isset($r['id'])) {
                $r['id'] = intval($r['id']);
            } else {
                foreach ($r as &$r0) {
                    $r0['id'] = intval($r0['id']);
                }
            }
        }
        return $r;
    }    
    
    private function completeAlbumCoverFilePath ($r)
    {
        if (is_array($r)) {
            if (isset($r['album_cover']) && !is_null($r['album_cover'])) {
                $r['album_cover'] = 'http://'.$_SERVER['SERVER_NAME'].'/media/album/cover/' . $r['album_cover'];
            } else {
                foreach ($r as &$r0) {
                    if (!is_null($r0['album_cover'])) {
                        $r0['album_cover'] = 'http://'.$_SERVER['SERVER_NAME'].'/media/album/cover/' . $r0['album_cover'];
                    } else {
                        $r0['album_cover'] = 'http://'.$_SERVER['SERVER_NAME'].'/media/album/cover/000.png';
                    }
                }
            }
        }
        return $r;
    }
}

?>
