<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['home_menu_home']					= 'หน้าหลัก';
$lang['home_menu_video']					= 'สื่อวีดีโอ';
$lang['home_menu_audio']					= 'สื่อเสียง';
$lang['home_menu_book']					= 'หนังสือ';
$lang['home_menu_files']					= 'ดาวน์โหลด';
$lang['home_menu_webboard']			= 'เว็บบอร์ด';
$lang['home_menu_faqs']						= 'คำถามที่พบบ่อย';
$lang['home_menu_contact']				= 'ติดต่อเรา';

$lang['home_search_inputtext']			= 'โปรแกรมตรวจหาและเทียบเคียงพุทธวจน';
$lang['home_search_radiothai']			= 'ไทย (บาลีสยามรัฐ)';
$lang['home_search_radiopali']			= 'บาลี (บาลีสยามรัฐ)';
$lang['home_search_read']					= 'อ่านพระไตรปิฏก';

$lang['home_title']							= 'พุทธวจน: ';
$lang['home_title_bw']					= 'พุทธวจน : วัดนาป่าพง';
$lang['home_title_live']					= 'ถ่ายทอดสดประจำวัน';
$lang['home_title_news']				= 'ข่าวสาร';
$lang['home_title_calendar']			= 'ปฏิทินแสดงธรรม';
$lang['home_title_video']				= 'ดูวีดีโอสื่อธรรมบรรยาย';
$lang['home_title_video_live']				= 'ดูวีดีโอถ่ายทอดสด';
$lang['home_title_book']					= 'สื่อหนังสือ (คลิก เพื่อ อ่าน หรือ ดาวน์โหลด)';
$lang['home_title_audio']				= 'เสียง';

$lang['view_resolution']					= 'ความละเอียดในการรับชม';
$lang['v_res_low']							= 'ต่ำ';
$lang['v_res_medium']					= 'กลาง';
$lang['v_res_high']							= 'สูง';
$lang['view_all']								= 'ดูทั้งหมด';
$lang['view_time']							= 'ครั้ง';
$lang['view_read']							= 'อ่าน';
$lang['view_listen']							= 'ฟัง';
$lang['today']									= 'วันนี้';
$lang['more_detail']						= 'รายละเอียดเพิ่มเติม';
$lang['lastest_news']						= 'ข่าวสารล่าสุด';

$lang['live_button_liveout']			= 'ถ่ายทอดสดนอกสถานที่';
$lang['live_button_ques_sndl']		= 'ส่งคำถามสนทนาธรรมค่ำวันเสาร์';
$lang['live_button_ques_live']		= 'ส่งคำถามสนทนาธรรมประจำวัน(หลังฉัน)';
$lang['live_title']							= 'วัดนาป่าพง';
$lang['live_nightsat']					= 'ถ่ายทอดสดสนทนาธรรมค่ำวันเสาร์';
$lang['live_daily']						= 'ถ่ายทอดสดสนทนาธรรมประจำวัน';
$lang['live_outside']					= 'ถ่ายทอดสดนอกสถานที่';

$lang['stat_title']						= 'ผู้เยี่ยมชม';
$lang['stat_today']						= 'วันนี้';
$lang['stat_yesterday']				= 'เมื่อวานนี้';
$lang['stat_month']						= 'เดือนนี้';
$lang['stat_year']						= 'ปีนี้';
$lang['stat_ipdetail']					= '**IP ไม่ซ้ำกันรายวัน';

$lang['link_landfund_namelist']	= 'รายชื่อเจ้าภาพกองทุนบุญที่ดิน';
$lang['link_fiber']						= 'รายละเอียดบัญชีไฟเบอร์ออฟติก';

$lang['buddha_net']					= 'เครือข่ายพุทธวจน';
$lang['wnpp_aboutus']				= 'เกี่ยวกับวัดนาป่าพง (ภาษาอังกฤษ)';








