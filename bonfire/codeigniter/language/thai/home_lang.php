<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['home_menu_home']					= 'HOME';
$lang['home_menu_video']					= 'VIDEO';
$lang['home_menu_audio']					= 'AUDIO';
$lang['home_menu_book']					= 'BOOKS';
$lang['home_menu_files']					= 'DOWNLOAD';
$lang['home_menu_webboard']			= 'WEBBOARD';
$lang['home_menu_faqs']			= 'FAQ';
$lang['home_menu_contact']				= 'CONTACT';

$lang['home_search_inputtext']			= 'FIND AND COMPARE BUDDHAWAJANA';
$lang['home_search_radiothai']			= 'Thai (Bali Siam)';
$lang['home_search_radiopali']			= 'Bali (Bali Siam)';
$lang['home_search_read']					= 'Read Tipitaka Online';

$lang['home_title']							= 'BUDDHAWAJANA: ';
$lang['home_title_bw']					= 'Budhawajana : Watnapahpong';
$lang['home_title_live']					= 'Live Dhamma Daily';
$lang['home_title_news']				= 'News';
$lang['home_title_calendar']			= 'Calendar';
$lang['home_title_video']				= 'Video';
$lang['home_title_video_live']				= 'Live video';
$lang['home_title_book']					= 'Books';
$lang['home_title_audio']				= 'Audio';

$lang['view_resolution']					= 'View Resolution';
$lang['v_res_low']					= 'LOW';
$lang['v_res_medium']					= 'MEDIUM';
$lang['v_res_high']					= 'HIGH';
$lang['view_all']								= 'View All';
$lang['view_time']							= 'views';
$lang['view_read']							= 'Read';
$lang['view_listen']							= 'Played';
$lang['today']									= 'Today';
$lang['more_detail']						= 'More details';
$lang['lastest_news']						= 'Lastest news';

$lang['live_button_liveout']			= 'LIVE DHAMMA - OFF SITE';
$lang['live_button_ques_sndl']		= 'Send question for Saturday Night (Live)';
$lang['live_button_ques_live']		= 'Send question for Dhamma Daily (Live)';
$lang['live_title']							= 'Watnapahpong';
$lang['live_nightsat']					= 'Live Talk on Saturday evening.';
$lang['live_daily']						= 'Live Discussion of the day.';
$lang['live_outside']					= 'Live Outside.';

$lang['stat_title']						= 'VISITORS';
$lang['stat_today']						= 'Today';
$lang['stat_yesterday']				= 'Yesterday';
$lang['stat_month']						= 'This month';
$lang['stat_year']						= 'This year';
$lang['stat_ipdetail']					= '**IP  daily deduplicated';

$lang['link_landfund_namelist']	= 'DONOR LIST : LAND PURCHASE PROJECT';
$lang['link_fiber']						= 'Donation for cost of Fiber Optic Lease Line';

$lang['buddha_net']					= 'Buddhawajana Network';
$lang['wnpp_aboutus']				= 'Watnapahpong (About us)';












