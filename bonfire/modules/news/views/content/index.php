<div id="content">
  <div class="paging-box float-right">หน้า 
    <?= $this->pagination->create_links(); ?>
  </div>
  <ul>
    <?php foreach ($news_list as $news): ?>
    <li><?= $news->id; ?> 
        <?= $news->title; ?> 
        <?php if ($news->status == "เปิด"): ?>
          <a 
              href="<?= site_url("/admin/content/news/hide/".$news->id); ?>"
              >[Hide]</a></li>
        <?php else: ?>
          <a 
              href="<?= site_url("/admin/content/news/show/".$news->id); ?>"
              >[Show]</a></li>
        <?php endif; ?>
    <?php endforeach; ?>
  </ul>
</div>
