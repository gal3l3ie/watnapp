﻿<?php
class Content extends Admin_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');
		$this->auth->restrict('Permissions.Audi-file.Manage');
		Template::set('toolbar_title', 'News');	
    Assets::add_css("v");
	}

  public function index() {
    $news = new Wpnew();
    $this->load->library('pagination');

    $config = array();
    $config['base_url'] = site_url('/admin/content/news/index/');
    $config['total_rows'] = $news->count();
    $config['per_page'] = 100; 
    $config['uri_segment'] = 5;

    $this->pagination->initialize($config); 

    Template::set('news_list',
                  $news->order_by('id','desc')
                    ->get($config['per_page'], 
                          $this->uri->segment(5)));
    Template::render();
  }

  public function hide() {
    $id = $this->uri->segment(5);
    $news = new Wpnew($id);
    $news->status = "ปิด";
    $news->save();
    Template::redirect('/admin/content/news');
  }

  public function show() {
    $id = $this->uri->segment(5);
    $news = new Wpnew($id);
    $news->status = "เปิด";
    $news->save();
    Template::redirect('/admin/content/news');
  }

}
