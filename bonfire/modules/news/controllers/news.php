<?php 
class News extends Front_Controller {
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Bangkok');        
  }
  
	function index() 
	{
    Template::render();        
	}
		
	public function items() 
	{
	  $s = 0;
	  $e = 10;
	  if(FALSE !== $this->uri->segment(3)) {
	    $s = (int)$this->uri->segment(3);
	  }
	  if(FALSE !== $this->uri->segment(4)) {
	    $e = (int)$this->uri->segment(4);
	  }
	  $wpns = new wpnew();	  
	  $numFound = $wpns->where("status","เปิด")->count();        
    $wpns = $wpns->where("status","เปิด")->order_by('id','desc')->offset($s)->limit($e - $s)->get();
	  $shown_news_count = 0;
	  $news_list = array();
    foreach ($wpns as $wpn):
      $pic_url = $wpn->pic; 
      if(!isset($pic_url)) {
        $pic_url = site_url('assets/images/news-watnapp.jpg');  
      }
      $link_url = $wpn->url;
      $msg = $wpn->title;
      $news = array("msg" => $msg,
                    "pic_url" => $pic_url,
                    "link_url" => $link_url);
      $news_list[] = $news;
    endforeach;    
    Template::set("news_list", $news_list);
    Template::set("numFound", $numFound);
    Template::set("s", $s);
    Template::set("e", $e);
    Template::render("json");
	}
}
?>
