<?php 
class Book extends Front_Controller {
  function index()
  {
    $file = "book";
    $medias = new Media();
    Template::set('medias', $medias->where('type', $file)->where("visibility", "show")->get());
    Template::set('files_id',$file);
    Template::render();
  }
  
  function count() 
  {
    if ($this->uri->segment(3)) {
      $media_id = $this->uri->segment(3);
      $medias = new Media();
      $m = $medias->where('id',$media_id)->get(1);
      $m->count += 1;
      $m->save();
      echo "DONE $media_id";
    }
  }
  
  function download()
  {
    $medias = new Media();
    
    if ($this->uri->segment(4) == FALSE){
      if ($this->uri->segment(3) == FALSE){
         redirect('book');
      }else{
          $media_id = $this->uri->segment(3);
      }
    }else{
          $media_id = $this->uri->segment(4);
    }
      
    $m = $medias->where('id',$media_id)->get(1);
    if($m->link){
      Template::set ('refresh',$m->link);
    }else if($m->file){
      Template::set ('refresh','/media/'.$m->file);
      }else{
      redirect('book');
    }
    $m->count += 1;
    $m->save();
        
    Template::set ('m',$m);
    Template::set ('title','พุทธวจน ดาวน์โหลด รอสักครู่');
    Template::render('redirect');
    
  }
}
