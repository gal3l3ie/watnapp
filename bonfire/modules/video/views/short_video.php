<h2><?=lang('home_title')?><?=lang('short_video')?></h2>

<ul id="short_video">
</ul>

<div class="clear"></div>

<script>loadVideoList("<?= site_url("home/shortVideoInfo"); ?>", $("ul#short_video"));</script>
