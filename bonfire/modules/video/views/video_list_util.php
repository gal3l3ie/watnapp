<script id="video_template" type="text/html">
    <li class="colItem">
		<a href="{watch}"><img src="{thumbnail}" class="videoThumbnail"/></a>
		<br>
		<a href="{watch}"">{title}</a>
	</li>
</script>

<script>
  // https://github.com/trix/nano
  function nano(template, data) {
    return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
      var keys = key.split("."), v = data[keys.shift()];
      for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
      return (typeof v !== "undefined" && v !== null) ? v : "";
    });
  }

  function loadVideoList(url, parent) {
    var template = $("script#video_template").html();
    jQuery.getJSON(url, function(data) {
        for( var i in data.entries ) {
          var entry = data.entries[i];
          entry.watch = entry.watch;
          entry.viewCount = entry.viewCount;
          entry.thumbnail = entry.thumbnail;
          entry.title = entry.title;
          var html = nano(template, entry);
          parent.append(html);
        };
    });
  }
</script>
