<div class="videoBox">
	<div class="pure-g">
		<div class="pure-u-1 pure-u-lg-4-5">
			<?php require "video_list_util.php"; ?>
			<?php require "morning_video.php"; ?>
			<?php require "outside_video.php"; ?>
			<?php require "short_video.php"; ?>			
		</div>
		<div class="pure-u-1 pure-u-lg-1-5">
			<?php require "right.php"; ?>
		</div>
	</div>
</div>

