<script>
  $(document).ready(function() {
			var date = new Date();
			var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
				             
			$("#tf_date").datepicker({
    			dateFormat: 'yy-mm-dd',
   				minDate: new Date(2012, 1-1, 1),
				maxDate: new Date(y, m, d),
				beforeShowDay: enableSaturday,
				onSelect: function(dateText, inst) { 
	   		$('#showstream').load('<?php echo site_url('video/streaming/sndl/') ?>/'+ dateText);
}
});
// Custom function to enable friday only in jquery calender
function enableSaturday(date) {
    var day = date.getDay();
    return [(day == 6), ''];
}

	var daysToDisable = [<?php  foreach($dategroup as $dg) echo $dg; ?>];

    $("#tf_date_mobile").datepicker({
			beforeShowDay: disableSpecificDates,
			minDate: new Date(2012, 1-1, 1),
			maxDate: new Date(y, m, d),
			dateFormat: 'yy-mm-dd',
			onSelect: function(dateText, inst) { 
	   		$('#showstream').load('<?php echo site_url('video/streaming/mobile/') ?>/'+ dateText);
}
			});
	
           function disableSpecificDates(date) {
                var month = date.getMonth();
                var day = date.getDate();
                var year = date.getFullYear();
                for (i = 0; i < daysToDisable.length; i++) {
                    if ($.inArray((month + 1) + '-' + day + '-' + year, daysToDisable) != -1) {
                        return [true];
                    }
                }
                return [false];
            }
  });
  
  
  </script>

