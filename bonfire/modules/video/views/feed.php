<?php
function show_feed($feedURL) 
{
    // read feed into SimpleXML object
    $sxml = simplexml_load_file($feedURL);
	
    foreach ($sxml->entry as $entry) {
        // get nodes in media: namespace for media information
        $media = $entry->children('http://search.yahoo.com/mrss/');
        
        // get video player URL
        $attrs = $media->group->player->attributes();
        $watch = $attrs['url']; 
        
        // get video thumbnail
        $attrs = $media->group->thumbnail[0]->attributes();
        $thumbnail = $attrs['url']; 
        
        // get <yt:duration> node for video length
        $yt = $media->children('http://gdata.youtube.com/schemas/2007');
        $attrs = $yt->duration->attributes();
        $length = $attrs['seconds']; 
        
        // get <yt:stats> node for viewer statistics
        $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
        $attrs = $yt->statistics->attributes();
        $viewCount = $attrs['viewCount']; 
      
        // get <gd:rating> node for video ratings
        
        $gd = $entry->children('http://schemas.google.com/g/2005'); 
        if ($gd->rating) {
            $attrs = $gd->rating->attributes();
            $rating = $attrs['average']; 
        } else {
            $rating = 0; 
        } 
        
		$listyt[] = '<ul class="columns">
          <li>
            <div class="yt_thumb_img">
				'.anchor($watch,img($thumbnail), array('target' => '_blank')).'
			 </div>
          </li>
          <div class="clear"></div>
        </ul>
		 <div class="browse-item-content">
        <h3 dir="ltr">
 '.anchor($watch, $media->group->title, array('target' => '_blank')).'
  </h3>
         <span class="browse-item-info">
		<span class="viewcount">
        '.$viewCount.' ครั้ง
        </span></span>

      	</div>';
    }//
    echo ul($listyt);
}
?>
