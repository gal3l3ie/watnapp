<?php 
class Video extends Front_Controller {
    public function __construct()
	{
		parent::__construct();
		
				Template::set('livemode','video');

	}

	function index()
	{
		$wpl = new Wplive ();
		Template::set('livemobile',$wpl->where('filestream !=','')->get());
		Template::render();
	}
	
    function ytlive()
	{
        header("Location: ". YOUTUBE_LIVE_URL);
        die();
	}

    function live()
	{
        $ida = $this->uri->segment(3);
        $idb = $this->uri->segment(4);
        
        if ($ida == "mobile"){
            $wpl = new Wplive ();
            $match = date($idb);
            $t = $wpl->like('time_live_start', $match)->get(1);
            $filestream = "mp4:".$t->filestream;
        }else{
        $filestream = "mp4:".$ida."_".$idb.".mp4";
        }
        Template::set('filestream',$filestream);
        Template::render();
	}
	
    function streaming()
	{			
        $durastream = $this->uri->segment(3);
	    $datestream = $this->uri->segment(4);
				
		$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");  
		$thai_month_arr=array("0"=>"", "1"=>"มกราคม", "2"=>"กุมภาพันธ์", "3"=>"มีนาคม", "4"=>"เมษายน", "5"=>"พฤษภาคม", "6"=>"มิถุนายน", 
		"7"=>"กรกฎาคม", "8"=>"สิงหาคม", "9"=>"กันยายน", "10"=>"ตุลาคม", "11"=>"พฤศจิกายน", "12"=>"ธันวาคม");
		$str_dmy = strtotime($datestream);
		$str_d = date("j",$str_dmy);
		$str_m = $thai_month_arr[date("n",$str_dmy)];
		$str_y = date("Y",$str_dmy)+543;
		$strdates_th =$str_d." ".$str_m." ".$str_y;
		$strdates = date("Ymd",strtotime($datestream));
		$filestream = "0";
				
		if ($durastream == "mobile"){
		    $wpl = new Wplive ();
			$match = date($datestream);
			$tlstart = $wpl->like('time_live_start', $match)->get(1);
			foreach ($tlstart as $t){
			    $filestream = "mp4:".$t->filestream;
		    }
	    } else {
		    $filestream = "mp4:".$durastream."_".$strdates.".mp4";
	    }
				
		$imagestream = "";
				
	
		if ($durastream == "sndl") {
		    $duratext = "วีดีโอสนทนาธรรมค่ำวันเสาร์ ".$strdates_th;
		} else if($durastream == "mobile") {
            $duratext = "วีดีโอพุทธวจนบรรยายนอกสถานที่ ".$strdates_th;
        } else {
		    $duratext = "ไม่มีรายการ";
	    }
	    Template::set('filestream',$filestream);
		Template::set('imagestream',$imagestream);
		Template::set('duratext',$duratext);
		Template::render('blank');
	}
	
	function lives()
	{
	    Template::render();	
	}
}
