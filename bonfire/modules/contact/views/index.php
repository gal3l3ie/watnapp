<div id="contactBox">
	<h2>พุทธวจน : ติดตามการเผยแผ่พระธรรมคำสอนตามหลักพุทธวจน โดย พระอาจารย์คึกฤทธิ์ โสตฺถิผโล</h2>
    <?="<center><img src='/images/map.jpg' border='0'></center>";?>


	<div class="pure-g">
		<div class="pure-u-1 pure-u-lg-1-2">
			<h3> เว็บไซต์</h3>
			<ul>
				<li>http://www.watnapp.com : หนังสือ และสื่อธรรมะ บนอินเทอร์เน็ต</li>
				<li>http://media.watnapahpong.org : ศูนย์บริการมัลติมีเดียวัดนาป่าพง</li>
				<li> http://www.buddhakos.org : มูลนิธิพุทธโฆษณ์</li>
				<li>http://etipitaka.com : โปรแกรมตรวจหาและเทียบเคียงพุทธวจน</li>
				<li>http://www.watnapahpong.net : แกลอรี่รูปภาพ</li>
				<li>http://www.watnapahpong.org : เว็บไซต์วัดนาป่าพง</li>
				<li>http://www.buddha-ins.org : สถาบันพุทธวจน</li>
				<li>http://www.watnapahpong.com : เว็บไซต์วัดนาป่าพง</li>
				<li>http://www.buddhawaj.org : ฐานข้อมูลพระสูตรออนไลน์, เสียงอ่านพุทธวจน</li>
				<li>http://www.buddhaoat.org : กลุ่มผู้สนับสนุนการเผยแผ่พุทธวจน</li>
			</ul>
			
			<h3>วิทยุ-โทรทัศน์</h3>			
			<ul>
				<li>คลื่น ส.ว.พ. FM ๙๑.๐ MHz ทุกวันพระ เวลา ๑๗.๔๐ น.</li>
			</ul>

		</div>
		<div class="pure-u-1 pure-u-lg-1-2">
			<h3>ดาวน์โหลดโปรแกรมตรวจหาและเทียบเคียงพุทธวจน (E-Tipitaka)</h3>
			<h4>สำหรับคอมพิวเตอร์</h4>
			<ul>
				<li>http://etipitaka.com/download หรือ รับแผ่นโปรแกรมได้ที่วัดนาป่าพง</li>
				<li>ระบบปฏิบัติการ Windows, Macintosh, Linux</li>
			</ul>
			
			<h4>สำหรับโทรศัพท์เคลื่อนที่</h4>
			<ul>
				<li>ระบบปฏิบัติการ Android</li>
				<li>ดาวน์โหลดได้ที่ Android Market โดยพิมพ์คำว่า พุทธวจน หรือ e-tipitaka</li>
				<li>ดาวน์โหลดได้ที่ App Store โดยพิมพ์คำว่า พุทธวจน หรือ e-tipitaka</li>
			</ul>

			<br>
			
			<h3>ดาวน์โหลดโปรแกรมพุทธวจน (Buddhawajana)</h3>
			
			<ul>
				<li>เฉพาะระบบปฏิบัติการ iOS (สำหรับ iPad, iPhone, iPod)</li>
				<li>ดาวน์โหลดได้ที่ App Store โดยพิมพ์คำว่า พุทธวจน หรือ buddhawajana</li>
			</ul>   
			

		</div>
	</div>

	
</div>

