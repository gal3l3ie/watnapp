<div id="newsbox">
	<h2><?= lang('home_title') ?> <?= lang('home_title_news') ?></h2>
	ส่งข่าวได้ที่ <a href="mailto:buddhawajana.news@gmail.com">buddhawajana.news@gmail.com</a>

	<div id="news_container">
		<table>
		</table>
	</div>

	<div style="text-align:center">
		<a class="pure-button" href="<?= site_url("news");?>"><?= lang('view_all'); ?></a>
	</div>

</div>

<script type="text/html" id="tweet_tmpl">
	<tr>
		<td>
		<a href="{link_url}">
			<img class="newspic" src="{pic_url}" alt="Budhawajana : Watnapahpong live" border="0">
		</a>
		</td>
		<td>
			{msg}
			<a href="{link_url}"><?=lang("more_detail");?>
		</td>
	</tr>
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>

<script>
  // https://github.com/trix/nano
  function nano(template, data) {
    return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
      var keys = key.split("."), v = data[keys.shift()];
      for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
      return (typeof v !== "undefined" && v !== null) ? v : "";
    });
  }

  var parent = $("#news_container table");
  var template = $("script#tweet_tmpl").html();
  var step = 20;
  var e = step;
  var numFound = 0;
  jQuery.getJSON("<?=site_url("news/items/0");?>" + "/" + e, function(data) {
    numFound = data.numFound;
    _.forEach(data.newsList, function(news_item) {
      var html = nano(template, news_item);
      parent.append(html);          
    });
  });
  
  function getMore(s_, e_) {
    if(e_ < numFound) {
      jQuery.getJSON("<?=site_url("news/items");?>" + "/" + s_ + "/" + e_, function(data) {
        e = e_;
        numFound = data.numFound;
        _.forEach(data.newsList, function(news_item) {
          var html = nano(template, news_item);
          parent.append(html);
        });
      });
    }
  }
  
  jQuery("#news_container").scroll(function(evt) {
    var myDiv = evt.target;
    if (myDiv.offsetHeight + myDiv.scrollTop >= myDiv.scrollHeight) {
      getMore(e, e + step);
    }
  });
</script>
