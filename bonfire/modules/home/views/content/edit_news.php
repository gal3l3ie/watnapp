<?php echo form_open_multipart(site_url('admin/content/home/add_news/'.$wpn->id), 'name="frm" class="constrained"'); ?>

<div>
    <h2>เพิ่มข่าวสารใหม่</h2><br />
    <br/>
    <?php
        echo form_label ('สถานะ','status');
        $status = array ('เปิด','ปิด');
        foreach ($status as $st){
            if ($st == $wpn->status){
                echo form_radio('status', $st,TRUE);
                echo $st;
            } else {
                echo form_radio('status', $st);
                echo $st;								
            }
        }
        
        echo br();

        echo form_label ('โหมดในการโชว์','mode');
        $mode = array ('0','1');
        foreach ($mode as $md) {
            if ($md == $wpn->mode) {
                echo form_radio('mode', $md,TRUE);
                if ($md == 1){
                    echo "แบบรูปภาพสไลด์เดอร์";
                } else {
                    echo "แบบข้อความตัวอักษร";
                }
            } else {
                echo form_radio('mode', $md);
                if ($md == 1) {
                    echo "แบบรูปภาพสไลด์เดอร์";
                } else {
                    echo "แบบข้อความตัวอักษร";
                }
            }
        }

        echo br();
						
        $data = array('name' => 'title', 'id' => 'title', 'value' => $wpn->title, 'maxlength' => '300');
        echo form_label ('หัวข้อข่าว','title');
        echo form_input($data);
        echo br();
                                
        $data = array('name' => 'description', 'id' => 'description', 'value' => $wpn->description, 'rows' => '3', 'maxlength' => '500');
        echo form_label ('รายละเอียด (ถ้ามี)','description');
        echo form_textarea($data);
        echo br();
                            
        $data = array('name' => 'url', 'id' => 'url', 'value' => $wpn->url, 'maxlength' => '1000');
        echo form_label ('ลิ๊งค์หัวข้อข่าว','url');
        echo form_input($data);

        echo br();

        echo form_label('อัพโหลดภาพสไลด์เดอร์','pic');
        echo form_upload('pic');
        echo "หากเลือกโหมดสไลด์เดอร์นี้ ถ้ายังไม่เคยอัพ ให้อัพภาพขนาด 960 x 360px เท่านั้น";
    ?>
    <br/>
    <?php if(!is_null($wpn->pic)): ?>
        <img src="<?= $wpn->pic; ?>?random=<?= rand(100000000000, 1000000000000); ?>"/>
    <?php else: ?>
        <span>No PIC</span>
    <?php endif; ?>
    <p class="small indent"></p>
</div>
			
<div class="submits">
    <br/>
    <input type="submit" name="submit" value="SAVE" />
</div>
			
<?php echo form_close(); ?>
