<style>
.list-view .permission_set {cursor: pointer; border-bottom: 1px solid #CCCCCC; background: #F5F5F5; vertical-align: middle; }
.list-view .permission_set img { height: 10px; width: 10px; float: right; margin: 14px 14px 0 0; }
</style>


<div class="view split-view">
	
	<!-- Role List -->
	<div class="view">	
		<div class="scrollable">
			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        ข่าวสารหน้าหลัก
					</h4>
					
					<div class="list-item" data-id="create_news"> 
						<p>
							<b>เพิ่มข่าวสารใหม่</b><br/> 
							<span class="small">**ไม่จำเป็นให้ใช้แก้ไข และเปิดสถานะแทน หรือถ้าเพิ่มใหม่ ควรเข้าไปปิดสถานะข่าวสารที่ไม่จำเป็นออก เพื่อมิให้ข่าวสารมากจนเกินไป</span>
						</p>
					</div>

                    <div class="list-item" data-id="breaking_news"> 
						<p>
							<b>แก้ไขข่าวสารใหม่</b><br/>
							<span class="small">สามารถเปิดและปิดสถานะได้</span>
						</p>
					</div>
                    			</div>	<!-- /list-view -->
                    
                    <div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        ถ่ายทอดสดนอกสถานที่
					</h4>
					
					<div class="list-item" data-id="create_mobile_live"> 
						<p>
							<b>เพิ่มรายการถ่ายทอดสดนอกสถานที่ใหม่</b><br/>
							<span class="small">ระบุวันที่ เวลาเริ่มต้น และสิ้นสุดการถ่ายทอดสด นอกสถานที่ เพื่อแสดงรายละเอียดและเปลี่ยนจอภาพ ที่หน้าเว็บวันและเวลาดังกล่าว</span>
						</p>
					</div>
                    <div class="list-item" data-id="mobile_live"> 
						<p>
							<b>แก้ไขถ่ายทอดสดนอกสถานที่</b><br/>
							<span class="small"></span>
						</p>
					</div>
               

			</div>	<!-- /list-view -->
            
            		
		</div>
	</div>
	
	<!-- Role Editor -->
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
				
			<div class="box create rounded">
				<!--a class="button good ajaxify" href="<?php echo site_url(SITE_AREA .'/content/home/create_news'); ?>">เพิ่มข่าวสารใหม่</a>-->

				<h3>หน้าหลัก</h3>

				<p>สำหรับหน้าแรก เว็บไซต์ watnapp.com</p>
			</div>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
