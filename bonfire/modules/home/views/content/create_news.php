<script>
$(document).ready(function() {
    $('#time_live_start').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm:ss',});
});

$(document).ready(function() {
    $("#time_live_stop").datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm:ss',});
});
</script>

<?php echo form_open_multipart(site_url('admin/content/home/add_news'), 'name="frm" class="constrained"'); ?>
<div>
    <h2>เพิ่มข่าวสารใหม่</h2><br />
    <br/>
    <?php
        echo form_label ('สถานะ','status');
        echo form_radio('status', 'เปิด',TRUE);
        echo 'เปิด';
        echo form_radio('status', 'ปิด');
        echo 'ปิด';								
        
        echo br();

        echo form_label ('โหมดในการโชว์','mode');
        echo form_radio('mode', '0',TRUE);
        echo 'แบบข้อความตัวอักษร';
        echo form_radio('mode', '1');
        echo 'แบบรูปภาพสไลด์เดอร์';								

        echo br();

        $data = array('name' => 'title', 'id' => 'title', 'value' => '', 'maxlength' => '300');
        echo form_label ('หัวข้อข่าว','title');
        echo form_input($data);

        echo br();
					
        $data = array('name' => 'description', 'id' => 'description', 'value' => '', 'rows' => '3', 'maxlength' => '500');
        echo form_label ('รายละเอียด (ถ้ามี)','description');
        echo form_textarea($data);

        echo br();
					
        $data = array('name' => 'url', 'id' => 'url', 'value' => '', 'maxlength' => '1000');
        echo form_label ('ลิ๊งค์หัวข้อข่าว (ถ้ามี)','url');
        echo form_input($data);

        echo br();

        echo form_label('อัพโหลดภาพสไลด์เดอร์','pic');
        echo form_upload('pic');
        echo "หากเลือกโหมดสไลด์เดอร์นี้ ให้อัพภาพขนาด 960 x 360px เท่านั้น";
    ?>
    
    <p class="small indent"></p>
</div>
			
<div class="submits">
    <br/>
    <input type="submit" name="submit" value="SAVE" />
</div>
			
<?php echo form_close(); ?>
