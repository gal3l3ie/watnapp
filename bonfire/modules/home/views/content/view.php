<style>
#flex_table { margin: 0; }
#flex_table th { font-weight: bold; }
#flex_table th.sorting_desc, #flex_table th.sorting_asc { background-color: #F5F5F5;}
</style>

<div class="scrollable" id="ajax-content">
  <script>
  $(document).ready(function() {
    $("#tf_date").datepicker({ dateFormat: 'yy-mm-dd' });
  });
  </script>

	<div id="user_activities">
		<table id="flex_table">
			<thead>
				<tr>
					<th>ลำดับ</th>
					<th>หัวข้อ</th>
					<th>วันที่บันทึก</th>
					<th>ดำเนินการ</th>
				</tr>
			</thead>
			
			<tfoot></tfoot>
			
			<tbody>
				<?php foreach ($activity_content as $activity) : ?>
				<tr>
					<td><?php echo $activity->id; ?></td>
					<td><?php echo $activity->title; ?></td>
					<td><?php echo $activity->created; ?></td>
					<td><?php
					if ($check == "mobile_live"){
			echo '<a href="'.site_url(SITE_AREA .'/content/home/edit_mobile_live/'.$activity->id).'" class="ajaxify">[แก้ไข]</a>';
					}else if ($check == "breaking_news"){
			echo '<a href="'.site_url(SITE_AREA .'/content/home/edit_news/'.$activity->id).'" class="ajaxify">[แก้ไข]</a>';
			}
					?>
                    </td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>