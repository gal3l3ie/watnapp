<div class="videobox">
    <h2><?= lang('home_title') ?> <?= lang('short_video') ?></h2>

	<ul id="short_video">
	</ul>

	<div class="clear"></div>

	<div style="text-align:center">
        <a class="pure-button" href="<?= site_url("video");?>"><?= lang('view_all'); ?></a>
	</div>

	<script>loadVideoList("<?= site_url("home/shortVideoInfo"); ?>", $("ul#short_video"));</script>  
</div>
