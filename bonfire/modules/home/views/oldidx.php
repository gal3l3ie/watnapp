﻿<script>
function loadCSS_(filename){ 
  var file = document.createElement("link");
  file.setAttribute("rel", "stylesheet");
  file.setAttribute("type", "text/css");
  file.setAttribute("href", filename);

  if (typeof file != "undefined")
     document.getElementsByTagName("head")[0].appendChild(file);
}
<?php $min = ENVIRONMENT == "production" ? "-min" : ""; ?>
loadCSS_("<?= site_url("/scripts/nivo-slider/themes/light/light$min.css"); ?>");
loadCSS_("<?= site_url("/scripts/nivo-slider/nivo-slider$min.css"); ?>");
</script>

<div id="content_wraper_slider">
  <div class="slider-wrapper theme-light">
	  <div id="slider" class="nivoSlider">
			<!-- Top banners -->
			<a href="http://download.watnapahpong.org/data/banner/MEE_pano.jpg"><img src="http://download.watnapahpong.org/data/banner/bbwpano2015.jpg"></a>
            <a href="http://download.watnapahpong.org/data/banner/bbw2014.jpg"><img src="http://download.watnapahpong.org/data/banner/bbw2014s.jpg"></a>
			<a href="http://download.watnapahpong.org/data/banner/nongpano2015.jpg"><img src="http://download.watnapahpong.org/data/banner/nongpano2015small.jpg"></a>
			<img src="http://download.watnapahpong.org/data/banner/watna_event_banner.jpg">
			<img src="http://download.watnapahpong.org/data/banner/timeline_palicanon.jpg">
			<img src="http://download.watnapahpong.org/data/banner/chant.jpg">
			<img src="http://download.watnapahpong.org/data/banner/watnatab.jpg">
		</div>
	</div>
</div>

<?php require "stat_count.php" ?>
<div id="left-content" class="float-left">
  <div class="left_content_middle">
    <?php require "title_live.php"; ?>
    <div class="other_display_box">
      <div class="stream_online">
        <?php require "stream.php"; ?>

        <br/><br/>
        
        <?php require "banner_below_stream.php"; ?>

        <br/><br/>

      </div>
    </div>
  </div>

  <div class="left_content_middle">
    <?php require "news.php"; ?>
    <?php require "cal.php"; ?>
    <?php require "buddha-net.php"; ?>
    <?php require "video_list_util.php"; ?>

    <?php require "video_morning.php"; ?>
    <div class="clear"></div>

    <?php require "video_saturday.php"; ?>
    <div class="clear"></div>

    <?php require "video_outside.php"; ?>
    <div class="clear"></div>

    <?php require "video_short.php"; ?>
    <div class="clear"></div>

    <?php require "book.php"; ?>
    <div class="clear"></div>

    <?php require "audio.php"; ?>
  </div>
</div><!--end ilc-->

<?php require "right.php"; ?>
<div class="clear">        
</div><!--cc-->

<script type='text/javascript' src="<?php echo site_url('/jwplayer/jwplayer.js') ?>"></script>
<script>jwplayer.key="VLpObflx0eD07Fi8VMCjMVjV16rLqbhlatz1ExnmPL4=";</script>

<script>
(function($) {

  var cssmenu = $("#cssmenu"),
      hires_button = $("#hires_button"),
      textstream = $("#textstream"),
      live_iframe = $("iframe#live_iframe");

  function useJwplayer(data) {
    jwplayer('mediastream').setup({
      id: 'playerID',
      autostart: true,
      width: 640,
      height: 360,
      stretching: 'fill',
      primary: 'flash',
      image: data.imagestream,
      sources: [
        {file: data.html5stream}
      ]});

    if (data.filestream === "livestream.stream") {
      cssmenu.show();
      //if (data.ctscan === 1)
      //  hires_button.show();
    }
  }

  function useLink(data) {
    var container = document.querySelector("#mediastream");
    var link = data.html5stream;
    var button = document.createElement("button");
    button.onclick = function() {
      window.location = link;
    }
    var h1 = document.createElement("h1");
    h1.appendChild(document.createTextNode("คลิกเพื่อรับชมการถ่ายทอดสด"));
    button.appendChild(h1);
    container.appendChild(button);
  }

  $.getJSON("<?= site_url("home/liveVideoInfo"); ?>", function(data) {
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if (isAndroid) {
      useLink(data);
    } else {
      useJwplayer(data);
    }
    textstream.text(data.textstream);

    var iframe_src = "http://live.watnapp.com/live/iframe.php?c=" + data.cname + "&p=watnapp.com&s=";
    iframe_src += data.livemode === "mobile" ? "mobile" : "livestream";

    live_iframe.attr("src", iframe_src);
  });

  $.getJSON(<?= json_encode(site_url("youtube/index")); ?>, function(data) {
    if (data) {
      var youtubeButton = document.querySelector("a#youtube");
      youtubeButton.setAttribute("style", "display: inline");
      youtubeButton.setAttribute("href", data.link);
    }
  }); 
})(jQuery);
</script>
