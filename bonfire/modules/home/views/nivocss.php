<script>
function loadCSS_(filename){ 
  var file = document.createElement("link");
  file.setAttribute("rel", "stylesheet");
  file.setAttribute("type", "text/css");
  file.setAttribute("href", filename);

  if (typeof file != "undefined")
     document.getElementsByTagName("head")[0].appendChild(file);
}
<?php $min = ENVIRONMENT == "production" ? "-min" : ""; ?>
loadCSS_("<?= site_url("/scripts/nivo-slider/themes/light/light$min.css"); ?>");
loadCSS_("<?= site_url("/scripts/nivo-slider/nivo-slider$min.css"); ?>");
</script>
