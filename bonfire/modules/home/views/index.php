﻿<?php require "slide.php"; ?>
<?php require "stat_count.php"; ?>

<div class="pure-g">
	<div class="pure-u-1 pure-u-lg-18-24">
		<div class="stream_online">
			<?php require "title_live.php"; ?>
			<?php require "stream.php"; ?>
			<?php // require "banner_below_stream.php"; ?>
		</div>
                <div id="youtubestream">
                    
                </div>    
        <?php require "radio.php"; ?>
		<?php //require "news.php"; ?>
		<?php //require "cal.php";*/ ?>
		<?php require "buddha-net.php"; ?>
		<?php require "video_list_util.php"; ?>
		<?php require "video_morning.php"; ?>
		<?php require "video_outside.php"; ?>
		<?php require "video_short.php"; ?>
		<?php require "book.php"; ?>
		<?php require "audio.php"; ?>
        <div style="margin: 1em;"><h2>&nbsp</h2></div>
	</div>
	<div class="pure-u-1 pure-u-lg-6-24">
		<?php require "right.php"; ?>
	</div>
</div>
<link href="<?= "/assets/wnplayer/video-js.css" ?>" rel="stylesheet" />
<script src="<?= "/assets/wnplayer/video.js" ?>"></script>
<script src="<?= "/assets/wnplayer/videojs-contrib-hls.js" ?>"></script>
<script src="<?= "/assets/wnplayer/wnplayer.js" ?>"></script>

<script>
 (function($) {

     var cssmenu = $("#cssmenu"),
         hires_button = $("#hires_button"),
         textstream = $("#textstream"),
         live_iframe = $("iframe#live_iframe");

     function usePlayer(data) {
         var player = new wnplayer('mediastream');
         player.setup({
             width:     '640',    
             height:    '360',
             autostart:  true,
             poster:    'http://download.watnapahpong.org/data/static/broadcast_info.jpg',
             hls:     data.html5stream,
             mpd:       'http://d5wlf5xewgt66.cloudfront.net/liveedge/livestream.stream/manifest.mpd',   
             rtsp:      'rstp://d5wlf5xewgt66.cloudfront.net/liveedge/livestream.stream' ,
             playbutton:'/images/play_button.png'
         });
         if (data.filestream === "livestream.stream") {

             cssmenu.show();
             //if (data.ctscan === 1)
             //  hires_button.show();
         }
     }

     function useLink(data) {
         var container = document.querySelector("#mediastream");
         var link = data.html5stream;
         var button = document.createElement("button");
         button.onclick = function() {
             window.location = link;
         }
         var h1 = document.createElement("h1");
         h1.appendChild(document.createTextNode("คลิกเพื่อรับชมการถ่ายทอดสด"));
         button.appendChild(h1);
         container.appendChild(button);
     }


     $.getJSON("<?= site_url("home/liveVideoInfo"); ?>", function(data) {
         var ua = navigator.userAgent.toLowerCase();
         var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
         if (isAndroid) {
             useLink(data);
         } else {
             usePlayer(data);
         }
         textstream.text(data.textstream);

         var iframe_src = "http://live.watnapp.com/live/iframe.php?c=" + data.cname + "&p=watnapp.com&s=";
         iframe_src += data.livemode === "mobile" ? "mobile" : "livestream";

         live_iframe.attr("src", iframe_src);
     }).fail(function() {
         var container = document.querySelector("#mediastream");
         var h1 = document.createElement("h1");
         h1.appendChild(document.createTextNode("ไม่สามารถโหลดวิดีโอได้"));
         container.appendChild(h1);
         $("#mediastream").height("100px");
         $(".stream_online").height("300px");
     });

     $.getJSON(<?= json_encode(site_url("youtube/index")); ?>, function(data) {
         if (data) {
             var youtubeButton = document.querySelector("a#youtube");
             youtubeButton.setAttribute("style", "display: inline");
             youtubeButton.setAttribute("href", data.link);

             var m = data.link.match(/v=(.+)/);

             if (m) {
                 var vid = m[1];
                 var iframe = document.createElement("iframe");
                 iframe.setAttribute("width", "560");
                 iframe.setAttribute("height", "315");
                 iframe.setAttribute("src", "https://www.youtube.com/embed/" + vid + "?rel=0");
                 iframe.setAttribute("frameborder", "0");
                 iframe.setAttribute("frameborder", "0");
                 document.getElementById("youtubestream").appendChild(iframe);
             }
         } else {
             $("#youtubestream").height("1px");
         }
     }); 
 })(jQuery);
</script>
