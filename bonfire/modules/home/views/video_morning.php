<div class="videobox">
    <h2><?= lang('home_title') ?> <?= lang('morning_video') ?></h2>

	<ul id="morning_video">
	</ul>
	
	<div class="clear"></div>
	
	<div style="text-align:center">
		<a class="pure-button" href="<?= site_url("video");?>"><?= lang('view_all'); ?></a>
	</div>

	<script>loadVideoList("<?= site_url("home/morningVideoInfo"); ?>", $("ul#morning_video"));</script>
</div>
