<script>
function count(id) {
    jQuery.get("<?= site_url("book/count/");?>/" + id, function(data) {            
    });
}
</script>

<div class="bookbox">
    <h2>
		<?=lang('home_title')?>
		<?=lang('home_title_book')?>
	</h2>
	
    <ul>
		<?php $i = 0; ?>
		<?php foreach ($medias as $m): ?>
			<?php 
			$mp = $m->pic ? $m->pic : 'media/cover/'."noimg.jpg"; 
			$mp_pic = array('src' => $mp,
				'style' => 'width:128px;height:175px;');
  			$mlink = 'book/download/'.$m->name.'/'.$m->id;
			$shownew = "" ? empty($m->created) : " ".ShowNewIcon($m->created);
  			$showup = "" ? empty($m->dmyupdate) : " ".ShowUpIcon($m->dmyupdate);
			?>
			<li class="colItem">
				<a href="<?= $m->link; ?>" onclick="count(<?=$m->id;?>)" download>
					<img src="<?=$mp_pic["src"];?>" style="<?=$mp_pic["style"];?>">
				</a>
				<br>
				<a href="<?= $m->link; ?>" onclick="count(<?=$m->id;?>)" download>
					<?= $m->name; ?> <?=$shownew;?> <?=$showup;?><br>อ่าน <?=$m->count;?>
				</a>			  
			</li>
			<?php $i++; ?>
			<?php if ($i==6) break; ?>
		<?php endforeach; ?>
    </ul>
	<div class="clear"></div>
	<div style="text-align:center">
		<a class="pure-button" href="<?= site_url("book");?>"><?= lang('view_all'); ?></a>
	</div>
</div>
