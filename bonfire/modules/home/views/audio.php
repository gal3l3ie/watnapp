<div class="audioBox">
    <h2>
		<?= lang('home_title') ?>
		<?= lang('home_title_audio') ?></span>
	</h2>

    <ul>
		<?php $i = 0; ?>
		<?php foreach( $categories as $c ): ?>
			<?php 
			if( $c->album_cover ) {
				$ac = $c->small_album_cover;
			} else {
				$ac = 'thumb.php?h=128&w=128&f=media/album/cover/' . "000.png";
			}
			$album_pic = array('src' => $ac,
				'style' => 'width:128px;height:128px;');
			
			if( !empty($c->created) ) {
				$shownew = " " . ShowNewIcon($c->created);
			} else {
				$shownew = "";
			}
			if( !empty($c->dmyupdate) ) {
				$showup = " " . ShowUpIcon($c->dmyupdate);
			} else {
				$showup = "";
			}
			?>
			<li class="colItem">
				<a href="audio/view_category/<?= $c->id; ?> "><img src="<?= htmlentities($album_pic['src'], ENT_QUOTES, "UTF-8"); ?>" style="<?= $album_pic['style']; ?>"/></a>
				<br>
				<?= anchor('audio/view_category/' . $c->id, $c->album_name) . ' ' . $shownew . ' ' . $showup . '<br>' . lang('view_listen') . ' (' . $c->count . ')'; ?></span>
			</li>
			<?php $i++; ?>
			<?php if ($i==6) break; ?>			
		<?php endforeach; ?>
	</ul>
	
	<div class="clear"></div>
    <div style="text-align:center">
		<a class="pure-button" href="<?= site_url("audio");?>"><?= lang('view_all'); ?></a>
	</div>

</div>
