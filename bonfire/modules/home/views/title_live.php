<div class="left_section_title_live">	
	<h2><?=lang('home_title')?>
		<span id="textstream"></span>
	</h2>

	<iframe id="live_iframe" width=66 height=28 id=status frameborder=0 scrolling=no></iframe>	
    <div class="pure-menu pure-menu-horizontal">
		<a href="#" class="pure-menu-heading pure-menu-link"><?=lang('view_resolution');?></a>
        <ul class="pure-menu-list">
			<li class="pure-menu-item"
				id="hires_button"><a class="pure-menu-link"
									  href='live/streaming/high'
									  ><?=lang('v_res_high');?></a></li>
			<li class="pure-menu-item active"
				><a class="pure-menu-link" href='live/streaming/medium'
					 ><?=lang('v_res_medium');?></a></li>
			<li class="pure-menu-item"
				><a class="pure-menu-link" href='live/streaming/low'
					 ><?=lang('v_res_low');?></a></li>
			<li class="pure-menu-item"
				><a class="pure-menu-link" href='live/streaming/liveaudio'
					 ><?=lang('v_res_audio');?></span></a></li>
			<li class="pure-menu-item">
				<?php if ($have_toggle_switch == "Y"): ?>
					<a class="pure-menu-link" href="/live/toggle_server">
						<?= $videoloc == "THA" ?
										 lang("use_ww_server") :
										 lang("use_tha_server"); ?>
					</a>
				<?php endif; ?>
			</li>
        </ul>
    </div>
</div>
