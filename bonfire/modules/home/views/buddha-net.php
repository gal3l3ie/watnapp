<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAkdY8xM9SQ5U9Wqq-qotk11Abzf7n5hKM"></script>
<script language="javascript" src="<?php echo site_url("assets/js/map$min.js"); ?>" type="text/javascript"></script>

<div id="mapbox">
	<h2>
		<?= lang('home_title_buddha_net') ?>:
		<span id="places"><?= lang('places') ?></span>
		<span id="countries"><?= lang('countries') ?></span>
	</h2>

	<div class="gmap" id="id_gmap" style="height:400px;"></div>
	<br>
	<div style="text-align:center">
		<a class="pure-button" href="http://buddha-net.com/"><?= lang('view_all'); ?></a>
	</div>
	
	<script>
	$(document).ready(function() {
		init_map();
		$.ajax({
			url: "<?= site_url("/buddha_net/stat.php") ?>"
		}).done(function(response) {
			$('#places').html(response.places);
			$('#countries').html(response.countries);
		});
	}); 
	</script>
	<div id="myModal" class="reveal-modal">
		<h1>Modal Title</h1>
		<p>Any content could go in here.</p>
		<a class="close-reveal-modal">&#215;</a>
	</div>
</div>
