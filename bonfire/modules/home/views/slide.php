<script>
function loadCSS_(filename){ 
  var file = document.createElement("link");
  file.setAttribute("rel", "stylesheet");
  file.setAttribute("type", "text/css");
  file.setAttribute("href", filename);

  if (typeof file != "undefined")
     document.getElementsByTagName("head")[0].appendChild(file);
}
<?php $min = ENVIRONMENT == "production" ? "-min" : ""; ?>
loadCSS_("<?= site_url("/scripts/nivo-slider/themes/light/light$min.css"); ?>");
loadCSS_("<?= site_url("/scripts/nivo-slider/nivo-slider$min.css"); ?>");
</script>

<div id="content_wraper_slider">
  <div class="slider-wrapper theme-light">
	  <div id="slider" class="nivoSlider">
		  <!-- Top banners -->
            <a href="https://lft.watnapp.com/"><img src="http://download.watnapahpong.org/data/banner/bbw2019pre.jpg"></a>
            <a href="http://download.watnapahpong.org/data/banner/bbw2018pano-orig.jpg"><img src="http://download.watnapahpong.org/data/banner/bbw2018pano.jpg"></a>
			<a href="http://download.watnapahpong.org/data/banner/nongpano2015.jpg"><img src="http://download.watnapahpong.org/data/banner/nongpano2015small.jpg"></a>
			<img src="http://download.watnapahpong.org/data/banner/watna_event_banner.jpg">
			<img src="http://download.watnapahpong.org/data/banner/timeline_palicanon.jpg">
			<img src="http://download.watnapahpong.org/data/banner/chant.jpg">
			<img src="http://download.watnapahpong.org/data/banner/watnatab.jpg">
		</div>
	</div>
</div>
