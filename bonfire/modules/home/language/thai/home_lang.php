<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$lang["using_ww_server"] = "กำลังใช้เครื่องแม่ข่ายวิดีโอนอกประเทศไทย";
$lang["using_tha_server"] = "กำลังใช้เครื่องแม่ข่ายวิดีโอในประเทศไทย";
$lang["use_ww_server"] = "ใช้แม่ข่ายนอกประเทศ";
$lang["use_tha_server"] = "ใช้แม่ข่ายในประเทศ";
$lang['home_menu_home']					= 'หน้าหลัก';       
$lang['home_menu_news']					= 'ข่าว';
$lang['home_menu_video']					= 'วีดีโอ';
$lang['home_menu_audio']					= 'เสียง';
$lang['home_menu_book']					= 'หนังสือ';
$lang['home_menu_files']					= 'ดาวน์โหลด';
$lang['home_menu_webboard']			= 'เว็บบอร์ด';
$lang['home_menu_faqs']						= 'คำถามที่พบบ่อย';
$lang['home_menu_contact']				= 'ติดต่อเรา';

$lang['home_search_inputtext']			= 'โปรแกรมตรวจหาและเทียบเคียงพุทธวจน';
$lang['home_search_radiothai']			= 'ไทย (บาลีสยามรัฐ)';
$lang['home_search_radiopali']			= 'บาลี (บาลีสยามรัฐ)';
$lang['home_search_read']					= 'อ่านพระไตรปิฏก';

$lang['home_title']							= 'พุทธวจน: ';
$lang['home_title_bw']					= 'พุทธวจน : วัดนาป่าพง';
$lang['home_title_live']					= 'ถ่ายทอดสดประจำวัน';
$lang['home_title_news']				= 'ข่าวสาร';
$lang['home_title_calendar']			= 'ปฏิทินแสดงธรรม';
$lang['outside_video']				= 'พุทธวจนบรรยายนอกสถานที่';
$lang['outside_video']				= 'พุทธวจนบรรยายนอกสถานที่';
$lang['morning_video']				= 'วีดีโอพุทธวจนสนทนาช่วงก่อนฉัน-หลังฉัน';
$lang['saturday_video']				= 'วีดีโอพุทธวจนสนทนาค่ำวันเสาร์';
$lang['short_video'] = "วีดีโอสั้น";
$lang['home_title_book']					= 'สื่อหนังสือ (คลิกเพื่อดาวน์โหลด)';
$lang['home_title_audio']				= 'เสียง';
$lang['home_title_buddha_net'] = 'เครือข่ายพุทธวจน';

$lang['home_title_radio']				= 'วิทยุ';


$lang['places'] = 'สถานที่';
$lang['countries'] = 'ประเทศ';

$lang['view_resolution']					= 'ความละเอียดในการรับชม';
$lang['v_res_low']							= 'ต่ำ';
$lang['v_res_medium']					= 'กลาง';
$lang['v_res_high']							= 'สูง';
$lang['v_res_audio']							= 'เฉพาะเสียง';
$lang['view_all']								= 'ดูทั้งหมด';
$lang['view_time']							= 'ครั้ง';
$lang['view_read']							= 'อ่าน';
$lang['view_listen']							= 'ฟัง';
$lang['today']									= 'วันนี้';
$lang['more_detail']						= 'รายละเอียดเพิ่มเติม';
$lang['lastest_news']						= 'ข่าวสารล่าสุด';

$lang['live_button_liveout']			 = 'ถ่ายทอดสดนอกสถานที่';
$lang['live_button_liveout_audio'] = 'ถ่ายทอดสดนอกสถานที่ (เฉพาะเสียง)';
$lang['live_button_ques_sndl']		= 'ส่งคำถามสนทนาธรรมค่ำวันเสาร์';
$lang['live_button_ques_live']		= 'ส่งคำถามสนทนาธรรมประจำวัน(หลังฉัน)';
$lang['live_title']							= 'วัดนาป่าพง';
$lang['live_nightsat']					= 'ถ่ายทอดสดสนทนาธรรมค่ำวันเสาร์';
$lang['live_daily']						= 'ถ่ายทอดสดสนทนาธรรมประจำวัน';
$lang['live_outside']					= 'ถ่ายทอดสดนอกสถานที่';

$lang['stat_title']						= 'ผู้เยี่ยมชม';
$lang['stat_today']						= 'วันนี้';
$lang['stat_yesterday']				= 'เมื่อวานนี้';
$lang['stat_month']						= 'เดือนนี้';
$lang['stat_year']						= 'ปีนี้';
$lang['stat_ipdetail']					= '**IP ไม่ซ้ำกันรายวัน';

$lang['link_landfund_namelist']	= 'รายชื่อเจ้าภาพกองทุนบุญที่ดิน';
$lang['link_fiber']						= 'รายละเอียดบัญชีไฟเบอร์ออฟติก';

$lang['buddha_net']					= 'เครือข่ายพุทธวจน';
$lang['wnpp_aboutus']				= 'เกี่ยวกับวัดนาป่าพง (ภาษาอังกฤษ)';

$lang['bitrate']                    = 'อัตราบิต';
$lang['listener_number']            = 'จำนวนผู้ฟัง';
