<?php
class Content extends Admin_Controller {
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		
		Template::set('toolbar_title', 'Home');
		Template::set('editor_id', $this->session->userdata('user_id'));

		$this->auth->restrict('Permissions.Audi-file.Manage');
		
		$this->lang->load('activities');
		$this->lang->load('datatable');
		Assets::add_module_js('home', 'jquery.dataTables.min.js');
		Assets::add_module_css('home', 'datatable.css');				
	}

	function index()
	{
        Assets::add_js($this->load->view('content/files_js', null, true), 'inline');
        Template::render();
	}

    function create_news()
	{
        Template::render();
	}
	
    function create_mobile_live()
	{
        Template::render();
	}
	
    function add_mobile_live()
	{
		$id = $this->uri->segment(5);
		if($this->input->post('submit')) {
			$wpl = new Wplive($id);
			$wpl->from_array($_POST);
						
			if($wpl->save()) {
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/home');
			} else {
				Template::set_message('There was a problem uploading file.');
			}
		}
		Template::render();
	}
	
    function add_news()
	{
		$id = $this->uri->segment(5);
		if($this->input->post('submit')) {
			$wpn = new Wpnew($id);
			$wpn->from_array($_POST);
            
            $config = array();
			$config['upload_path'] = './media/slider/';
			$config['allowed_types'] = 'jpg';
			$config['max_size']	= '1000000';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('pic'))
			{
				$d = $this->upload->data();
                $this->load->library('cloud');
				if($id) {
					$name = $wpn->title.'-'.$id;
				} else {
					$name = $wpn->title;
				}
				$wpn->pic = $this->cloud->add_slider($d['file_name'], $name);
			}

			if($wpn->save()) {
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/home');
			} else {
				Template::set_message('There was a problem uploading file.');
			}
		}
		Template::render();
	}
	
    function edit_news() 
    {
        $id = $this->uri->segment(5);
        $wpns = new Wpnew();
        Template::set('wpns',$wpns->where('id',$id)->get());
        Template::set('wpn',$wpns);
        Template::render();
    }
	
    function edit_mobile_live() 
	{
		$id = $this->uri->segment(5);
		$wpls = new Wplive();
		Template::set('wpls',$wpls->where('id',$id)->get());
		Template::set('wpl',$wpls);
		Template::render();
	}
	
    function view()
    {
    }
	
	public function breaking_news() 
	{
		return $this->_get_activity('breaking_news');
	}
	
    public function mobile_live() 
	{
        return $this->_get_activity('mobile_live');
	}
	
    public function _get_activity($which='alll_news',$find_value=FALSE)
	{	
        switch ($which) { 
        case 'breaking_news':
            $wpn = new Wpnew();
            Template::set('activity_content',$wpn->order_by('id','desc')->get());
            Template::set('check','breaking_news');
            break;
        case 'mobile_live':
            $wpl = new Wplive();
            Template::set('activity_content',$wpl->order_by('id','desc')->get());
            Template::set('check','mobile_live');
            break;
        default:
            $wpn = new Wpnew();
            Template::set('activity_content',$wpn->order_by('id','desc')->get());
            break;
		}
        Template::set_view('content/view');
        Template::render();
    }
}
