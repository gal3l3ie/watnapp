<?php

class Home extends Front_Controller
{

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Bangkok');
  }

  private function loadVideoInfo($category) {
      header("Content-type", "application/json");
      $video_table = new YoutubeVideos();
      $videos = array();
      foreach($video_table->where("category", $category)->get() as $entry) {
          $videos[] = array(
              "watch" => $entry->videoUrl,
              "thumbnail" => $entry->pictureUrl,
              "title" => $entry->title,
              "viewCount" => $entry->viewCount,
              "likeCount" => $entry->likeCount);
      }
      echo json_encode(array("entries" => $videos));
  }

  function outsideVideoInfo()
  {
      $this->loadVideoInfo("outside");
  }

  function shortVideoInfo()
  {
      $this->loadVideoInfo("short");
  }

  function morningVideoInfo()
  {
      $this->loadVideoInfo("morning");
  }

  function saturdayVideoInfo()
  {
      $this->loadVideoInfo("saturday");
  }


  function liveVideoInfo()
  {
    $this->load->library("session");
    include("geolite/geoip.inc");
    include("geolite/geoipcity.inc");
    $gi = geoip_open("geolite/GeoLiteCity.dat", GEOIP_STANDARD);

    date_default_timezone_set('Asia/Bangkok');
    $time = date("G");
    $week = date("w");
    //$rtmpstream = "rtmp://media.watnapahpong.org/live-record";
    $filestream = "livestream";
    //check ip to country;
    $ip = $this->getIP();
    $record = GeoIP_record_by_addr($gi, $ip);

    //ctscan 0=low medium 1=low medium high
    $ctscan = "1";

    $server = null;

    $cname = "";

    if (!$this->session->userdata("videoloc")) {
      if ($record && $record->country_name == "Thailand")
        $this->session->set_userdata("videoloc", "THA");
      else
        $this->session->set_userdata("videoloc", "WW");
    }

    if ($record && $record->country_code == "TH") {
      $this->session->set_userdata("videoloc", "THA");
    }

    if ($this->session->userdata("videoloc") == "THA") {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://live.watnapp.com:1935/loadbalancer");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $redirect = substr(curl_exec($ch), 9);
      $server = (strlen($redirect) > 0 && $redirect != 'unknown') ? $redirect.":1935" : "live.watnapp.com:1935";
      $cname = $record != null ? $record->continent_code : 'AS';
    } else {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://api.watnapp.com/cloudfront.php");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $server = json_decode(curl_exec($ch))->{'server'};
    }

    $filestream = "livestream.stream";

    $rtmpstream = "rtmp://" . $server . "/liveedge";
    $html5stream = "http://" . $server . "/liveedge/" . $filestream . "/playlist.m3u8";
    $rtspstream = "rtsp://" . $server . "/liveedge/" . $filestream;

    //....
    $livemode = "livestream";
    $testcheck = "1";

    $imagestream = "http://watnapahpong.com/static/broadcast_info.jpg";

    if ($week == "6" && $time >= "18") {
      //ทุกค่ำวันเสาร์ตั้งแต่ 18.00 น.
      $textstream = lang('live_nightsat');
      $livemode = "nightsat";
    } else if ($week != "6" && $time >= "18") {
      //ทุกวันยกเว้นวันเสาร์ตั้งแต่ 18.00 น.
      $textstream = lang('live_daily');
    } else if ($time <= "4") {
      //ก่อน 04.00 น.
      $textstream = lang('live_daily');
    } else {
      //นอกเหนือจากเวลาข้างต้น
      $textstream = lang('live_daily');
    }

    //ถ่ายทอดสดนอกสถานที่ และรายการธรรมะ
    $wpl = new Wplive ();
    $match = date("Y-m-d");
    $tlstart = $wpl->like('time_live_start', $match)->get();

    foreach ($tlstart as $t) {
      $strTlstart = date($t->time_live_start);
      $strTlstop = date($t->time_live_stop);
      $strTlnow = date("Y-m-d H:i:s");

      $datelimit = (strtotime($strTlstop) - (strtotime($strTlstart)));

      $datediff = (strtotime($strTlnow) - (strtotime($strTlstart)));  // 1 day = 60*60*24

      if ($datediff >= -600 && $datediff <= $datelimit + 1800) {
        $filestream = "mobile";
        $livemode = $filestream;
        $textstream = lang('live_outside');
        $imagestream = "http://watnapahpong.com/images/watnapp-mobilewait.jpg";
      }
    }

    geoip_close($gi);

    echo json_encode(
            array(
                "filestream" => $filestream,
                "html5stream" => $html5stream,
                "livemode" => $livemode,
                "textstream" => $textstream,
                "imagestream" => $imagestream,
                "ctscan" => $ctscan,
                "cname" => $cname));
  }

  function getIP()
  {
    $ip_lst = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER["REMOTE_ADDR"];
    $toks = preg_split('/,\s+/', $ip_lst);
    $ip = $toks[0];
    return $ip;
  }

  function index()
  {
    $this->load->library('user_agent');
    $this->load->library("session");

    include("geolite/geoip.inc");
    include("geolite/geoipcity.inc");
    $gi = geoip_open("geolite/GeoLiteCity.dat", GEOIP_STANDARD);
    $ip = $this->getIP();
    $record = GeoIP_record_by_addr($gi, $ip);

    $have_toggle_switch = "Y";
    if ($record && $record->country_code == "TH") {
      $this->session->set_userdata("videoloc", "THA");
      $have_toggle_switch = "N";
    }

    if (!$this->session->userdata("videoloc")) {
      if ($record && $record->country_name == "Thailand")
        $this->session->set_userdata("videoloc", "THA");
      else
        $this->session->set_userdata("videoloc", "WW");
    }
    $cats = new Category();
    if (!$this->agent->is_mobile())
      Template::set('categories', $cats->order_by('dmyupdate', 'desc')->get(10));
    else
      Template::set('categories', $cats->order_by('dmyupdate', 'desc')->get(5));

    $file = "book";
    $medias = new Media();
    if (!$this->agent->is_mobile())
      Template::set('medias', $medias->where('type', $file)->where("visibility", "show")->order_by('id', 'desc')->get(10));
    else
      Template::set('medias', $medias->where('type', $file)->where("visibility", "show")->order_by('id', 'desc')->get(5));

    //counter stat ip
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d', strtotime("-1 day"));
    $thismonth = date('Y-m');
    $lastmonth = date('Y-m', strtotime("-1 month"));
    $thisyear = date('Y');
    $lastyear = date('Y', strtotime("-1 year"));

    $cout = new counter();
    $dail = new daily();

    Template::set('tday', $cout->where('date', $today)->count());
    $daila = new daily();
    Template::set('yday', $daila->where('date', $yesterday)->get());
    $dailb = new daily();
    Template::set('tmonth', $dailb->select_sum('num')->where("DATE_FORMAT(date,'%Y-%m')", $thismonth)->get());
    $dailc = new daily();
    Template::set('tyear', $dailc->select_sum('num')->where("DATE_FORMAT(date,'%Y')", $thisyear)->get());

    //new
    $wpns = new wpnew();
    Template::set('wpns', $wpns->where("status", "เปิด")->order_by('id', 'desc')->get());

    $wpnssl = new wpnew();
    Template::set('wpnssl', $wpnssl->where('status', 'เปิด')->where('mode', '1')->order_by('id', 'asc')->get());


    Template::set("videoloc", $this->session->userdata("videoloc"));
    Template::set("have_toggle_switch", $have_toggle_switch);

    Template::render();
  }

  function onlinerealtime()
  {
    date_default_timezone_set('Asia/Bangkok');
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    $today = date("Y-m-d");
    $co = new counter();
    $ctcount = $co->where('ip', $ip)->count();

    if ($ctcount == 0) {
      $co->date = $today;
      $co->ip = $ip;

      if (!DONOTCOUNT) {
        $co->save();
      }
    }
    Template::render('ajax');
  }

  function exonlinerealtime()
  {
    date_default_timezone_set('Asia/Bangkok');
    //counter stat ip

    $today = date("Y-m-d");
    $yesterday = date('Y-m-d', strtotime("-1 day"));
    $thismonth = date('Y-m');
    $lastmonth = date('Y-m', strtotime("-1 month"));
    $thisyear = date('Y');
    $lastyear = date('Y', strtotime("-1 year"));

    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    $co = new counter_media();
    $ctcount = $co->where('ip', $ip)->count();

    if ($ctcount == 0) {
      $co->date = $today;
      $co->ip = $ip;

      if (!DONOTCOUNT) {
        $co->save();
      }
    }

    //***//
    $ct = $co->get(1);

    if ($ct->date != $today) {
      $cou = new counter_media();

      $ytd_total = $cou->where('date', $yesterday)->count();

      $dai = new daily_media();
      $dai->date = $yesterday;
      $dai->num = $ytd_total;

      if (!DONOTCOUNT) {
        $dai->save();
        $co->truncate();
      }
    }

    $cout = new counter_media();
    $dail = new daily_media();

    Template::set('tday', $cout->where('date', $today)->count());
    $daila = new daily_media();
    Template::set('yday', $daila->where('date', $yesterday)->get());
    $dailb = new daily_media();
    Template::set('tmonth', $dailb->select_sum('num')->where("DATE_FORMAT(date,'%Y-%m')", $thismonth)->get());
    $dailc = new daily_media();
    Template::set('tyear', $dailc->select_sum('num')->where("DATE_FORMAT(date,'%Y')", $thisyear)->get());
    //***//
    Template::render('ajax');
  }

  function datetest()
  {
    Template::render('blank');
  }

  function statement()
  {
    Template::render();
  }

  function testmo()
  {
    include("geolite/geoip.inc");
    include("geolite/geoipcity.inc");
    $gi = geoip_open("geolite/GeoLiteCity.dat", GEOIP_STANDARD);

    $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER["REMOTE_ADDR"];
    $record = GeoIP_record_by_addr($gi, $ip);

    $server = null;
    if ($record != null && $record->country_name == "Thailand") {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://ec2.watnapp.com:1935/loadbalancer");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $redirect = substr(curl_exec($ch), 9);
      $server = (strlen($redirect) > 0 && $redirect != 'unknown') ? $redirect.":1935" : "live.watnapp.com:1935";
    } else {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://api.watnapp.com/cloudfront.php");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $server = json_decode(curl_exec($ch))->{'server'};
    }

    $cname = $record != null ? $record->continent_code : 'AS';
    $imagestream = "http://watnapahpong.com/images/watnapp-satall.jpg";
    $filestream = "livestream";

    $rtmpstream = "rtmp://" . $server . "/liveedge";
    $html5stream = "http://" . $server . "/liveedge/" . $filestream . ".stream/playlist.m3u8";

    Template::set('filestream', $filestream);
    Template::set('rtmpstream', $rtmpstream);
    Template::set('html5stream', $html5stream);
    Template::set('imagestream', $imagestream);
    Template::set('cname', $cname);

    Template::render();

    geoip_close($gi);
  }

  function count_proxy()
  {
  
    $url = COUNTER_URL;
    $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER["REMOTE_ADDR"];
    // http://stackoverflow.com/questions/5224790/curl-post-format-for-curlopt-postfields
    $data = array("remote_ip" => $ip);
    // http://wezfurlong.org/blog/2006/nov/http-post-from-php-without-curl/
    $data = http_build_query($data);
    $params = array('http' => array(
            'method' => 'POST',
            'content' => $data
    ));
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if (!$fp) {
      throw new Exception("Problem with $url");
    }
    $response = @stream_get_contents($fp);
    if ($response === false) {
      throw new Exception("Problem reading data from $url");
    }
    echo $response;
 
  }

  function count()
  {
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d', strtotime("-1 day"));
    $thismonth = date('Y-m');
    $lastmonth = date('Y-m', strtotime("-1 month"));
    $thisyear = date('Y');
    $lastyear = date('Y', strtotime("-1 year"));

    $ip = $_POST['remote_ip'];

    $co = new counter();
    $ctcount = $co->where('ip', $ip)->count();

    if ($ctcount == 0) {
      $co->date = $today;
      $co->ip = $ip;
      if (!DONOTCOUNT) {
        $co->save();
      }
    }

    $ct = $co->get(1);

    if ($ct->date != $today) {
      $cou = new counter();

      $ytd_total = $cou->where('date', $yesterday)->count();

      $dai = new daily();
      $dai->date = $yesterday;
      $dai->num = $ytd_total;
      if (!DONOTCOUNT) {
        $dai->save();
        $co->truncate();
      }
    }
    echo "DONE $ip";
  }

  function ipinfo()
  {
    $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER["REMOTE_ADDR"];
    echo file_get_contents("http://api.easyjquery.com/ips/?ip=" . $ip . "&full=true");
  }

  function training()
  {
    Template::render();
  }

}
