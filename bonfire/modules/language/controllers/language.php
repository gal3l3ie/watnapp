<?php 
class Language extends Front_Controller {
    public function __construct() {
        $this->load->library('session');
    }
	
    function index()
    {
        Template::render();
    }
    
    function th()
    {
        $this->session->set_userdata('LANGUAGE','TH');
        $this->lang->load('home','thai');
        Template::redirect(site_url('/')); 
    }
	
    function en()
    {
        $this->session->set_userdata('LANGUAGE','EN');
        $this->lang->load('home','english');
        Template::redirect(site_url('/')); 
    }
}
