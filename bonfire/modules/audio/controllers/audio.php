<?php 
class Audio extends Front_Controller {
  
  public function __construct()
  {
    parent::__construct();    
    Template::set('livemode','audio');
  }
  
  function index()
  {
    $cats = new Category();
    $song = new Song();
    $this->load->library('pagination');

    $config['base_url'] = site_url('/audio/index/');
    $config['total_rows'] = $cats->count();
    $config['per_page'] = 15; 
    $config['uri_segment'] = 3;

    $this->pagination->initialize($config); 
        
    Template::set('categories',$cats->order_by('dmyupdate','desc')->get($config['per_page'],$this->uri->segment(3)));
    Template::set('songten',$song->order_by('updated','desc')->get(10));
    Template::set('song',$song);

    Template::render();
  }

  function view_category($id, $sid = NULL)
  {
    $cat = new Category($id);
    $song = new Song($sid);

    if ($song->file != '') {
      Template::set('filestream',site_url('media/audio/'.$song->file));
      Template::set('imgstream',site_url('media/album/cover/'.$cat->album_cover));
      Template::set('song_play',$song->name);
    } else {
      Template::set('filestream',$song->file_url);
      Template::set('imgstream',site_url('media/album/cover/'.$cat->album_cover));
      Template::set('song_play',$song->name);
    }
    Template::set('ogg_url',$song->ogg_url);

    if ($sid) {
      $song->count += 1;
      $song->save();
    } else {
      $cat->count += 1;
      $cat->save();
    }
    Template::set('cat',$cat);
    Template::set('song',$song);

    Template::render();
  }
  
  function gen_name($song)
  {
    return "พุทธวจน_".mb_substr($song->name, 0, 30, "UTF-8")."_".$song->id.".mp3";
  }

  function inc_dl($id) {
    $song = new Song($sid);
    $song->dl_count += 1;
    $song->save();
  }

  
  function download($id,$sid = NULL)
  {    
    $cat = new Category($id);
    $song = new Song($sid);
    #$song->dl_count += 1;
    #$song->save();
      
    if ( ! empty($song->file)) {
      $data = site_url('media/audio/'.$song->file);
      $name = $this->gen_name($song);
      header('Content-type: audio/mpeg');
      header('Content-Disposition: attachment; filename="'.$name.'"');
      readfile($data);
    } else if( ! empty($song->file_url)) {
      $data = $song->file_url;
      $name = $this->gen_name($song);
      header('Content-type: audio/mpeg');
      header('Content-Disposition: attachment; filename="'.$name.'"');
      readfile($data);      
    } else {
      Template::redirect('/audio');
    }
  }

  function radio_stat()
  {
    header('Content-type: application/json');
    $radio_stat = new RadioStat(1);

    if (!$radio_stat->stat || rand(0,1000) == 44) {
        $stat = file_get_contents("http://live2.watnapp.com:9000/status-json.xsl");
        $radio_stat->stat = $stat;
        $radio_stat->save();
    } else {
        $stat = $radio_stat->stat;
    }
    echo $stat;
  }
}
