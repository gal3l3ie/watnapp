<?php

require_once(FCPATH.'/convert_to_thumb.php');

class Content extends Admin_Controller
{
  public function __construct()
  {
    date_default_timezone_set('Asia/Bangkok');
    parent::__construct();
    Template::set('toolbar_title', 'Audio');
    $this->auth->restrict('Permissions.Audi-file.Manage');
    $this->lang->load('activities');
    $this->lang->load('datatable');
    Assets::add_module_js('landfund', 'jquery.dataTables.min.js');
    Assets::add_module_css('landfund', 'datatable.css');
  }

  function index()
  {
    Assets::add_js($this->load->view('content/files_js', null, true), 'inline');
    Template::render();
  }

  function create()
  {
    $this->auth->restrict('Audio.Content.Create');
    Template::render();
  }

	function add_file()
	{
		$id = $this->uri->segment(5);
		if($this->input->post('submit'))
		{
			$this->load->library('cloud');
			$cat = new Category($id);
			$cat->from_array($_POST);

			$config['upload_path'] = './media/album/cover';
			$config['allowed_types'] = '*';
			$config['max_size']	= '10000000';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('album_cover'))
			{
				$ul = $this->upload->data();
				$cat->album_cover = $this->cloud->add_album_cover($ul['file_name'], $_POST['album_name']);
        convert_to_thumb(FCPATH.'/media/album/cover/'.$ul['file_name'],
                         FCPATH.'/media/album/smallcover/'.$ul['file_name'], 128, 128);
				$cat->small_album_cover = $this->cloud->add_small_album_cover($ul['file_name'], $_POST['album_name']);
        unlink(FCPATH.'/media/album/cover/'.$ul['file_name']);
        unlink(FCPATH.'/media/album/smallcover/'.$ul['file_name']);
			}

			$this->upload->initialize($config);
			if($this->upload->do_upload('album_content_doc'))
			{
				$doc = $this->upload->data();
				$cat->album_content_doc = $this->cloud->add_album_cover($doc['file_name'], $_POST['album_name'].'.doc');
        unlink(FCPATH.'/media/album/cover/'.$doc['file_name']);
			}

			$this->upload->initialize($config);
			if($this->upload->do_upload('album_content_txt'))
			{
				$txt = $this->upload->data();
				$cat->album_content_txt = $this->cloud->add_album_cover($txt['file_name'], $_POST['album_name'].'.txt');
        unlink(FCPATH.'/media/album/cover/'.$txt['file_name']);
			}

			$cat->dmyupdate =date('Y-m-d H:i:s');

			if($cat->save())
			{
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/audio');
			}
			else
			{
				Template::set_message('There was a problem uploading file.');
			}
		}
		Template::render();
	}

  function edit()
	{
		$this->auth->restrict('Audio.Content.Edit');

		$id = $this->uri->segment(5);

		$albums = new Category();
		Template::set('a',$albums->where('id',$id)->get());

		Template::render();
	}

	function add_song()
	{
		$id = $this->uri->segment(5);
		$sid = $this->uri->segment(6);

		if($this->input->post('submit'))
		{
			$son = new Song($sid);
			$son->from_array($_POST);
			$config['upload_path'] = './media/audio/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '300000';
			$this->load->library('upload', $config);
			$this->load->library('cloud');
			if($this->upload->do_upload('file'))
			{
				$d = $this->upload->data();
        $son->file_url = $this->cloud->add_audio($d['file_name'], $_POST['name']);
        unlink(FCPATH."/media/audio/".$d['file_name']);

			}
			$son->category_id = $id;
			$son->dmyupdate =date('Y-m-d H:i:s');

			if($son->save())
			{
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/audio/add_song/'.$id);
			}
			else
			{
				Template::set_message('There was a problem uploading file.');
			}
		}
		$cat = new Category();
		$song = new Song();

		Template::set('cat',$cat->where('id',$id)->get());
		Template::set('song',$song->where('id',$sid)->get());
    Template::render();
	}

	function tag()
	{
    Template::render();
	}

	function add_tag()
	{
		$id = $this->uri->segment(5);
		if($this->input->post('submit'))
		{
			$tag = new Tag($id);
			$tag->from_array($_POST);

			if($tag->save())
			{
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/audio');
			}
			else
			{
				Template::set_message('There was a problem uploading file.');
			}
		}
	}

	//ACTIVITY

	public function album_edit()
	{
		if (has_permission('Audio.Content.Edit')) {
			return $this->_get_activity('album_edit');
		}

		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/audio');
	}

	public function _get_activity($which='all_album',$find_value=FALSE)
	{
		//Assets::add_js($this->load->view('content/datatable_js', null, true), 'inline');
    switch ($which) {
      case 'album_edit':
        $albums = new Category();
        Template::set('activity_content',$albums->order_by('id','desc')->get());
        Template::set('check','album_edit');
        break;
      default:
        $albums = new Category();
        Template::set('activity_content',$albums->order_by('id','desc')->get());
        //Template::set('check','A');
        break;
		}
		Template::set_view('content/view');
		Template::render();
	}
}
