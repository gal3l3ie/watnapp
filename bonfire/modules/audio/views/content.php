<div class="paging-box float-right">หน้า <?= $this->pagination->create_links(); ?></div>
<ul>
	<?php foreach( $categories as $c ): ?>
		<?php 
		if( $c->album_cover ) {
			$ac = $c->small_album_cover;
		} else {
			$ac = 'thumb.php?h=128&w=128&f=media/album/cover/' . "000.png";
		}
		$album_pic = array('src' => $ac,
			'style' => 'width:128px;height:128px;');
		
		if( !empty($c->created) ) {
			$shownew = " " . ShowNewIcon($c->created);
		} else {
			$shownew = "";
		}
		if( !empty($c->dmyupdate) ) {
			$showup = " " . ShowUpIcon($c->dmyupdate);
		} else {
			$showup = "";
		}
		?>
		<li class="colItem">
			<a href="/audio/view_category/<?= $c->id; ?> "><img src="<?= htmlentities($album_pic['src'], ENT_QUOTES, "UTF-8"); ?>" style="<?= $album_pic['style']; ?>"/></a>
			<br>
			<?= anchor('audio/view_category/' . $c->id, $c->album_name) . ' ' . $shownew . ' ' . $showup . '<br>' . lang('view_listen') . ' (' . $c->count . ')'; ?></span>
		</li>
	<?php endforeach; ?>
</ul>

<div class="clear"></div>
