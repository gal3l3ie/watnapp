﻿<div class="audioBox">
	<h2>พุทธวจน: เสียง</h2>
	<div class="pure-g">
		<div class="pure-u-1 pure-u-lg-4-5">
			<?php require("addthis.php"); ?>
			<?php
			if($cat->album_cover) {
				$ac = $cat->small_album_cover;
			} else {
				$ac = 'media/album/cover/'."000.png";
			}
			
			$album_pic = array(
				'src' => $ac,
					'style' => 'width:128px;height:128px;');
			?>
			
			<?= img($album_pic); ?>
			<?php 
			$song_id = $song->id;
			$found_current = false;
			$next_id = NULL;
			
			foreach($cat->song->order_by("id")->get() as $s) {
				if ($found_current) {
					$next_id = $s->id;
					break;
				} else if($s->id == $song_id) {
					$found_current = true;
				}
			}
			?>


			<h3><?php echo $cat->album_name ?></h3><br>
			<?php if ($filestream != "") { ?>
				<h4>กำลังฟัง :<?= $song_play; ?></h4><br />
				<div id="audioContainer">
					<script>
					var div = document.getElementById("audioContainer");
					var audio = document.createElement('audio');
					audio.setAttribute("controls", true);
					audio.setAttribute("autoplay", true);
					var url = "<?=$ogg_url;?>";
					if(url === "" || audio.canPlayType("audio/ogg").replace(/^no$/, "") === "") {
						url = "<?=$filestream;?>";
					}
					audio.setAttribute("src", url);
					div.appendChild(audio);
					
					var next_id = <?=json_encode($next_id);?>;
					
					if (next_id) {
						var next_url = window.location.toString();
						next_url = next_url.replace(/\d+$/, next_id);
						audio.addEventListener("ended", function(evt) {
							window.location = next_url;
						});
					}
					</script>
				</div>
			<?php } ?>

			<h3>รายชื่อเสียงทั้งหมดในอัลบั้มนี้</h3>
			<table class="album">
				<tr>
					<td></td>
					<td>จำนวนดาวน์โหลด</td>
					<td>จำนวนคนฟัง</td>
					<td></td>
					<td></td>
				</tr>
				<?php foreach($cat->song->order_by("id")->get() as $s): ?>
					<?php $count = $s->count ? "(" . $s->count . ")" : "(0)"; ?>
					<tr>
						<td><?= $s->name; ?></td>
						<td><?= $s->dl_count; ?></td>
						<td><?= $s->count; ?></td>
						<td><a class="pure-button" href="<?= site_url("audio/view_category"). '/' . $cat->id . '/' . $s->id;?>">PLAY</a></td>
						<td><a class="pure-button" onclick="inc_dl(<?= $s->id; ?>);" href="<?= $s->file_url; ?>" download>MP3</a></td>
					</tr>
				<?php endforeach; ?>
			</table>
			<?php if($cat->zip_url != NULL): ?>
				<div style="text-align:center;">
					<a class="pure-button pure-button-primary" href="<?= $cat->zip_url; ?>">ดาวโหลดทั้งหมด</a>
				</div>
			<?php endif; ?>

		</div>
		<div class="pure-u-1 pure-u-lg-1-5">
			<h3>วิธีฟังเสียง และการดาวน์โหลด</h3>
			<ol>
				<li>ฟังบนเว็บให้คลิ๊กปุ่ม PLAY</li>
				<li>ดาวน์โหลดเก็บไว้ฟังคลิ๊กปุ่ม MP3 (ถ้าหากไม่สำเร็จสามารถคลิกขวาและเลือก Save link as ...) </li>
				<li>ดาวน์โหลดทั้งหมดเป็น ZIP เลือกรายการท้ายสุด</li>
			</ol>

			<h3>สารบัญเสียงอัลบั้มนี้ คลิ๊กเพื่ออ่าน</h3>
			<?php
			if ($cat->album_content_doc || $cat->album_content_txt) {
				if ($cat->album_content_doc) {
					echo anchor($cat->album_content_doc, img('media/album/images/doc.png') , array('target' => '_blank'));
				}
				if ($cat->album_content_txt) {
					echo anchor($cat->album_content_txt, img('media/album/images/txt.png') , array('target' => '_blank'));
				}
			} else {
				echo "ไม่มีสารบัญเสียง";
			}
			?>			
		</div>
	</div>
</div>
<script>
function inc_dl(id) {
	$.get("http://www.watnapp.net/audio/inc_dl/".id);
	return true;
}
</script>

