<style>
#flex_table { margin: 0; }
#flex_table th { font-weight: bold; }
#flex_table th.sorting_desc, #flex_table th.sorting_asc { background-color: #F5F5F5;}
</style>

<div class="scrollable" id="ajax-content">
  <script>
  $(document).ready(function() {
    $("#tf_date").datepicker({ dateFormat: 'yy-mm-dd' });
  });
  </script>

	<div id="user_activities">
		<table id="flex_table">
			<thead>
				<tr>
					<th>ลำดับ</th>
					<th>ชื่ออัลบั้ม</th>
					<th>	ดำเนินการ1</th>
					<th>ดำเนินการ2</th>
				</tr>
			</thead>
			
			<tfoot></tfoot>
			
			<tbody>
				<?php foreach ($activity_content as $activity) : ?>
				<tr>
					<td><?php echo $activity->id; ?></td>
					<td>
					<?php echo $activity->album_name ?>
                    </td>
					<td >
					<?php 
		if ($check =="album_edit"){
						echo '<a href="'.site_url(SITE_AREA .'/content/audio/edit/'.$activity->id).'" class="ajaxify">[แก้ไขอัลบั้ม]</a>';
		}
					?>
                    </td>
					<td><?php 
					/*
			if ($activity->dep == "1"){
					echo "<span class='buttons' id='button_".$activity->id."'>".$nameposter."<a class='btn-printing' href='javascript: void(0)' title='".$activity->dep_dated."'></a></span>";
			}else{
					echo "<span class='buttons' id='button_".$activity->id."'>".$nameposter."<a class='btn-print' href='javascript: void(0)' title='".$activity->dep_dated."'></a></span>";
			}
			echo "</div>";
		}
		*/
		
		if ($check =="album_edit"){
			echo '<a href="'.site_url(SITE_AREA .'/content/audio/add_song/'.$activity->id).'" class="ajaxify">[เพิ่มหรือแก้ไข ไฟล์เสียง]</a>';
}

					//echo date('M j, Y g:i A', strtotime($activity->created));
					?>
                    </td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>