<style>
.list-view .permission_set {cursor: pointer; border-bottom: 1px solid #CCCCCC; background: #F5F5F5; vertical-align: middle; }
.list-view .permission_set img { height: 10px; width: 10px; float: right; margin: 14px 14px 0 0; }
</style>


<div class="view split-view">
	
	<!-- Role List -->
	<div class="view">	
		<div class="scrollable">
			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        ข้อมูล
					</h4>
					
					<div class="list-item" data-id="create"> 
						<p>
							<b>เพิ่มอัลบั้มใหม่</b><br/>
							<span class="small"></span>
						</p>
					</div>
                    <div class="list-item" data-id="tag">
						<p>
							<b>เพิ่มหมวดหมู่อัลบั้ม</b><br/>
							<span class="small">เพื่อจัดกลุ่มให้กับอัลบั้ม</span>
						</p>
					</div>
               
			</div>	<!-- /list-view -->
            
            			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        แก้ไข
					</h4>
					
					<div class="list-item" data-id="album_edit">
						<p>
							<b>แก้ไขอัลบั้ม</b><br/>
							<span class="small">แก้ไขชื่ออัลบั้ม เพิ่มหรือแก้ไขไฟล์เสียง</span>
						</p>
					</div>
			</div>	<!-- /list-view -->
            
		</div>
	</div>
	
	<!-- Role Editor -->
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
				
			<div class="box create rounded">
				<a class="button good ajaxify" href="<?php echo site_url(SITE_AREA .'/content/audio/create'); ?>">เพิ่มอัลบั้มใหม่</a>

				<h3>ไฟล์เสียง</h3>

				<p></p>
			</div>
			<br />
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
