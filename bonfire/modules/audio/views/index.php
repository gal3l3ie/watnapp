<div class="radioBox">
    <h2>
        <?=lang('home_title')?>
        <?=lang('home_title_radio')?>
    </h2>

    <?php $radio_urls = array(1 => "http://listentobuddha.org:8040/stream",
                              2 => "http://listentobuddha.org:8034/stream",
                              3 => "http://listentobuddha.org:8094/stream",
                              4 => "http://listentobuddha.org:8100/stream",
                              5 => "http://listentobuddha.org:8106/stream"); ?>
    

    <?php $desc_array = array(1 => "ถ่ายทอดสดประจำวัน เสียงอ่านพระสูตร และธรรมบรรยาย",
			      2 => "ถ่ายทอดสดนอกสถานที่ เสียงอ่านพระสูตร และธรรมบรรยาย",
			      3 => "เสียงอ่านพระสูตร",
			      4 => "ธรรมบรรยาย",
			      5 => "Suttas in English"); ?>

    
    <script>
     function show_ch(ch) {
	 var i;
	 for(i=1;i<=5;i++) {
	     document.getElementById('radioPanel' + i).pause()
	     $("div#radioch" + i).css("display", "none");
	 }
	 $("div#radioch" + ch).css("display", "block");
     }
    </script>

    <div class="pure-g">
	<div class="pure-u-1">
	    <div class="pure-menu">
		<ul class="pure-menu-list">
		    <?php for($i=1;$i<=5;$i++): ?>
			<tr>
			    <td>
				<li class="pure-menu-item">
				    <a onclick="show_ch(<?= $i ?>);" href="#" class="pure-menu-link">ช่อง <?= $i ?>: <?= $desc_array[$i]; ?></a>
				</li>
			    </td>
			</tr>
		    <?php endfor; ?>
		</ul>
	    </div>
	</div>

	<div class="pure-u-1">
	    <?php for($i=1;$i<=5;$i++): ?>
		<div id="radioch<?= $i ?>" <?php if ($i != 1): ?>class="hide"<?php endif; ?> >
		    <?= $desc_array[$i]; ?>
		    <div>
			<br>
			<img src="assets/images/play.png" width="100px" onclick="document.getElementById('radioPanel<?= $i ?>').play()">
			<img src="assets/images/pause.png" width="100px" onclick="document.getElementById('radioPanel<?= $i ?>').pause()">
			<br><br>
			<audio controls name="media<?= $i ?>" id="radioPanel<?= $i ?>" preload="none">
			    <source src="<?= $radio_urls[$i]; ?>" type="audio/mpeg">
			</audio>

		    </div>
		</div>
	    <?php endfor; ?>
	</div>
    </div>
</div>

<div class="audioBox">
    <h2>
        <?= lang('home_title') ?>
        <?= lang('home_title_audio') ?></span>
    </h2>
    <div class="pure-g">
        <div class="pure-u-1 pure-u-lg-4-5">
	    <?php require_once("content.php"); ?>
	</div>
	<div class="pure-u-1 pure-u-lg-1-5">
	    <?php require_once("right.php"); ?>
	</div>
    </div>
</div>
