﻿<?php
class Content extends Admin_Controller {
  public function __construct() {
    parent::__construct();
  }

  public function index() {
	  Template::render("blank");
  }

  public function photoset_list() {
    header('Content-Type: application/json');
    $photo_sets = new Photos();
    $photo_sets->select('id', 'name');
    $photo_sets->get();
    $list_of_photo_sets = array();
    foreach($photo_sets as $photo_set) {
      $a_set = array();
      print_r($photo_set);
      $list_of_photo_sets[] = $a_set;
    }
    echo json_encode(array("photosets" => $list_of_photo_sets));
  }
}
