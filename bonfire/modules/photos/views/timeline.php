<!DOCTYPE html>
<html>
  <head>
    <title>Tipitaka Timeline</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css">
    <style type="text/css">
    ul.horizontal-slide {
        margin: 0;
        padding: 0;
        width: 100%;
        white-space: nowrap;
        overflow-x: auto;
    }

    ul.horizontal-slide li[class*="span"] {
        display: inline-block;
        float: none;
    }

    ul.horizontal-slide li[class*="span"]:first-child {
        margin-left: 0;
    }
    
    img#pic {
      display: block;
      margin-left: auto;
      margin-right: auto;
    }
    
    body { background: black; }
    
    div#container {
      display:block;
    }
  </style>
  </head>
  <body>
    <div id="container">
        <img data-bind="attr: { src: mainPicUrl }" id="pic">
    </div>
<!-- http://jsfiddle.net/harianus/Bn4BE/     -->
    <ul class="horizontal-slide" data-bind="foreach: thumbnails">
        <li class="span2">
            <a href="#" class="thumbnail">
              <img data-bind="attr:{src: url}, click: $parent.foo" alt="" />
            </a>
        </li>
    </ul>
        
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/knockout/2.3.0/knockout-min.js"></script>
    <script>
      var imgList = ['img_1002.jpg', 'img_1003.jpg', 'img_1004.jpg', 'img_1005.jpg', 'img_1006.jpg',
          'img_1007.jpg', 'img_1008.jpg', 'img_1009.jpg', 'img_1010.jpg', 'img_1011.jpg', 
          'img_1012.jpg', 'img_1013.jpg', 'img_1014.jpg', 'img_1015.jpg', 'img_1016.jpg', 
          'img_1017.jpg', 'img_1018.jpg', 'img_1019.jpg', 'img_1020.jpg', 'img_1021.jpg', 
          'img_1022.jpg', 'img_1023.jpg', 'img_1024.jpg', 'img_1025.jpg', 'img_1026.jpg', 
          'img_1027.jpg', 'img_1028.jpg', 'img_1029.jpg', 'img_1030.jpg', 'img_1031.jpg', 
          'img_1032.jpg', 'img_1033.jpg', 'img_1034.jpg'];
      var prefix="http://122.154.30.202:8079/watnafiles/tipitaka_timeline/";
      function Model() {
        var self = this;        
        this.thumbnails = imgList.map(function(img) {
          return {url: prefix + "small_" + img};
        });
        this.idx = ko.observable(0);
        this.mainPicUrl = ko.computed(function() {
          return prefix + imgList[self.idx()];
        });
        this.foo = function(data, event) {
          var context = ko.contextFor(event.target);
          self.idx(context.$index());
        };
      };
      
      var model = new Model();
      
      var $window = $(window);
      
      function setSize() { 
        var div = $("div#container");
        var pic = $("img#pic");
        
        var div_h = $window.height() - 200;
        var div_w = $window.width();
        
        var pic_h = pic.height();
        var pic_w = pic.width();
        
        div.height(div_h);
        
        if(div_w / div_h < pic_w / pic_h) {
          pic.width(div_w);
        } else {
          pic.height(div_h);
        }
        // model.height();
      }
      
      $window.resize(setSize);                
      $window.ready(setSize);                
      
      ko.applyBindings(model);
      </script>
    
  </body>
</html>
