<?php
require_once(FCPATH.'/convert_to_thumb.php');
class Content extends Admin_Controller {
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		$this->auth->restrict('Permissions.Audi-file.Manage');
		Template::set('toolbar_title', 'Youtube links');	
    Assets::add_css("v");
	}

	function index() {
		Template::render();
	}

  function save() {
    $id = $_POST["id"];
    if ($id) 
      $live = new YoutubeLive($id);
    else
      $live = new YoutubeLive();
    $live->start_broadcast = $_POST["start_broadcast"];
    $live->link = $_POST["link"];
    $live->video_length = $_POST["video_length"];
    echo $live->save();
  }

  function all() {
    $lives = new YoutubeLive();
    $live_array = array();
    foreach($lives->order_by('id','desc')->get() as $live) {
      $live_array[] = array("start_broadcast" => $live->start_broadcast,
                            "video_length" => $live->video_length,
                            "id" => $live->id,
                            "link" => $live->link);
    }
    header('Content-type: application/json');
    echo json_encode($live_array);
  }

  function delete() {
    $live = new YoutubeLive($_POST["id"]);
    $live->delete();
    echo "DONE";
  }

	function del()
	{
		$file = $this->uri->segment(5);
		$med = new Media($file);
		$med->delete();
		redirect('admin/content/files');		
	}

}
