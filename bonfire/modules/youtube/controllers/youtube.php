<?php
class Youtube extends Front_Controller {
    private function get_info() 
    {
        date_default_timezone_set('Asia/Bangkok');
        $lives = new YoutubeLive();
        $live = null;
        
        $i_plus = new DateInterval("PT24H");
        $t2_ = new DateTime();
        $t2_->add($i_plus);
        $t2 = $t2_->format("Y-m-d H:i:s");
        
        $t1 = new DateTime();
        

        foreach($lives->where("start_broadcast < ", $t2)
                      ->order_by("start_broadcast", "asc")
                      ->get() as $live_) {
            
            if (isset($live_->video_length)) {
                $len = $live_->video_length;
            } else {
                $len = 300;
            }
            $spec = "PT".($len + 10)."M";
            $i_len = new DateInterval($spec);
            $t0_ = new DateTime();
            $t0_->sub($i_len);
            $t0 = $t0_->format("Y-m-d H:i:s");
            
            if ($live_->start_broadcast >= $t0) {
                $live = array("link" => $live_->link, 
                "start_broadcast" => $live_->start_broadcast, 
                "video_length" => $live_->video_length);
                break;
            }
        }
        return $live;        
    }

	function index() {
        $live = $this->get_info();
        header('Content-type: application/json');
        echo json_encode($live);
	}

    function live() {
        $live = $this->get_info();
        if ($live) 
            header("Location: ". $live["link"]);
        else
            header("Location: ". YOUTUBE_LIVE_URL);
        die();
    }
}

