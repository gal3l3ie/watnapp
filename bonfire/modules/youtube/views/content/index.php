<div class="view split-view">
  <div id="content" class="view">
    <div class="scrollable">
    <br>
    <form class="constrained" onsubmit="return false">
      <p>ID: <span id="video_id"></span>
      <p>วันที่ถ่ายทอดสด <input type="text" id="datepicker"></p>
      <p>เวลาที่ถ่ายทอดสด 
        <select id="hour" class="time_selector">
          <?php for ($i = 0; $i < 23; $i++): ?>
            <option value="<?= $i; ?>"><?= $i; ?></option>
          <?php endfor; ?>
        </select>
        :
        <select id="minute" class="time_selector">
          <?php for ($i = 0; $i < 60; $i+=10): ?>
            <option value="<?= $i; ?>"><?= $i; ?></option>
          <?php endfor; ?>
        </select>
      </p>
      <p>
        ความยาว:
        <select id="len_hour" class="time_selector">
          <?php for ($i = 0; $i < 23; $i++): ?>
            <option value="<?= $i; ?>"><?= $i; ?></option>
          <?php endfor; ?>
        </select> ชั่วโมง
        :
        <select id="len_minute" class="time_selector">
          <?php for ($i = 0; $i < 60; $i+=10): ?>
            <option value="<?= $i; ?>"><?= $i; ?></option>
          <?php endfor; ?>
        </select> นาที
      </p>
      <p>URL: <input type="text" id="link"></p>
      <button id="save">บันทึก</button>
      <button id="reset">ล้าง</button>
    </form>

    <br>

    <ul id="list">
      
    </li>
    <script>

      var prefix = "/admin/content/youtube";
    
      function reset() {
        var start_broadcastElement = document.querySelector("input#datepicker");
        var linkElement = document.querySelector("input#link");
        var hourElement = document.querySelector("select#hour");
        var minuteElement = document.querySelector("select#minute");
        var lenHourElement = document.querySelector("select#len_hour");
        var lenMinuteElement = document.querySelector("select#len_minute");
        var idSpan = document.querySelector("span#video_id");

        start_broadcastElement.value = "";
        linkElement.value = "";
        hourElement.value = 0;
        minuteElement.value = 0;
        lenHourElement.value = 0;
        lenMinuteElement.value = 0;
        idSpan.innerHTML = "";
      };
      
      document.querySelector("button#reset").onclick = reset;

      function fetchList() {
        $.get(prefix + "/all", function(data) {
          var ul = document.querySelector("ul#list");
          ul.innerHTML = "";
          for(var i = 0; i < data.length; i++) {
            var row = data[i];
            var li = document.createElement("li");
            var len_hour = row.video_length ? Math.floor(row.video_length/60)   : 0;
            var len_minute = row.video_length ? row.video_length % 60 : 0;
            var text = document.createTextNode("ID: " + row.id + 
              " |  วันเวลาเริ่มออกอากาศ: " + row.start_broadcast + 
              " | ความยาว: " + len_hour + ":" + len_minute + 
              " | URL:");
            var a = document.createElement("a");
            a.href = row.link;
            a.appendChild(document.createTextNode(row.link));

            var editButton = document.createElement("button");
            editButton.appendChild(document.createTextNode("แก้ไข"));
            (function(row_) {
              editButton.onclick = function() {
                var start_broadcastElement = document.querySelector("input#datepicker");
                var linkElement = document.querySelector("input#link");
                var hourElement = document.querySelector("select#hour");
                var minuteElement = document.querySelector("select#minute");
                var lenHourElement = document.querySelector("select#len_hour");
                var lenMinuteElement = document.querySelector("select#len_minute");
                var idSpan = document.querySelector("span#video_id");
            
                start_broadcastElement.value = row_.start_broadcast.split(/\s+/)[0];
                linkElement.value = row_.link;
                hourElement.value = row_.start_broadcast.split(/\s+/)[1].split(/:/)[0];
                minuteElement.value = row_.start_broadcast.split(/\s+/)[1].split(/:/)[1];
                lenHourElement.value = Math.floor(row_.video_length / 60);
                lenMinuteElement.value = row_.video_length % 60;
                
                idSpan.innerHTML = "";
                idSpan.appendChild(document.createTextNode(row_.id));
              }
            })(row);

            var deleteButton = document.createElement("button");
            deleteButton.appendChild(document.createTextNode("ลบ"));
            (function(row_) {
              deleteButton.onclick = function() {
                if(confirm("ต้องการลบจริงหรือไม่")) {
                  $.post(prefix + "/delete", {id: row_.id}, function(data) {
                    fetchList();
                  });
                }
              }
            })(row);

            li.appendChild(text);
            li.appendChild(a);
            li.appendChild(document.createTextNode(" "));
            li.appendChild(editButton);
            li.appendChild(document.createTextNode(" "));
            li.appendChild(deleteButton);
            ul.appendChild(li);
          }
        });
      }

      $(function() {
        $("#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
      });

      fetchList();

      document.querySelector("button#save").onclick = function() {
        var start_broadcastElement = document.querySelector("input#datepicker");
        var linkElement = document.querySelector("input#link");
        var hourElement = document.querySelector("select#hour");
        var minuteElement = document.querySelector("select#minute");
        var lenHourElement = document.querySelector("select#len_hour");
        var lenMinuteElement = document.querySelector("select#len_minute");
        var idSpan = document.querySelector("span#video_id");

        var start_broadcast = start_broadcastElement.value + " " + 
          hourElement.value + ":" + minuteElement.value;
        console.log(parseInt(lenHourElement.value));
        console.log(parseInt(lenMinuteElement.value));
        var video_length = parseInt(lenHourElement.value) * 60 + parseInt(lenMinuteElement.value);
        console.log(video_length);
        var link = linkElement.value;
        var id = idSpan.textContent;

        if (!start_broadcast) {
          alert("โปรดป้อนเวลาถ่ายทอดสด");
          return;
        }

        if (!link) {
          alert("โปรดป้อนลิงค์");
          return;
        }
     
        $.post(prefix + "/save", {id: id, 
                                  link: link, 
                                  start_broadcast: start_broadcast,
                                  video_length: video_length})
          .done(function(data) {
            reset();
            fetchList();
          })
          .fail(function() {
            alert("ไม่สามารถบันึกได้");
          });
      }
    </script>
   
    </div>	<!-- /scrollable -->
  </div>	<!-- /content -->
</div>	<!-- /vsplit -->

