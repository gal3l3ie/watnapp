<?php 
class Live extends Front_Controller 
{

  function index()
  {
    redirect('home');
  }
   
  function streaming($id=null) 
  {   
    $this->load->library("session");
  
    include("geolite/geoip.inc");
    include("geolite/geoipcity.inc");

    $gi = geoip_open("geolite/GeoLiteCity.dat",GEOIP_STANDARD);
    
    if($id) {
      date_default_timezone_set('Asia/Bangkok');
      $time = date("G");
      $week = date ("w");
      $livemode = "livestream";

      if ($week == "6" && $time >= "20") {
        $imagestream = site_url("images/watnapp-sat24.jpg");
      } else if($week == "6" && $time >= "17") {
        $imagestream = site_url("images/watnapp-sat18.jpg");
      } else {
        $imagestream = site_url("images/watnapp-satall.jpg");
      }
        
      if ($week == "6" && $time >= "17"){
        //ทุกค่ำวันเสาร์ตั้งแต่ 18.00 น.
        $textstream = "ถ่ายทอดสดสนทนาธรรมค่ำวันเสาร์.";
        $livemode = "nightsat";
      } else if ($week != "6" && $time >= "17"){
        //ทุกวันยกเว้นวันเสาร์ตั้งแต่ 18.00 น.
        $textstream = "ถ่ายทอดสดประจำวัน";
      } else if ($time <= "4"){
        //ก่อน 04.00 น.
        $textstream = "ถ่ายทอดสดประจำวัน";
      } else{
        //นอกเหนือจากเวลาข้างต้น
        $textstream = "ถ่ายทอดสดประจำวัน (ช่วงเช้า - เย็น)";
      }
        
      //bitrate   
      if ($id == "low"){
        $bitrate = "livestreamlow";
        $width = "560";
        $height = "315";            
        $livemode = $bitrate;
      } else if ($id == "medium"){
        $bitrate = "livestream";
        $width = "640";
        $height = "360";
        $livemode = $bitrate;
      } else if($id == "high"){
        $bitrate = "livestreamHD";
        $width = "853";
        $height = "480";
        $livemode = $bitrate;           
      } else if ($id == 'liveaudio') {
        $bitrate = "livestreamaudio";
        $width = "560";
        $height = "349";            
        $livemode = $bitrate;
        $textstream .= " (เฉพาะเสียง)";
      } else if ($id == "mobile") {
        $bitrate = "mobile";
        $width = "640";
        $height = "390";
        $textstream = "ถ่ายทอดสดพุทธวจนบรรยาย นอกสถานที่";
        $imagestream = site_url("images/watnapp-mobile.jpg");
        $livemode = $bitrate;
      } else if ($id == "mobileaudio") {
        $bitrate = "mobileaudio";
        $width = "560";
        $height = "349";            
        $textstream = "ถ่ายทอดสดพุทธวจนบรรยาย นอกสถานที่ (เฉพาะเสียง)";
        $imagestream = site_url("images/watnapp-mobile.jpg");
        $livemode = $bitrate;
      } else {
        redirect('home');
      }

      //file ถ่ายทอดสด
      $filestream = $bitrate;
      
      //check ip to country;
      $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER["REMOTE_ADDR"];
      $record = GeoIP_record_by_addr($gi, $ip);

      $server = null;

      if (!$this->session->userdata("videoloc")) {
        if ($record && $record->country_name == "Thailand")
          $this->session->set_userdata("videoloc", "THA");
        else
          $this->session->set_userdata("videoloc", "WW");
      } 
      if ($this->session->userdata("videoloc") == "THA") {
        // Thailand
        $cname = $record != null ? $record->continent_code : 'AS';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://ec2.watnapp.com:1935/loadbalancer");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
        $redirect = substr(curl_exec($ch), 9);
        $server = (strlen($redirect) > 0 && $redirect != 'unknown') ? $redirect.":1935" : "live.watnapp.com:1935";
      } else {
        // Worldwide
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.watnapp.com/cloudfront.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server = json_decode(curl_exec($ch))->{'server'};
      }
      
      $filestream = $filestream.".stream";        
      $rtmpstream = "rtmp://".$server."/liveedge";                
      $hlsstream = "http://".$server."/liveedge/".$filestream."/playlist.m3u8";
      $mpdstream = "http://".$server."/liveedge/".$filestream."/manifest.mpd";
      $rtspstream = "rtsp://".$server."/liveedge/".$filestream;
        
      $audiostream = ($id == 'mobile') ? '/live/streaming/mobileaudio' : '/live/streaming/liveaudio';
      
      //....
      Template::set('idcheck',$id);
      Template::set('filestream',$filestream);
      Template::set('rtmpstream',$rtmpstream);
      Template::set('hlsstream',$hlsstream);
      Template::set('html5stream',$hlsstream);
      Template::set('mpdstream',$mpdstream);
      Template::set('rtspstream',$rtspstream);
      Template::set('livemode',$livemode);
      Template::set('width',$width);
      Template::set('height',$height);
      Template::set('textstream',$textstream);
      Template::set('imagestream',$imagestream);
      Template::set('audiostream',$audiostream);
      Template::set('videoloc', $this->session->userdata("videoloc"));
      Template::render();
    } else {
      redirect('home');
    }

    geoip_close($gi);    
  }

  function toggle_server()
  {
    $this->load->library('user_agent');
    $this->load->library("session");
    
    $alter_videoloc = ($this->session->userdata("videoloc") == "THA") ? "WW" : "THA";    
    $this->session->set_userdata('videoloc', $alter_videoloc);
    
    if (!$this->agent->is_referral()) {
      echo "DONE $alter_videoloc";
      return;
    }
    redirect($this->agent->referrer());
  }
    
  function realtime()
  {
    $livemode = $this->uri->segment(3);
    $domain = $this->uri->segment(4);
                
    Template::set('domain',$domain);
    Template::set('lmode',$livemode);
    Template::render('ajax');
  }

  function exstreaming($id=null)
  {
    include("geolite/geoip.inc");
    include("geolite/geoipcity.inc");

    $gi = geoip_open("geolite/GeoLiteCity.dat",GEOIP_STANDARD);
    
    if ($id) {
      date_default_timezone_set('Asia/Bangkok');
      $time = date("G");
      $week = date ("w");
      $livemode = "livestream";

      if ($week == "6" && $time >= "20") {
        $imagestream = site_url("images/watnapp-sat24.jpg");
      } else if($week == "6" && $time >= "17") {
        $imagestream = site_url("images/watnapp-sat18.jpg");
      } else {
        $imagestream = site_url("images/watnapp-satall.jpg");
      }
      
      if ($week == "6" && $time >= "17") {
        //ทุกค่ำวันเสาร์ตั้งแต่ 18.00 น.
        $textstream = "ถ่ายทอดสดสนทนาธรรมค่ำวันเสาร์.";
        $livemode = "nightsat";
      } else if ($week != "6" && $time >= "17") {
        //ทุกวันยกเว้นวันเสาร์ตั้งแต่ 18.00 น.
        $textstream = "ถ่ายทอดสดประจำวัน";
      } else if ($time <= "4") {
        //ก่อน 04.00 น.
        $textstream = "ถ่ายทอดสดประจำวัน";
      } else {
        //นอกเหนือจากเวลาข้างต้น
        $textstream = "ถ่ายทอดสดประจำวัน (ช่วงเช้า - เย็น)";
      }
      
      //bitrate   
      if ($id == "low") {
        $bitrate = "livestreamlow";
        $width = "560";
        $height = "349";
      } else if ($id == "medium") {
        $bitrate = "livestream";
        $width = "640";
        $height = "390";
        $livemode = $bitrate;
      } else if($id == "high") {
        $bitrate = "livestreamHD";
        $width = "853";
        $height = "510";
        $livemode = $bitrate;
      } else if ($id == "mobile") {
        $bitrate = "mobile";
        $width = "640";
        $height = "390";
        $textstream = "ถ่ายทอดสดพุทธวจนบรรยาย นอกสถานที่";
        $imagestream = site_url("images/watnapp-mobile.jpg");
        $livemode = $bitrate;
      } else {
        redirect('home');
      }

      //file ถ่ายทอดสด
      $filestream = $bitrate;
      
          //check ip to country;
      $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER["REMOTE_ADDR"];
      $record = GeoIP_record_by_addr($gi, $ip);
      $cname = $record != null ? $record->continent_code : 'AS';
      
      $server = null;
      if ($record != null && $record->country_name == "Thailand") {
        /*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://ec2.watnapp.com:1935/loadbalancer");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
        $redirect = substr(curl_exec($ch), 9);
        $server = (strlen($redirect) > 0 && $redirect != 'unknown') ? $redirect.":1935" : "live.watnapp.com:1935";
        */
        $server = "live.watnapp.com:1935";
      } else {
        /*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.watnapp.com/cloudfront.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server = json_decode(curl_exec($ch))->{'server'};
        */
        $server = "ec2.watnapp.com:1935";
      }
      
      $filestream = $filestream.".stream";                        
      $rtmpstream = "rtmp://".$server."/liveedge";
      $html5stream = "http://".$server."/liveedge/".$filestream."/playlist.m3u8";
      $rtspstream = "rtsp://".$server."/liveedge/".$filestream;
      
      //....
      Template::set('idcheck',$id);
      Template::set('filestream',$filestream);
      Template::set('rtmpstream',$rtmpstream);
      Template::set('html5stream',$html5stream);
      Template::set('rtspstream',$rtspstream);
      Template::set('livemode',$livemode);
      Template::set('width',$width);
      Template::set('height',$height);
      Template::set('textstream',$textstream);
      Template::set('imagestream',$imagestream);

      Template::render();
    } else {
      redirect('home');
    }
    geoip_close($gi);    
  }

}
