<div id="live">
  <div class="left_section_title">
    พุทธวจน : 
      <span class="des"><?= $textstream ?>
        <div id="iframestatus" class="float-left">
          <?php
          if($livemode == "mobile") {
            echo '<iframe width=66 height=28 src="http://live.watnapp.com/live/iframe.php?s=mobile&p=watnapp.com" id=status frameborder=0 scrolling=no></iframe>';
          } else {
            echo '<iframe width=66 height=28 src="http://live.watnapp.com/live/iframe.php?s=livestream&p=watnapp.com" id=status frameborder=0 scrolling=no></iframe>';
          }
          ?>
        </div>
      </span><!--cd-->
    <div class="clear"></div>

    <div class="paging-box float-right"></div><!--cpbfr-->

    <div class="clear"></div><!--cc-->
  </div><!--clst-->

  <?php if ($idcheck != 'liveaudio' && $idcheck != 'mobileaudio'): ?>
    <div id='cssmenu'>
      <ul>
        <li><a href="<?=$audiostream;?>"><span>ฟังเฉพาะเสียง</span></a></li>
        <li>
          <a href="/live/toggle_server">
              <span><?= $videoloc == "THA" ? lang("use_ww_server") : lang("use_tha_server"); ?>
              </span>
          </a>
        </li>
      </ul>
    </div>
  <?php endif; ?>

  <link href="<?= "/assets/wnplayer/video-js.css" ?>" rel="stylesheet" />
  <script src="<?= "/assets/wnplayer/video.js" ?>"></script>
  <script src="<?= "/assets/wnplayer/videojs-contrib-hls.js" ?>"></script>
  <script src="<?= "/assets/wnplayer/wnplayer.js" ?>"></script>

  <div id="stream_online_live">

      <div id='mediastream'></div>
      <script type="text/javascript">
       var player = new wnplayer('mediastream');
       player.setup({
           width:     '640',    
           height:    '360',
           autostart:  true,
           poster:    '<?= $imagestream; ?>',
           hls:     '<?= $hlsstream; ?>',
           mpd:       '<?= $mpdstream; ?>',   
           rtsp:      '<?= $rtspstream; ?>' ,
           playbutton:'/images/play_button.png'
       });

       /* jwplayer('mediastream').setup({
        *     'id': 'playerID',
        *     'autostart': 'true',
        *     'width': '<?= $width ?>',
        *     'height': '<?= $height ?>',
        *     'image': '<?= $imagestream; ?>',
        *     'stretching' : 'fill',
        *     'primary': 'flash',
        *     'sources':[
        *         {'file': '<?= $html5stream; ?>'},
        *     ]    
        * });*/
      </script>
  </div><!--cso-->
</div>
