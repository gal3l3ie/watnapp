<div class="view split-view">
	
	<div class="view">
	
		
		<div class="scrollable">
			<div class="list-view" id="user-list">
				<div class="list-item" data-id="pdf">
					<?= img(Template::theme_url('images/issue.png')) ?>
					<p>
					<b>PDF</b>
					
					</p>
				</div>
				<div class="list-item" data-id="ppt">
					<?= img(Template::theme_url('images/issue.png')) ?>
					<p>
					<b>PPT</b>
					</p>
				</div>
				<div class="list-item" data-id="word">
					<?= img(Template::theme_url('images/issue.png')) ?>
					<p>
					<b>WORD</b>
					</p>
				</div>
				<div class="list-item" data-id="book">
					<?= img(Template::theme_url('images/issue.png')) ?>
					<p>
					<b>BOOK</b>
					</p>
				</div>
                				<div class="list-item" data-id="program">
					<?= img(Template::theme_url('images/issue.png')) ?>
					<p>
					<b>PROGRAM</b>
					</p>
				</div>
			</div>	<!-- /list -->
		</div>
		
		
	</div>	
	
	
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
				<?php if (false) : ?>
				<div class="notification attention">
				</div>
				<?php endif; ?>
			
			
				<?php echo form_open_multipart(site_url('admin/content/files/add_file'), 'class="constrained"'); ?>

				<div>
					<br/>
					<?php
						echo form_label('TYPE','type');
						echo form_radio('type', 'pdf', TRUE).'PDF';
						echo form_radio('type', 'word').'WORD';
						echo form_radio('type', 'ppt').'PPT';
						echo form_radio('type', 'book').'BOOK';
						echo form_radio('type', 'program').'PROGRAM';

						echo br();
						echo form_label('Name','name');
						echo form_input('name');
						echo br();
						echo form_label('Link','link');
						echo form_input('link');
						echo br();
						echo form_label('Upload','file');
						echo form_upload('file');
						echo br();
						echo form_label('Picture','pic');
						echo form_upload('pic');
						echo "PHOTO SIZE 470 x 647 px";
					?>
											   
					
					<p class="small indent"></p>
				</div>
			
				<div class="submits">
					<br/>
					<input type="submit" name="submit" value="Upload" />
				</div>
			
			<?php echo form_close(); ?>
			
			
			
			<div class="notification information">
				<p>รายละเอียด</p>
			</div>
			
		
			<div class="row" style="margin-top: 3em">
				<!-- Access Logs -->
				<div class="column size1of2">
<ul class="clean">
<?php 
					foreach ($medias as $m)

	{
		echo '<li><span class="small">'.img(array('src'=>$m->pic,'style'=>'width:128px;height:175px;'));
		if(empty($m->link))
		{
			echo anchor('media/'.$m->file,$m->name);
		}
		else
		{
			echo anchor($m->link,$m->name);
		}
		echo anchor('admin/content/files/edit/'.$m->id,' [แก้ไข]');
		echo anchor('admin/content/files/del/'.$m->id,' [ลบ]').'</span></li>';
		
	}
	
	?>
</ul>				


</div>
				
				<!-- Login Attempts -->
				<div class="column size1of2 last-column">
					
					
						<ol>
						</ol>
					
				
				</div>
			</div>
		</div>	<!-- /scrollable -->
	</div>	<!-- /content -->
</div>	<!-- /vsplit -->

