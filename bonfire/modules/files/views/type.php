<div id="downloadBox">

	<div>
		<?php
		echo anchor('files/type/book','BOOK')." | ";
		echo anchor('files/type/PDF','PDF')." | ";
		echo anchor('files/type/ppt','PPT')." | ";
		echo anchor('files/type/word','WORD')." | ";
		echo anchor('files/type/program','PROGRAM');
		?>
	</div>
	<div>
		<ul>
			<?php foreach ($medias as $m): ?>
				<?php $mlink = $m->link; ?>
				<li class="colItem">
					<?php if (empty($m->pic)): ?>
						<a href="<?=$mlink;?>"><img class="download" src="/media/images/noimg.jpg"></a>
					<?php else: ?>
						<a href="<?=$mlink;?>"><img class="download" src="<?=$m->pic;?>"></a>
					<?php endif; ?>
					
					<?php if ($m->type == "pdf"): ?>
						<img src="/media/images/pdf.png">
					<?php elseif ($m->type == "ppt"): ?>
						<img src="/media/images/ppt.png">
					<?php elseif ($m->type == "word"): ?>
						<img src="/media/images/word.png">
					<?php elseif ($m->type == "book"): ?>
						<img src="/media/images/book.png">
					<?php endif; ?>

					<br>
					
					<a href="<?= $mlink; ?>"><?=$m->name;?></a>

					<?php if (!empty($m->created)): ?>
						<?= " ".ShowNewIcon($m->created); ?>
					<?php endif; ?>

					<br>
					
					<?php if (!empty($m->dmyupdate)): ?>
						<?= " ".ShowUpIcon($m->dmyupdate); ?>
					<?php endif; ?>

					<br>
					
					ดาวน์โหลด : <?= $m->count; ?>

				</li>				
			<?php endforeach; ?>
		</ul>
		<div class="clear"></div>
	</div>
</div>
