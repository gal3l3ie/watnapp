<?php
require_once(FCPATH.'/convert_to_thumb.php');
class Content extends Admin_Controller {
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		
		$this->auth->restrict('Permissions.Audi-file.Manage');

		Template::set('toolbar_title', 'System Files');	
		
		Assets::add_js($this->load->view('content/files_js', null, true), 'inline');
				
	}
	function index()
	{
		$medias = new Media();
		
		Template::set('medias',$medias->order_by('id','desc')->get());
		Template::render();
	}
	function view()
	{
		
		$file = $this->uri->segment(5);
		
		$medias = new Media();
		Template::set('medias',$medias->where('type',$file)->order_by('id','desc')->get());
		
		Template::set('files_id',$file);
		Template::render();
	}
	function del()
	{
		$file = $this->uri->segment(5);
		$med = new Media($file);
		$med->delete();
		redirect('admin/content/files');		
	}

	function add_file()
	{
		$id = $this->uri->segment(5);

		if($this->input->post('submit'))
		{
			$me = new Media($id);
			$me->from_array($_POST);
			$config['upload_path'] = './media/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '30000000';
			$this->load->library('upload', $config);
	
			$this->load->library('cloud');

			if($this->upload->do_upload('file'))
			{
				$d = $this->upload->data();
				$me->file = $d['file_name'];
                $public_url = $this->cloud->add_media($me->file, $_POST['name']);
                $me->link = $public_url;
                unlink(FCPATH.'/media/'.$me->file);
			}
			
			$config['upload_path'] = './media/cover/';
			$this->upload->initialize($config);
			if($this->upload->do_upload('pic'))
			{
				$d = $this->upload->data();
				$me->pic = $this->cloud->add_cover($d['file_name'], $_POST['name']);
                convert_to_thumb(FCPATH.'/media/cover/'.$d['file_name'], 
                    FCPATH.'/media/smallcover/'.$d['file_name'], 175, 128);
                $me->smallpic = $this->cloud->add_small_cover($d['file_name'], $_POST['name']);
                unlink(FCPATH.'/media/cover'.$d['file_name']);
                unlink(FCPATH.'/media/smallcover'.$d['file_name']);
			}
			
			$me->dmyupdate =date('Y-m-d H:i:s');

			if($me->save())
			{
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/files');
			}
			else 
			{
				Template::set_message('There was a problem uploading file.');
			}
		}
		Template::render();

	}
	public function edit() 
	{
		$id = $this->uri->segment(5);
		
		$medias = new Media();
		Template::set('medias',$medias->where('id',$id)->get());
		Template::set('m',$medias);

		
		/*
		if ($this->input->post('submit'))
		{
			
		}
		*/
		Template::render();
	}
}
