<?php
class Files extends Front_Controller {
	function index()
	{
		$medias = new Media();
		
		Template::set('medias',$medias->where("visibility","show")->order_by('id','desc')->get());
		Template::render();
	}
	function type($file = NULL)
	{
		$medias = new Media();
		Template::set('medias',$medias->where("visibility","show")->where('type',$file)->order_by('id','desc')->get());
		
		Template::set('files_id',$file);
		Template::render();
	}


}

