﻿<?php
class Content extends Admin_Controller {
	public function __construct()
	{
				date_default_timezone_set('Asia/Bangkok');		
		parent::__construct();
		
		Template::set('toolbar_title', 'Landfund');
		Template::set('editor_id', $this->session->userdata('user_id'));

		$this->auth->restrict('Landfund.Content.Manage');
		
		$this->lang->load('activities');
		$this->lang->load('datatable');
		Assets::add_module_js('landfund', 'jquery.dataTables.min.js');
		Assets::add_module_css('landfund', 'datatable.css');				
	}
	function index()
	{
		Assets::add_js($this->load->view('content/files_js', null, true), 'inline');
		$funds = new Fundho();
		Template::set('last_five',$funds->order_by('id','desc')->get(10));
		$funda = new Fundho();	
		Template::set('fid',$funda->count());
		$fundb = new Fundho();
		Template::set('mmore',$fundb->select_sum('m_more')->get());
		$fundc = new Fundho();
		Template::set('fmmore',$fundc->select_sum('fm_more')->get());
		Template::render();
	}
	
	function add ()
	{
		/*
		$this->auth->restrict('Landfund.Content.Create');
		Template::render();
		*/
	}
	
	function date_jsondata ()
	{
		Template::render("blank");
	}
	
	public function create() 
	{
		$this->auth->restrict('Landfund.Content.Create');
		Template::render();
	}
	
	function prints ()
	{
		/*
		$this->auth->restrict('Landfund.Content.Print');
		$subprints = $this->uri->segment(5);

		$funds = new Fundho();
		Template::set('check',$subprints);
		
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/content/landfund/prints/'.$subprints);
		$config['total_rows'] = $funds->count();
		$config['per_page'] = 20; 
		$config['uri_segment'] = 6;

		$this->pagination->initialize($config);
		//Template::set('fundlist',$fun->order_by('id','desc')->get($config['per_page'],$this->uri->segment(3)));

	if ($subprints == "host")
		{
			//Template::set('funds',$funds->order_by('id','desc')->get());
			Template::set('funds',$funds->order_by('id','desc')->get($config['per_page'],$this->uri->segment(6)));
			
		}
		else if($subprints == "hostco")
		{
			Template::set('funds',$funds->order_by('id','desc')->get($config['per_page'],$this->uri->segment(6)));

		}		
		else if($subprints == "exportbank")
		{
			Template::set('funds',$funds->where('ch','ที่มูลนิธิฯ')->or_where('ch','ที่วัดนาป่าพง')->order_by('id','desc')->get($config['per_page'],$this->uri->segment(6)));
		}
		else if($subprints == "reportbank")
		{
			$this->auth->restrict('Landfund.Content.Database');

			Template::set('funds',$funds->where('ch','ที่มูลนิธิฯ')->or_where('ch','ที่วัดนาป่าพง')->order_by('id','desc')->get($config['per_page'],$this->uri->segment(6)));
		}
		else if($subprints == "reportother")
		{
			$this->auth->restrict('Landfund.Content.Database');

			Template::set('funds',$funds->where('ch','ที่ตู้ ATM')->or_where('ch','โอนผ่านธนาคาร')->or_where('ch','ทาง Internet')->or_where('ch',',อื่นๆ(โปรดระบุ)')->order_by('id','desc')->get($config['per_page'],$this->uri->segment(6)));
		}
		else
		{
			$this->auth->restrict('Landfund.Content.Database');

			Template::set('funds',$funds->order_by('id','desc')->get($config['per_page'],$this->uri->segment(6)));
		}

		Template::render();
		*/
	}
	
	function reports ()
	{
		/*
		$this->auth->restrict('Landfund.Content.Print');
		Template::render();
		*/
	}
	
	
	function edit() 
	{
		$this->auth->restrict('Landfund.Content.Edit');

		$id = $this->uri->segment(5);
		
		$funds = new Fundho();
		Template::set('funds',$funds->where('id',$id)->get());
		Template::set('f',$funds);
		
		
		/*
		if ($this->input->post('submit'))
		{
			
		}
		*/
		Template::render();
	}
	function view()
	{
		
		$this->auth->restrict('Landfund.Content.View');
/*
		$funds = new Fundho();
		$this->load->library('pagination');
		$config['base_url'] = site_url('admin/content/landfund/view/');
		$config['total_rows'] = $funds->count();
		$config['per_page'] = 20; 
		$config['uri_segment'] = 5;

		$this->pagination->initialize($config);
		
		Template::set('funds',$funds->order_by('id','desc')->get($config['per_page'],$this->uri->segment(5)));
		Template::render();
		*/
		
	}
	function del()
	{
		/*
		$file = $this->uri->segment(5);
		$fun = new Fundho($file);
		$fun->delete();
		redirect('admin/content/landfund');		
		*/
	}
	
	function add_printall()
	{
		$today = date('Y-m-d');
		$action = $this->input->post('action');
		if($this->input->post('checked')){
			foreach ($this->input->post('checked') as $c)
			{
				$fu = new Fundho($c);
				if ($action == "1"){
					$fu->dep = "1";
					$fu->dep_dated = $today;
					$fu->save();
				} else if ($action == "0") {
					$fu->dep = "0";
					$fu->dep_dated = "0000-00-00";
					$fu->save();
				}			
			}
		} else {
			Template::redirect('admin/content/landfund/');
		}			
		Template::redirect('admin/content/landfund/select_print');
	}
	
	function add_file()
	{
		$id = $this->uri->segment(5);
		if($this->input->post('submit'))
		{
			$fu = new Fundho($id);
			$fu->from_array($_POST);
			/*
			$config['upload_path'] = './media/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '300000';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('file'))
			{
				$d = $this->upload->data();
				$fu->file = $d['file_name'];
			}
			
			$config['upload_path'] = './media/cover/';
			$this->upload->initialize($config);
			if($this->upload->do_upload('pic'))
			{
				$d = $this->upload->data();
				$fu->pic = $d['file_name'];
			}
			*/
						
			if($fu->save())
			{
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/landfund');
			}
			else 
			{
				Template::set_message('There was a problem uploading file.');
			}				
		}
		Template::render();
	}
	
	function prefix() 
	{
		/*
		$funds = new Fundho();
		Template::set('funds',$funds->where('id',$id)->get());
		Template::set('f',$funds);
		*/
		$this->auth->restrict('Landfund.Content.Create');

		$id = $this->uri->segment(5);
		if($this->input->post('submit'))
		{
			$pf = new Prefix($id);
			$pf->from_array($_POST);
						
			if($pf->save())
			{
				Template::set_message('File successfully created.', 'success');
				Template::redirect('admin/content/landfund');
			}
			else 
			{
				Template::set_message('There was a problem uploading file.');
			}
			
					
		}
		
		Template::render();
	}
	
	function printpage()
	{
				$this->auth->restrict('Landfund.Content.Print');
				$funds = new Fundho();
				$iddate = $this->input->post('tf_date');
				
				if ($iddate == ""){
					$today = date('Y-m-d');
				}else{
					$today =$iddate;	
				}
				
				$editor_id= $this->session->userdata('user_id');	
				
				if($this->session->userdata('user_id') == "1" || $this->session->userdata('user_id') == "5"){
					$editor_id= 13;
				}
				
				//Template::set('funds',$funds->where('dep','1')->where('dep_dated',$today)->order_by('id','asc')->get());
				Template::set('funds',$funds->where('dep','1')->where('dep_dated',$today)->where('poster',$editor_id)->where('cheq_no','')->order_by('id','asc')->get());
								
				$funds_count = new Fundho();
				Template::set('fc_count',$funds_count->where('dep','1')->where('dep_dated',$today)->where('poster',$editor_id)->where('cheq_no !=','')->order_by('id','asc')->count());
				
				$funds_c = new Fundho();
				Template::set('funds_c',$funds_c->where('dep','1')->where('dep_dated',$today)->where('poster',$editor_id)->where('cheq_no !=','')->order_by('id','asc')->get());

				Template::set('todays',$today);
				Template::render('printform');
				

	}
	
	function PrintByDate()
	{
		$this->auth->restrict('Landfund.Content.Print');
		$funds = new Fundho();
				
		$editor_id= $this->session->userdata('user_id');	
			
		if($this->session->userdata('user_id') == "1" || $this->session->userdata('user_id') == "5"){
			$editor_id= 13;
		}
			
		Template::set('activity_content',$funds->where('dep','1')->where('poster',$editor_id)->group_by('dep_dated')->get());
		Template::render();
	}
	
	function reportpage()
	{
		$this->auth->restrict('Landfund.Content.Report');
		$funds = new Fundho();
		$iddate = $this->input->post('tf_date');
		
		if ($iddate == "") {
			$today = date('Y-m-d');
		} else {
			$today = $iddate;	
		}
		
		$funda = new Fundho();	
		$rpfid = $this->uri->segment(5);
		$rp_per_page = 600; 
		Template::set('fid',$funda->count());
		$fundb = new Fundho();
		Template::set('mmore',$fundb->select_sum('m_more')->get());
		$fundc = new Fundho();
		Template::set('fmmore',$fundc->select_sum('fm_more')->get());

		Template::set('funds',$funds->order_by('id','asc')->get($rp_per_page, $rpfid));
		
		Template::set('page_this',floor($rpfid/30));
		Template::set('page_stop',$rp_per_page);
		Template::set('todays',$today);
		Template::render('reportform');			
	}
	
	function export_sql()
	{
		$this->auth->restrict('Landfund.Content.Database');
		// Load the DB utility class
		$today = date('Y-m-d');

		$this->load->dbutil();

		$prefs = array(
              	'tables'      => array('bf_fundhoes','bf_fundcoes','bf_prefixes'),  // Array of tables to backup.
              	'ignore'      => array(),           // List of tables to omit from the backup
              	'format'      => 'txt',             // gzip, zip, txt
              	'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
              	'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
              	'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
              	'newline'     => "\n"               // Newline character used in backup file
            );

		$backup =& $this->dbutil->backup($prefs);

		$this->load->helper('download');
		force_download('fundwatnapp_'.$today.'.sql',$backup);
	}
	
	
	function export_excel()
	{
		$funds = new Fundho();
		$editor_id= $this->session->userdata('user_id');
		$fid = $this->uri->segment(5);
		$per_page = 600;
		Template::set('exportexcel',$funds->order_by('id','asc')->get($per_page,$fid));
		Template::set('idstart',$fid+1);
		Template::set('idstop',$fid+$per_page);

		Template::render('blank');

	}
	
	function addprintpage()
	{
		Template::render('blank');

	}
	
	function conew()
	{
		Template::render('blank');
	}
	
	function test()
	{
		Template::render();	
	}
	
	function give_playlist()
	{
		Template::render();	
	}
	
	/*
	TEST
	*/
	
	public function all_print() 
	{
		if (has_permission('Landfund.Content.Print')) {
			return $this->_get_activity();
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function select_print() 
	{
		if (has_permission('Landfund.Content.Print')) {
			return $this->_get_activity('select_print');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}

	
	public function page_print() 
	{
		if (has_permission('Landfund.Content.Print')) {
			Template::redirect(SITE_AREA .'/content/landfund/printpage');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function report_print() 
	{
		$rpfid = $this->uri->segment(5);
		if (has_permission('Landfund.Content.Report')) {
			Template::redirect(SITE_AREA .'/content/landfund/reportpage/'.$rpfid);
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_export_sql() 
	{
		if (has_permission('Landfund.Content.Database')) {
			Template::redirect(SITE_AREA .'/content/landfund/export_sql');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_export_excel() 
	{
		$fid = $this->uri->segment(5);
		$per_page = 600;
		if (has_permission('Landfund.Content.Database')) {
			Template::redirect(SITE_AREA .'/content/landfund/export_excel/'.$fid);
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
		
	public function fund_ho() 
	{
		if (has_permission('Landfund.Content.Database')) {
			return $this->_get_activity('fund_ho');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_hoco() 
	{
		if (has_permission('Landfund.Content.Database')) {
			return $this->_get_activity('fund_hoco');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_edit() 
	{
		if (has_permission('Landfund.Content.Edit') && has_permission('Landfund.Content.Print')) {
			return $this->_get_activity('fund_edit');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_edit_order() 
	{
		if (has_permission('Landfund.Content.Edit') && has_permission('Landfund.Content.Print')) {
			return $this->_get_activity('fund_edit_order');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_edit_note() 
	{
		if (has_permission('Landfund.Content.Edit') && has_permission('Landfund.Content.Print')) {
			return $this->_get_activity('fund_edit_note');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	
	public function fund_edit_user() 
	{
		if (has_permission('Landfund.Content.Edit')) {
			return $this->_get_activity('fund_edit_user');
		}
		
		Template::set_message(lang('activity_restricted'), 'error');
		Template::redirect(SITE_AREA .'/content/landfund');
	}
	

	
	
	public function _get_activity($which='all_print',$find_value=FALSE)
	{	
		
		Assets::add_js($this->load->view('content/datatable_js', null, true), 'inline');
		
		switch ($which)
		{
			case 'select_print':
					$funds = new Fundho();
					$editor_id= $this->session->userdata('user_id');
					
				if($this->session->userdata('user_id') == "1" || $this->session->userdata('user_id') == "5"){
					$editor_id= 13;
				}
		//Template::set('activity_content',$funds->where('ch','ที่มูลนิธิฯ')->or_where('ch','ที่วัดนาป่าพง')->order_by('id','desc')->get());
		//1 TAI-NOI//Template::set('activity_content',$funds->where('poster','19')->or_where('poster','27')->order_by('id','desc')->get());
		Template::set('activity_content',$funds->where('poster',$editor_id)->order_by('id','desc')->get(600));
		//เรียกมาแค่ 600
		Template::set('check','select_print');

		break;
		
					case 'fund_ho':
					$funds = new Fundho();
		Template::set('activity_content',$funds->order_by('id','desc')->get());
		Template::set('check','fund_ho');

		break;
		
					case 'fund_hoco':
					$funds = new Fundho();
		Template::set('activity_content',$funds->order_by('id','desc')->get());
		Template::set('check','fund_hoco');

		break;
		
							case 'fund_edit':
					$funds = new Fundho();
		Template::set('activity_content',$funds->order_by('id','desc')->get());
		Template::set('check','fund_edit');

		break;
		
									case 'fund_edit_order':
					$funds = new Fundho();
					$fid = $this->uri->segment(5);
					$per_page = 600; 

		Template::set('activity_content',$funds->order_by('id','asc')->get($per_page, $fid));
		Template::set('check','fund_edit');

		break;
		
							case 'fund_edit_user':
					$funds = new Fundho();
		$editor_id= $this->session->userdata('user_id');

		Template::set('activity_content',$funds->where('poster',$editor_id)->order_by('id','desc')->get());
		Template::set('check','fund_edit');

		break;
		
									case 'fund_edit_note':
					$funds = new Fundho();

		Template::set('activity_content',$funds->where('note !=','')->order_by('id','desc')->get());
		Template::set('check','fund_edit_note');

		break;
		
			
			default:
		$funds = new Fundho();
		Template::set('activity_content',$funds->order_by('id','desc')->get());
		Template::set('check','A');
		break;
		}

		Template::set_view('content/view');
		Template::render();
	}
}
