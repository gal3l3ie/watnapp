<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'กองทุนบุญที่ดินและสิ่งปลูกสร้าง พุทธวจนสถาบัน วัดนาป่าพง',
	'author'		=> 'Watnapp Team',
	'name'			=> 'กองทุนบุญที่ดิน (watnapahpong.com)'
);