﻿<style>
.list-view .permission_set {cursor: pointer; border-bottom: 1px solid #CCCCCC; background: #F5F5F5; vertical-align: middle; }
.list-view .permission_set img { height: 10px; width: 10px; float: right; margin: 14px 14px 0 0; }
</style>


<div class="view split-view">
	
	<!-- Role List -->
	<div class="view">	
		<div class="scrollable">
			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        เจ้าภาพ
					</h4>
					
					<div class="list-item" data-id="create"> 
						<p>
							<b>เพิ่มข้อมูลเจ้าภาพใหม่</b><br/>
							<span class="small"></span>
						</p>
					</div>
                    <div class="list-item" data-id="prefix">
						<p>
							<b>เพิ่มคำนำหน้าชื่อ</b><br/>
							<span class="small"></span>
						</p>
					</div>
                    <div class="list-item" data-id="fund_edit_user">
						<p>
							<b>แก้ไขข้อมูลเจ้าภาพ (USER)</b><br/>
							<span class="small">เจ้าภาพของผู้ประสานงานที่กรอกข้อมูล</span>
						</p>
					</div>
                    <!--
                    <div class="list-item" data-id="fund_edit">
						<p>
							<b>แก้ไขข้อมูลเจ้าภาพ (ALL)</b><br/>
							<span class="small">เจ้าภาพของผู้ประสานงานทั้งหมด</span>
						</p>
					</div>
                       //-->


			</div>	<!-- /list-view -->
                        <?
					if (has_permission('Landfund.Content.Report')) {
			?>
            			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        เจ้าภาพ (แก้ไขข้อมูล)
					</h4>
                     <div class="list-item" data-id="fund_edit_note">
						<p>
							<b>แก้ไขข้อมูลหมายเหตุ (NOTE)</b><br/>
							<span class="small">ผู้ประสานงานแจ้งหมายเหตุเพิ่มเติม</span>
						</p>
					</div>
					<?php
							$fundeo = new Fundho();
							$f_count = $fundeo->count();
							$f_per_page = 600;
							$f_num_all = floor($f_count/$f_per_page);
							
							for($i=0;$i<=$f_num_all;$i++)
							{
								$f_start = $i*$f_per_page;
								$f_start_id = $f_start+1;
								$f_stop = $f_start+$f_per_page;

								echo '<div class="list-item" data-id="fund_edit_order/'.$f_start.'"><p><b>แก้ไขลำดับเจ้าภาพที่ '. $f_start_id .' - '. $f_stop .'</b><br/></p></div>';
}

					?>
					
                    
			</div>	<!-- /list-view -->
            <?
					}//P-DATABASE EDIT ALL
			?>
            <?
					if (has_permission('Landfund.Content.Print')) {
			?>
            			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        พิมพ์ (ธนาคาร)
					</h4>
					
					<div class="list-item" data-id="select_print">
						<p>
							<b>นำฝากธนาคาร (เฉพาะข้อมูลของ user นี้เท่านั้น)</b><br/>
							<span class="small">เลือกรายชื่อเจ้าภาพเพื่อพิมพ์ใบนำฝาก</span>
						</p>
					</div>
                    <div class="list-item" data-id="page_print">
						<p>
							<b>พิมพ์ใบนำฝากธนาคารของวันนี้</b><br/>
							<span class="small">พิมพ์ใบนำฝากที่เลือกไว้เฉพาะของวันนี้</span>
						</p>
					</div>
                        <div class="list-item" data-id="PrintByDate">
						<p>
							<b>พิมพ์ใบนำฝากธนาคารย้อนหลัง</b><br/>
							<span class="small">เลือกเพื่อพิมพ์ใบนำฝากจากวันที่ย้อนหลัง</span>
						</p>
					</div>
			</div>	<!-- /list-view -->
            <?
					}//p-print
			?>
             <?
					if (has_permission('Landfund.Content.Report')) {
			?>
            	<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                       พิมพ์ (รายงาน)
					</h4>
					
                    					<?php
							$fundrp = new Fundho();
							$f_count = $fundrp->count();
							$f_per_page = 600;
							$f_num_all = floor($f_count/$f_per_page);
							
							for($i=0;$i<=$f_num_all;$i++)
							{
								$f_start = $i*$f_per_page;
								$f_start_id = $f_start+1;
								$f_stop = $f_start+$f_per_page;

								echo '<div class="list-item" data-id="report_print/'.$f_start.'"><p><b>พิมพ์ลำดับเจ้าภาพที่ '. $f_start_id .' - '. $f_stop .'</b><br/></p></div>';
}

					?>
                    <!--
					<div class="list-item" data-id="report_print">
						<p>
							<b>รายชื่อเจ้าภาพหลัก</b><br/>
							<span class="small">แสดงรายชื่อเฉพาะเจ้าภาพหลักทั้งหมด</span>
						</p>
					</div>
                    <div class="list-item" data-id="fund_hoco">
						<p>
							<b>รายชื่อเจ้าภาพหลักและเจ้าภาพร่วม</b><br/>
							<span class="small">แสดงรายชื่อเจ้าภาพหลักและเจ้าภาพร่วมทั้งหมด</span>
						</p>
					</div>
                    -->
			</div>	<!-- /list-view -->
            <?
					}//p-report
			?>
            <?
					if (has_permission('Landfund.Content.Database')) {
			?>
            
                        			<div class="list-view" id="role-list">
					<h4 class="permission_set pointer">
						<img src="<?php echo Template::theme_url('images/plus.png') ?>" />	
                        ฐานข้อมูล
					</h4>
					
					<div class="list-item" data-id="fund_export_sql">
						<p>
							<b>ฐานข้อมูล SQL กองทุนที่ดิน</b><br/>
							<span class="small">สำรองข้อมูลเจ้าภาพหลัก,ร่วม,คำนำหน้าชื่อ</span>
						</p>
					</div>
                    
                    	<div class="list-item" data-id="">
						<p>
							<b>ฐานข้อมูล EXCEL กองทุนที่ดิน</b><br/>
							<span class="small">สำรองข้อมูลเจ้าภาพหลัก,ร่วม ตามลำดับด้านล่างนี้</span>
						</p>
					</div>
                                        					<?php
							$fundex = new Fundho();
							$f_count = $fundex->count();
							$f_per_page = 600;
							$f_num_all = floor($f_count/$f_per_page);
							
							for($i=0;$i<=$f_num_all;$i++)
							{
								$f_start = $i*$f_per_page;
								$f_start_id = $f_start+1;
								$f_stop = $f_start+$f_per_page;

								echo '<div class="list-item" data-id="fund_export_excel/'.$f_start.'"><p><b>บันทึกลำดับเจ้าภาพที่ '. $f_start_id .' - '. $f_stop .'</b><br/></p></div>';
}

					?>

			</div>	<!-- /list-view -->
            <?
					}//p-database
			?>
		</div>
	</div>
	
	<!-- Role Editor -->
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
				
			<div class="box create rounded">
				<a class="button good ajaxify" href="<?php echo site_url(SITE_AREA .'/content/landfund/create'); ?>">เพิ่มเจ้าภาพใหม่</a>

				<h3>โครงการ “ผ้าป่ากองทุนบุญที่ดินและสิ่งปลูกสร้างพุทธวจนสถาบัน” วัดนาป่าพง</h3>

				<p>(ระยะเวลาของโครงการ : ม.ค. 2555 – ม.ค. 2556)</p>
			</div>
			<br />
<?
$fund["more"] = "69000";
$fund["have"] = round( $mmore->m_more/3900);
$fund["empty"] = round($fund["more"]-$fund["have"]);

?>
			<h2>รายละเอียดกองทุนที่ดิน (อัพเดจอัตโนมัติ)</h2>
			<table>
				<thead>
				<th>ที่ตั้งไว้ (กอง)</th>
				<th>ที่รับแล้ว (กอง)</th>
				<th>ยังขาดอีก (กอง)</th>
				<th>ลำดับผู้บริจาค</th>
				</thead>
				<tbody>
					<tr>
						<td><? echo number_format($fund["more"])." กอง"; ?></td>
						<td><? echo number_format($fund["have"])." กอง"; ?></td>
						<td><? echo number_format($fund["empty"])." กอง"; ?></td>
						<td><? echo number_format($fid)." ลำดับ"; ?></td>
					</tr>
				</tbody>
			</table>
            <br />
            <br />
            <h2>ข้อมูลเจ้าภาพ 10 อันดับล่าสุด</h2>
            			<table>
				<thead>
				<th>ลำดับ</th>
				<th>ชื่อ-นามสกุล</th>
				<th>เงินบริจาค</th>
				<th>ช่องทางบริจาค</th>
				</thead>
				<tbody>
                <? 
				foreach ($last_five as $lf){
					echo "<tr>
						<td>".$lf->id."</td>
						<td>".$lf->pref."".$lf->fname." ".$lf->lname."</td>
						<td>".$lf->m_more."</td>
						<td>".$lf->ch."</td>
					</tr>";
				}
				?>
					
				</tbody>
			</table>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
