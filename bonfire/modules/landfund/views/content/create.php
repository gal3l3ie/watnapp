 <script>
  $(document).ready(function() {
    $("#tf_date").datepicker({ dateFormat: 'yy-mm-dd' });
  });
  </script>
  
<script language="JavaScript">
function chk(){

var a1=parseFloat(document.frm.m_more.value);
var a2=parseInt(a1/3900);
var a3=parseFloat(a2*3900);
var a4=parseFloat(a1-a3);

document.frm.f_more.value = a2;
document.frm.fm_more.value=a3;
document.frm.dfm_more.value=a4;//---- เปลี่ยนเอาจะ + - * /

}
</script>


				<?php echo form_open_multipart(site_url('admin/content/landfund/add_file'), 'name="frm" class="constrained"'); ?>

				<div>
                                <h2>เพิ่มรายชื่อเจ้าภาพ</h2><br />
					<br/>
                    
					<?php
					
						$data = array('name' => 'tf_date','id' => 'tf_date','value' => '');		
						echo form_label('วันที่ร่วมทำบุญ*','tf_date');
						echo form_input($data,'','id=tf_date READONLY');
						echo br();

								$prefixs = new Prefix;
								$options[]= "เลือกคำนำหน้าชื่อ";
								foreach ($prefixs->get() as $pre)
								{
									$options[$pre->name] = $pre->name;
								}

						echo form_label('คำนำหน้าชื่อ*','pref');
						echo form_dropdown('pref', $options, '1');
						//echo form_checkbox('family', 'และครอบครัว')."และครอบครัว";
						
						$fami = array ('และครอบครัว','และคณะ');
						foreach ($fami as $fam){
							
							echo form_radio('family', $fam);
							echo $fam;
						}

						echo br();
						
						$data = array('name' => 'fname', 'id' => 'fname', 'value' => '', 'maxlength' => '50');
						echo form_label('ชื่อ*','fname');
						echo form_input($data);
						echo br();
						
						$data = array('name' => 'lname', 'id' => 'lname', 'value' => '', 'maxlength' => '50');
						echo form_label('นามสกุล*','lname');
						echo form_input($data);
						echo br();
						
						$data = array('name' => 'phone', 'id' => 'phone', 'value' => '', 'maxlength' => '50');
						echo form_label('โทรศัพท์มือถือ*','phone');
						echo form_input($data);
						echo br();
						
						$data = array('name' => 'm_more', 'id' => 'm_more', 'value' => '', 'maxlength' => '10', 'style' => 'width:75px');
						$js = 'onKeyUp="chk()"';
						echo form_label('จำนวนเงินที่ร่วมบุญ*','m_more');
						echo form_input($data,'',$js);
						
						$data = array('name' => 'f_more', 'id' => 'f_more', 'value' => '', 'maxlength' => '10', 'style' => 'width:75px');
						$attri = array ('style' => 'width:85px');
						echo form_label('จำนวนกองทุน','f_more',$attri);
						echo form_input($data,'','READONLY');
						
						$data = array('name' => 'fm_more', 'id' => 'fm_more', 'value' => '', 'maxlength' => '10', 'style' => 'width:75px');
						$attri = array ('style' => 'width:70px');
						echo form_label('เงินกองทุน','fm_more',$attri);
						echo form_input($data,'','READONLY');
						echo br();
						
						$data = array('name' => 'dfm_more', 'id' => 'dfm_more', 'value' => '', 'maxlength' => '10');
						echo form_label('บริจาคตามกำลังศรัทธา','dfm_more');
						echo form_input($data,'','READONLY');
						echo br();

					$arr = array ('ที่มูลนิธิฯ','ที่ตู้ ATM','ทาง internet','ที่วัดนาป่าพง','โอนผ่านธนาคาร','อื่นๆ(โปรดระบุ)');
						echo form_label('ช่องทางการบริจาค','ch');
						foreach ($arr as $ar){
							
							echo form_radio('ch', $ar);
							echo $ar;
						}
						/*
						echo form_label('ช่องทางการบริจาค*','ch');
						echo form_radio('ch', '1').'ที่มูลนิธิฯ';
						echo form_radio('ch', '2').'ที่ตู้ ATM';
						echo form_radio('ch', '3').'ทาง INTERNET';
						echo form_radio('ch', '4').'ที่วัดนาป่าพง';
						echo form_radio('ch', '5').'ธนาคาร';
						echo form_radio('ch','6').'อื่น ๆ';
						*/
						echo br();

						$data = array('name' => 'ch_other', 'id' => 'ch_other', 'value' => '', 'maxlength' => '50');
						echo form_label ('ช่องทาง:ระบุเพิ่มเติม','ch_other');
						echo form_input($data);
						echo br();

/*Start Cheq_bank & Cheq_no*/
						$datachb = array('name' => 'cheq_bank', 'id' => 'cheq_bank', 'value' => '', 'maxlength' => '300', 'style' => 'width:200px');
						$datachn = array('name' => 'cheq_no', 'id' => 'co_no', 'value' => '', 'maxlength' => '100', 'style' => 'width:130px');
						echo form_label('เช็คธนาคาร/สาขา','cheq_bank');
						echo form_input($datachb);
						$attri = array ('style' => 'width:90px');
						echo form_label('หมายเลขเช็ค','cheq_no', $attri);
						echo form_input($datachn);
						echo br();
/*Star Co-Hosting-Phone 1-7*/

						echo br();
						$datah = array('name' => 'co_h1', 'id' => 'co_h1', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p1', 'id' => 'co_p1', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 1','co_h1');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p1', $attri);
						echo form_input($datap);
						echo br();
						
						$datah = array('name' => 'co_h2', 'id' => 'co_h2', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p2', 'id' => 'co_p2', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 2','co_h2');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p2', $attri);
						echo form_input($datap);
						echo br();
						
						$datah = array('name' => 'co_h3', 'id' => 'co_h3', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p3', 'id' => 'co_p3', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 3','co_h3');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p3', $attri);
						echo form_input($datap);
						echo br();
						
						$datah = array('name' => 'co_h4', 'id' => 'co_h4', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p4', 'id' => 'co_p4', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 4','co_h4');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p4', $attri);
						echo form_input($datap);
						echo br();
						
						$datah = array('name' => 'co_h5', 'id' => 'co_h5', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p5', 'id' => 'co_p5', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 5','co_h5');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p5', $attri);
						echo form_input($datap);
						echo br();
						
						$datah = array('name' => 'co_h6', 'id' => 'co_h6', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p6', 'id' => 'co_p6', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 6','co_h6');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p6', $attri);
						echo form_input($datap);
						echo br();
						
						$datah = array('name' => 'co_h7', 'id' => 'co_h7', 'value' => '', 'maxlength' => '50', 'style' => 'width:250px');
						$datap = array('name' => 'co_p7', 'id' => 'co_p7', 'value' => '', 'maxlength' => '10', 'style' => 'width:80px');
						echo form_label('เจ้าภาพร่วม 7','co_h7');
						echo form_input($datah);
						$attri = array ('style' => 'width:90px');
						echo form_label('เบอร์โทรศัพท์','co_p7', $attri);
						echo form_input($datap);
						echo br(2);


/*End Co-Hosting-Phone 1-7*/

						$data = array('name' => 'note', 'id' => 'note', 'value' => '', 'rows' => '3');
						echo form_label('หมายเหตุ','note');
						echo form_textarea($data);
						echo br();
						
						echo form_hidden('poster',$editor_id)	;
						/*
						echo form_hidden('dep','1')	;
						$today = date('Y-m-d');
						echo form_hidden('dep_dated',$today)	;
						*/
						
					?>
											   
					
					<p class="small indent"></p>
				</div>
			
				<div class="submits">
					<br/>
					<input type="submit" name="submit" value="SAVE" />
					<input class="button" type="button" name="cancel" value="CANCEL" onclick="window.location.reload(true);"/>
				</div>
			
			<?php echo form_close(); ?>