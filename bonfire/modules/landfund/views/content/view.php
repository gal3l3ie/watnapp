<style>
#flex_table { margin: 0; }
#flex_table th { font-weight: bold; }
#flex_table th.sorting_desc, #flex_table th.sorting_asc { background-color: #F5F5F5;}
</style>

<div class="scrollable" id="ajax-content">
  <script>
  $(document).ready(function() {
    $("#tf_date").datepicker({ dateFormat: 'yy-mm-dd' });
  });
  
  head.ready(function(){
	// Attach our check all function
	$(".check-all").click(function(){
		$("table input[type=checkbox]").attr('checked', $(this).is(':checked'));
	});
});
  </script>

	<div id="user_activities">
    <?
    if ($check == "select_print"){
  echo '<p><input class="check-all" type="checkbox" />เลือก/ไม่เลือก รายการทั้งหมด</p>';
	}
	?>

				<?php echo form_open_multipart(site_url('admin/content/landfund/add_printall'), 'name="frm" class="constrained"'); ?>
		<table id="flex_table">
			<thead>
				<tr>
					<th>ลำดับ</th>
					<th>ชื่อ-นามสกุล</th>
					<th>	<?php	
                    if($check == "fund_edit_note"){
						echo "หมายเหตุ";
					}else{
					echo "เงินบริจาค"; 
					}
					?>
                    </th>
					<th>ดำเนินการ</th>
				</tr>
			</thead>
			
			<tfoot>
            <?
                if ($check == "select_print"){
					?>
            <tr>
            	<td colspan="4">
                <font color="red">*การติ๊กถูกมีผลเฉพาะหน้าที่กำลังดำเนินการเท่านั้น</font><br />
					With selected: 
					
					<select name="action">
						<option value="1">สั่งพิมพ์รายการที่เลือก</option>
						<option value="0">ยกเลิกการสั่งพิมพ์รายการที่เลือก</option>
					</select> 
					&nbsp;&nbsp;
					<input type="submit" name="submit" value="ยืนยันทำรายการ" >
				</td>
			</tr>
            <?
				}//if s
				?>
            </tfoot>
			
			<tbody>
				<?php foreach ($activity_content as $activity) : ?>
				<tr>
					<td>	
                    <?
					if ($check == "select_print"){
                    echo '<input type="checkbox" value="'.$activity->id.'" name="checked[]" />';
					}
					?>
					<?php echo $activity->id; ?></td>
					<td>
					<?php 
					echo $activity->pref."".$activity->fname." ".$activity->lname; 
						if ($check == "fund_hoco"){
							echo br();
							if ($activity->co_h1 !=""){echo $activity->co_h1."<br>";}
							if ($activity->co_h2 !=""){echo $activity->co_h2."<br>";}
							if ($activity->co_h3 !=""){echo $activity->co_h3."<br>";}
							if ($activity->co_h4 !=""){echo $activity->co_h4."<br>";}
							if ($activity->co_h5 !=""){echo $activity->co_h5."<br>";}
							if ($activity->co_h6 !=""){echo $activity->co_h6."<br>";}
							if ($activity->co_h7 !=""){echo $activity->co_h7."<br>";}
						}
					?>
                    </td>
					<td >
					<?php 
					if($check == "fund_edit_note"){
						echo $activity->note;
					}else{
					echo number_format($activity->m_more); 
					}
					?>
                    </td>
					<td><?php 

					
					 	if ($check == "select_print"){
							/*
					$np = new user();
					$npt = $np->where('id',$activity->poster)->get();
					$nameposter = $npt->first_name." ".$npt->last_name;
					*/
			echo "<div class='digg-panel'>";
			/*
			if ($activity->poster == "19"){
			$nameposter = "(ต่าย)";	
			}else if ($activity->poster == "27"){
			$nameposter = "(น้อย)";	
			}else{
			$nameposter = "(-)";	
			}
			*/
			if ($activity->dep == "1"){
					echo "<span class='buttons' id='button_".$activity->id."'><a class='btn-printing' href='javascript: void(0)' title='".$activity->dep_dated."'></a></span>";
			}else{
					echo "<span class='buttons' id='button_".$activity->id."'><a class='btn-print' href='javascript: void(0)' title='".$activity->dep_dated."'></a></span>";
			}
			echo "</div>";
		}
		
		if ($check =="fund_edit"){
			echo '<a href="'.site_url(SITE_AREA .'/content/landfund/edit/'.$activity->id).'" class="ajaxify">[แก้ไข]</a>';
		}
		
		if ($check =="fund_edit_note"){
			echo '<a href="'.site_url(SITE_AREA .'/content/landfund/edit/'.$activity->id).'" class="ajaxify">[แก้ไข]</a>';
		}
					//echo date('M j, Y g:i A', strtotime($activity->created));
					?>
                    </td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
			<?php echo form_close(); ?>
            	</div>
</div>