<?php
$today = date('Y-m-d');
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=landfund_".$idstart."_".$idstop."_".$today.".xls");
header("Pragma: no-cache");
header("Expires: 0");
header("Content-Transfer-Encoding: binary ");

?>
<table width="1500" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="70" rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>ลำดับ</b><br />
    id</td>
    <td width="115" rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>วันที่ร่วมทำบุญ</b><br />
    tf_date      <br /></td>
    <td colspan="3" align="center" valign="top" bgcolor="#fde9d9"><b>เจ้าภาพหลัก</b></td>
    <td width="135" rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>เบอร์โทรศัพท์มือถือ</b><br />
    phone</td>
    <td width="150" rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>จำนวนเงินที่ร่วมทำบุญ</b><br />
    m_more</td>
    <td width="125" rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>ช่องทางการบริจาค</b><br />
    ch</td>
    <td width="124" rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>ช่องอื่น ๆ</b><br />
    (โปรดระบุ)</td>
    <td colspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>เจ้าภาพร่วม</b></td>
    <td rowspan="2" align="center" valign="top" bgcolor="#fde9d9"><b>หมายเหตุ</b><br />
      note</td>
  </tr>
  <tr>
    <td width="85" align="center" valign="top" bgcolor="#fde9d9"><b>คำนำหน้าชื่อ</b></td>
    <td width="135" align="center" valign="top" bgcolor="#fde9d9"><b>ชื่อ</b></td>
    <td width="135" align="center" valign="top" bgcolor="#fde9d9"><b>นามสกุล</b></td>
    <td width="220" align="center" valign="top" bgcolor="#fde9d9"><b>ชื่อ-นามสกุล</b></td>
    <td width="110" align="center" valign="top" bgcolor="#fde9d9"><b>เบอร์โทรศัพท์</b></td>
  </tr>
    <?
  foreach ($exportexcel as $ex){
	  echo '<tr>
    <td align="center" valign="middle">'.$ex->id.'</td>
    <td align="center" valign="middle">'.$ex->tf_date.'</td>
    <td align="center" valign="middle">'.$ex->pref.'</td>
    <td align="center" valign="middle">'.$ex->fname.'</td>
    <td align="center" valign="middle">'.$ex->lname.'</td>
    <td align="center" valign="middle">'.$ex->phone.'</td>
    <td align="center" valign="middle">'.$ex->m_more.'</td>
    <td align="center" valign="middle">'.$ex->ch.'</td>
    <td align="center" valign="middle">'.$ex->ch_other.'</td>
    <td align="center" valign="middle" bgcolor="#bfdbff"></td>
    <td align="center" valign="middle" bgcolor="#bfdbff"></td>
    <td align="center" valign="middle">'.$ex->note.'</td>
  </tr>';
   if ($ex->co_h1){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h1.'</td>
    <td align="center" valign="middle">'.$ex->co_p1.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
 if ($ex->co_h2){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h2.'</td>
    <td align="center" valign="middle">'.$ex->co_p2.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
    if ($ex->co_h3){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h3.'</td>
    <td align="center" valign="middle">'.$ex->co_p3.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
    if ($ex->co_h4){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h4.'</td>
    <td align="center" valign="middle">'.$ex->co_p4.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
    if ($ex->co_h5){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h5.'</td>
    <td align="center" valign="middle">'.$ex->co_p5.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
    if ($ex->co_h6){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h6.'</td>
    <td align="center" valign="middle">'.$ex->co_p6.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
    if ($ex->co_h7){
	      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$ex->co_h7.'</td>
    <td align="center" valign="middle">'.$ex->co_p7.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
  }
  	$fundco = new Fundco;
	$fco = $fundco->where('co_id',$ex->id)->get();
  	foreach ($fco as $fc){
		      echo '<td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle"></td>
    <td align="center" valign="middle">'.$fc->co_pref.''.$fc->co_fname.' '.$fc->co_lname.'</td>
    <td align="center" valign="middle">'.$fc->co_phone.'</td>
    <td align="center" valign="middle"></td>
  </tr>';
	}	
  
  
  }//end
  ?>
 
</table>
