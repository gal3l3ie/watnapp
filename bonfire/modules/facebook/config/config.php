<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Facebook news feeder',
	'author'		=> 'Vee Satayamas',
	'name'			=> 'Facebook'
);
