<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url('images/favicon.ico');?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-translate-customization" content="829439035d4f00cd-b8019ad38ab11319-g8828d509d0660252-14"></meta>
<meta name="Description" content="วัดนาป่าพง (พุทธวจนสถาบัน) พระอาจารย์คึกฤทธิ์ โสตถิผโล ร่วมกันมุ่งมั่นศึกษา ปฏิบัติ เผยแผ่คำของตถาคต"/>
<meta name="keywords" content="วัดนาป่าพง, พุทธวจน, พุทธวจนสถาบัน, วัด, พระพุทธเจ้า, พระอาจารย์คึกฤทธิ์  โสตถิผโล, คึกฤทธิ์ โสตถิผโล, สนทนาธรรม, มูลนิธิพุทธโฆษณ์, Buddhawajana, watnapp, watnapahpong"/>
<link rel="image_src" href="<?php echo site_url('images/logo-watnapp.jpg'); ?>" />
<title><?php echo config_item('site.title'); ?></title>
	<link href="<?php echo site_url('static/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
	<style>
	blockquote small:before {
		content: '-- \00A0';
	}
	</style>
	<script src="<?php echo site_url('static/jquery/jquery-1.9.0.js'); ?>"></script>

<?php  echo Assets::external_js('head.min.js'); ?>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33792423-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body style="background-color: #FFF; background-image: url('<?php echo site_url("static/image/bg.png"); ?>')">
<?php /* <div style="width: 960px; margin-left: auto; margin-right: auto; background-image: url('<?php echo $this->config->base_url("static/image/main_bg.png"); ?>');"> */ ?>
<div class="container" style="background-color: #fff">
	<div class="row">
		<div class="span12"><div><img src="<?php echo $this->config->site_url("static/image/banner980x200.jpg"); ?>" /></div></div>
	</div>
	<div class="row">
		<div class="span3">
        <style>
	#left-menu li { 
		height: 35px;
		font-size: 14px;
		text-align: center;
		line-height: 35px;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		color: #999;
		border: solid 1px #e6e6e6;
		background: #fff;
		margin-bottom: 5px;
	}

	#left-menu li a {
		height: 35px;
		color: #804040;
		font-size: 14px;
		text-align: center;
		line-height: 35px;
		text-decoration: none;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		cursor: pointer;
	}
	
	#left-menu li a:hover {
		height: 30px;
		padding-left: 0px;
		padding-right: 0px;
		margin-left: 0px;
		margin-right: 0px;
	}
	
	#left-menu li a:active { 
		color: #804040;
		background-color: #eee;
	}
</style>

<ul class="nav nav-list" id="left-menu">
	<li><a href="<?php echo site_url("wncontest"); ?>">หน้าหลัก</a></li>
	<li><a href="<?php echo site_url("wncontest/detail");?>">รายละเอียดโครงการ</a></li>
	<li><a href="<?php echo site_url("wncontest/schedule");?>">กำหนดการ</a></li>
	<li><a href="<?php echo site_url("wncontest/rule");?>">หลักเกณฑ์การประกวด</a></li>
	<li><a href="<?php echo site_url("wncontest/sutta");?>">พระสูตรที่ใช้ในการประกวด</a></li>
	<li><a href="<?php echo site_url("wncontest/register");?>">สมัครร่วมโครงการ</a></li>
	<li><a href="http://www.buddhakos.com/volunteer.php" target="_blank">อาสาสมัครช่วยงานโครงการ</a></li>
	<li><a href="<?php echo site_url("wncontest/donate");?>">สนับสนุนโครงการ</a></li>
	<li><a href="<?php echo site_url("wncontest/faq");?>">FAQ</a></li>
	<li><a href="<?php echo site_url("wncontest/example");?>">ตัวอย่างการท่องพุทธวจน</a></li>
	<li><a href="#">เข้าสู่ระบบ</a></li>
</ul>
        </div>
		<div class="span9">
        	<?php echo Template::message(); ?>
			<?php echo isset($content) ? $content : Template::_yield(); ?>
        </div>
	</div>
	<div class="row">
		<div class="span12" style="background-color:#FFC; text-align: center;">สงวนลิขสิทธ์ &copy; มูลนิธิพุทธโฆษณ์ 2013</div>
	</div>
</div>
	<script>
	head.js(<?php echo Assets::external_js(null, true) ?>);

</script>

</body>
</html>
