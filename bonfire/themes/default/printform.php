<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
* {
	margin:0;
	padding:0;
	font-family:Arial, "times New Roman", tahoma;
	font-size:12px;
}
html {
	font-family:Arial, "times New Roman", tahoma;
	font-size:12px;
	color:#000000;
}
body {
	font-family:Arial, "times New Roman", tahoma;
	font-size:12px;
	padding:0;
	margin:0;
	color:#000000;
}
.headTitle {
	font-size:15px;
	font-weight:bold;
	text-transform:uppercase;
}
.headerTitle01 {
	border:1px solid #333333;
	border-left:2px solid #000;
	border-bottom-width:2px;
	border-top-width:2px;
	font-size:11px;
}
.headerTitle01_r {
	border:1px solid #333333;
	border-left:2px solid #000;
	border-right:2px solid #000;
	border-bottom-width:2px;
	border-top-width:2px;
	font-size:11px;
}
/* สำหรับช่องกรอกข้อมูล  */
.box_data1 {
	font-family:Arial, "times New Roman", tahoma;
	height:18px;
	border:0px dotted #333333;
	border-bottom-width:1px;
}
/* กำหนดเส้นบรรทัดซ้าย  และด้านล่าง */
.left_bottom {
	border-left:2px solid #000;
	border-bottom:1px solid #000;
}
/* กำหนดเส้นบรรทัดซ้าย ขวา และด้านล่าง */
.left_right_bottom {
	border-left:2px solid #000;
	border-bottom:1px solid #000;
	border-right:2px solid #000;
}
/* สร้างช่องสี่เหลี่ยมสำหรับเช็คเลือก */
.chk_box {
	display:block;
	width:10px;
	height:10px;
	overflow:hidden;
	border:1px solid #000;
}
/* css ส่วนสำหรับการแบ่งหน้าข้อมูลสำหรับการพิมพ์ */
@media all
{
	.page-break	{ display:none; }
	.page-break-no{ display:none; }
}
@media print
{
	.page-break	{ display:block;height:1px; page-break-before:always; }
	.page-break-no{ display:block;height:1px; page-break-after:avoid; }	
}
</style>

</head>

<body>
<table width="750" border="0" align="center" cellspacing="0"  style="border-collapse:collapse;border-top:5px double #000;">
  <tr>
    <td><table width="100%" border="0" cellspacing="0">
      <tr>
        <td><img src="file:///C|/Users/Gal3l3ie/Pictures/watnapp/logo_header.png" width="350" height="80" /></td>
        <td align="center" class="headTitle">ใบนำฝากเงิน<br />
          (ตามรายชื่อ)</td>
        </tr>
    </table></td>
  </tr>
  <tr></tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="5">
      <tr>
        <td width="93"><img src="file:///C|/Users/Gal3l3ie/Pictures/watnapp/0002a8.png" width="93" height="33" /></td>
        <td>เพื่อเข้าบัญชี &quot;จัดซื้อและพัฒนาที่ดิน เพื่อ วัดนาป่าพง&quot; มูลนิธิพุทธโฆษณ์ <br />
          เลขที่บัญชี 318-3-01598-0 </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="750" border="0" cellspacing="0">
      <tr>
        <td width="50" height="25" align="center" valign="middle" class="headerTitle01">ลำดับ<br /></td>
        <td align="center" valign="middle" class="headerTitle01">ชื่อผู้บริจาค</td>
        <td align="center" valign="middle" class="headerTitle01_r">จำนวนเงิน</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">1</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">2</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">3</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">4</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">5</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">6</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">7</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">8</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">9</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">10</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">11</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">12</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">13</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">14</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">15</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">16</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">17</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">18</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">19</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td height="20" align="center" class="left_bottom">20</td>
        <td class="left_bottom">&nbsp;</td>
        <td class="left_right_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
        <td>รวม</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

			<?php echo Template::_yield(); ?>
