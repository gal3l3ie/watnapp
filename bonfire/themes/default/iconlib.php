<?php
function ShowNewIcon($strDate1)
{
    $strDate2 = date("Y-m-d H:i:s");
    $datediff = (strtotime($strDate2) - strtotime($strDate1)) / ( 60 * 60 * 24 );  // 1 day = 60*60*24
    if( $datediff < 15 ) {
        $exiconnew = img('media/images/new.gif');
    } else {
        $exiconnew = "";
    }
    return $exiconnew;
}

function ShowUpIcon($strDate1)
{
    $strDate2 = date("Y-m-d H:i:s");
    $datediff = (strtotime($strDate2) - strtotime($strDate1)) / ( 60 * 60 * 24 );  // 1 day = 60*60*24
    if( $datediff < 15 ) {
        $exiconup = img('media/images/update.gif');
    } else {
        $exiconup = "";
    }
    return $exiconup;
}
?>