<?php
	// Setup our default assets to load.
	
	Assets::add_js( array(
		base_url() .'assets/js/jquery-1.5.min.js',
	));
	Assets::add_css(array(base_url().'/css/stylex.css'));
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo config_item('site.title'); ?></title>

<?php //echo Assets::css('stylex.css'); ?>
<link href="/css/stylex.css" rel="stylesheet" type="text/css" />

<?php  echo Assets::external_js('head.min.js'); ?>
<?php echo Assets::external_js('jquery-1.6.2.min.js'); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15209266-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script language="JavaScript">
function winnew(URL,w,h,scrollbar)
{
   var winl = (screen.width-w)/2;
   var wint = (screen.height-h)/2;
   settings='height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scrollbar+',toolbar=0,location=0,status=0,menubar=0,resizable=0,dependent=0,copyhistory=0' 
   {
       MyPopUp = window.open(URL,"",settings)
   }
}


</script>

</head>

<body onLoad="winnew('<?= site_url('popup-pix.html'); ?>','555','500','yes');">
<div id="false_header"></div>
<div id="top_header"><div id="logo_header" class="float-left"><?php echo img('images/logo_header.png') ?></div>
<div id="searchForm" class="float-right">
    <script type="text/javascript">
  function read_book() {
	  
    var lang = document.getElementById('search_language');
    window.open('http://etipitaka.com/read?language='+lang.value+'&volume=1&number=0');
    return true;
  }
</script>

    <form accept-charset="UTF-8" action="http://etipitaka.com/search" method="post" target="_blank">
            <fieldset>
            <input name="search[keywords]" type="search" id="s" value="โปรแกรมตรวจหาและเทียบเคียงพุทธวจน" onFocus='if(this.value==this.defaultValue)this.value="";' onBlur='if(this.value=="")this.value="โปรแกรมตรวจหาและเทียบเคียงพุทธวจน";' />
             <input id="submitButton" name="commit" type="submit" value="ค้นหา" />
             <div id="searchInContainer">
               <input type="radio" name="search[language]" value="thai" id="search_language" checked />
               <label for="search_language">ไทย (บาลีสยามรัฐ)</label>
                
               <input type="radio" name="search[language]" value="pali" id="search_language" />
               <label for="search_language">บาลี (บาลีสยามรัฐ)</label>
                 <input name="utf8" type="hidden" value="✓" />
        <input name="authenticity_token" type="hidden" value="6hEIZh4IIiusadUQVtyuHICjAeh9d/x3A1mDh0R4up0=" />
                <input name="read" type="button" value="อ่านพระไตรปิฎก" id="read" onclick="read_book();"/>
	 </div>
                     </fieldset> 
    </form>
</div>
<div class="clear"></div>

<div id="menu-header" class="float-left">
<ul id="menu" name="menu">
<li><a href="<?php echo base_url()?>" <?php echo check_class('welcome') ?> >หน้าหลัก</a></li>
<li><a href="<?php echo site_url('live')?>" <?php echo check_class('live') ?>>ถ่ายทอดสด</a></li>
<li><a href="<?php echo site_url('video')?>" <?php echo check_class('video') ?>>สื่อวีดีโอ</a></li>
<li><a href="<?php echo site_url('audio')?>" <?php echo check_class('audio') ?> >สื่อเสียง</a></li>
<li><a href="<?php echo site_url('media')?>" <?php echo check_class('media') ?>>สื่อสิ่งพิมพ์</a></li>
<li><a href="<?php echo site_url('files')?>" <?php echo check_class('files') ?>>ศูนย์ดาวน์โหลด</a></li>

<li><a href="<?php echo site_url('contact')?>" <?php echo check_class('contact') ?>>ติดต่อเรา</a></li>
</ul>

</div>
<div id="socialbutton" class="float-right"><?php echo anchor('http://www.facebook.com/pages/พุทธวจน/114077818673776
', img('images/facebook.png'),'target="_blank"');?><?php echo anchor('http://www.youtube.com/nirdukkha
', img('images/youtube.png'),'target="_blank"');?></div>
<div class="clear"></div>
</div>
<div class="main">
	<div id="content_wraper">
	
	<?php echo Template::message(); ?>
	<?php echo isset($content) ? $content : Template::_yield(); ?>
</div>
</div>	<!-- /main -->
<div class="clear"></div>

<div id="footer" class="text-center"><?php echo img('images/footer.jpg');?></div>
<script>
	head.js(<?php echo Assets::external_js(null, true) ?>);
</script>
<script>var _wau = _wau || []; _wau.push(["tab", "0c5kql4nbjvr", "awt", "bottom-right"]);(function() { var s=document.createElement("script"); s.async=true; s.src="http://widgets.amung.us/tab.js";document.getElementsByTagName("head")[0].appendChild(s);})();</script>
<?php echo Assets::inline_js(); ?>
</body>
</html>
