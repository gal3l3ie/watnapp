<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url('images/favicon.ico'); ?>">
<meta name="google-translate-customization" content="829439035d4f00cd-b8019ad38ab11319-g8828d509d0660252-14"></meta>
<meta name="Description" content="วัดนาป่าพง (พุทธวจนสถาบัน) พระอาจารย์คึกฤทธิ์ โสตถิผโล ร่วมกันมุ่งมั่นศึกษา ปฏิบัติ เผยแผ่คำของตถาคต"/>
<meta name="keywords" content="วัดนาป่าพง, พุทธวจน, พุทธวจนสถาบัน, วัด, พระพุทธเจ้า, พระอาจารย์คึกฤทธิ์  โสตถิผโล, คึกฤทธิ์ โสตถิผโล, สนทนาธรรม, มูลนิธิพุทธโฆษณ์, Buddhawajana, watnapp, watnapahpong"/>
<link rel="image_src" href="<?php echo site_url('images/logo-watnapp.jpg'); ?>" />
<title><?php echo config_item('site.title'); ?></title>
