<div id="etipitaka">
	<script type="text/javascript">
	function read_book() {
		var lang = document.getElementById('search_language');
		window.open('http://etipitaka.com/read?language=' + lang.value + '&volume=1&number=0');
		return true;
	}
	</script>

	<form accept-charset="UTF-8" action="http://etipitaka.com/search"
		method="post" target="_blank" class="pure-form" id="etipitaka">
		<fieldset>
            <div>
			   <input type="radio" name="search[language]" value="thai" id="search_language" checked><label for="search_language">ไทย</label>
			   <input type="radio" name="search[language]" value="pali" id="search_language"><label for="search_language">บาลี</label>
			</div>
			<input name="search[keywords]" type="search" id="s" 
				value="โปรแกรมตรวจหาและเทียบเคียงพุทธวจน" 
				onFocus='if (this.value == this.defaultValue)
						 this.value = ""' 
				onBlur='if (this.value == "")
						this.value = "โปรแกรมตรวจหาและเทียบเคียงพุทธวจน"' />
			<input id="submitButton" name="commit" type="submit" value="ค้นหา" class="pure-button pure-button-primary"/>
			<input name="utf8" type="hidden" value="✓" />
			<input name="authenticity_token" type="hidden" value="6hEIZh4IIiusadUQVtyuHICjAeh9d/x3A1mDh0R4up0=" />
			<input name="read" type="button" value="อ่านพระไตรปิฎก" id="read" onclick="read_book();" class="pure-button pure-button-primary"/>
		</fieldset> 
	</form>
</div>
