<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="refresh" content="5; url=<? echo $refresh; ?>"/>
<title><?php echo $title; ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?= site_url('images/favicon.ico'); ?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="Description" content="วัดนาป่าพง (พุทธวจนสถาบัน) พระอาจารย์คึกฤทธิ์ โสตฺถิผโล ร่วมกันมุ่งมั่นศึกษา ปฏิบัติ เผยแผ่คำของตถาคต"/>

<meta name="keywords" content="วัดนาป่าพง, พุทธวจน, พุทธวจนสถาบัน, วัด, พระพุทธเจ้า, พระอาจารย์, พระอาจารย์คึกฤทธิ์, คึกฤทธิ์ โสตฺถิผโล, คึกฤทธิ์ โสตถิผโล, สนทนาธรรม, มูลนิธิพุทธโฆษณ์ "/>

<link rel="image_src" href="<?= site_url("images/logo-watnapp.jpg"); ?>" />
<link href="/css/stylex.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15209266-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<style type="text/css">
<!--
#redirectwrap

{
	background: #dedede;
	border: 1px solid #666;
	margin: 200px auto 0 auto;
	text-align: left;
	width: 500px;
}



#redirectwrap h4

{
	color: #CCC;
	font-size: 14px;
	margin: 0;
	padding: 5px;
	background-color: #333;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-top-color: #333;
	border-bottom-color: #333;
}



#redirectwrap p

{

	margin: 0;

	padding: 5px;

}



#redirectwrap p.redirectfoot

{
	text-align: center;
	background-color: #FFF;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #333;
}



-->
</style>
</head>
<body>

	<?php echo isset($content) ? $content : Template::_yield(); ?>
 
 <script id="_wauskr">var _wau = _wau || [];
_wau.push(["colored", "0c5kql4nbjvr", "skr", "ffc20e000000"]);
(function() {var s=document.createElement("script"); s.async=true;
s.src="http://widgets.amung.us/colored.js";
document.getElementsByTagName("head")[0].appendChild(s);
})();</script>
</body>
</html>
