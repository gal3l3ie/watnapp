<div class="pure-g">
	<div class="pure-u-1 pure-u-md-6-24">
		<img src="/images/logo_header.png" id="logo">
	</div>
	<div class="pure-u-1 pure-u-md-14-24 lg-hide">
		<?php require_once("search.php"); ?>
	</div>
	<div class="pure-u-1 pure-u-md-4-24">
		<?php require_once("social.php"); ?>
	</div>
</div>
