<!-- CSS -->
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">		
<!--[if lte IE 8]>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
<![endif]-->
<!--[if gt IE 8]><!-->
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">
<!--<![endif]-->
<link rel="stylesheet" type="text/css" href="<?= site_url("assets/css/reveal$min.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?= site_url("assets/css/watna.css"); ?>"> 
<!-- END CSS -->
