<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
	<title><?php echo config_item('site.title'); ?></title>
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url('images/favicon.ico');?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-translate-customization" content="829439035d4f00cd-b8019ad38ab11319-g8828d509d0660252-14"></meta>
<meta name="Description" content="วัดนาป่าพง (พุทธวจนสถาบัน) พระอาจารย์คึกฤทธิ์ โสตถิผโล ร่วมกันมุ่งมั่นศึกษา ปฏิบัติ เผยแผ่คำของตถาคต"/>
<meta name="keywords" content="วัดนาป่าพง, พุทธวจน, พุทธวจนสถาบัน, วัด, พระพุทธเจ้า, พระอาจารย์คึกฤทธิ์  โสตถิผโล, คึกฤทธิ์ โสตถิผโล, สนทนาธรรม, มูลนิธิพุทธโฆษณ์, Buddhawajana, watnapp, watnapahpong"/>
<link rel="image_src" href="<?php echo site_url('images/logo-watnapp.jpg'); ?>" />
	<meta name="robots" content="index, follow" />
	<meta name="viewport" content="width=768px, minimum-scale=1.0, maximum-scale=1.0" />
   	<link href="<?php echo site_url('/assets/ipad/css/reset.css') ?>" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?php echo site_url('/assets/ipad/css/style.css') ?>" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?php echo site_url('/assets/ipad/css/procarousel.css') ?>" media="screen" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo site_url('/assets/ipad/css/light.css') ?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo site_url('/assets/ipad/css/nivo-slider.css') ?>" type="text/css" media="screen" />

    <script type="text/javascript" src="<?php echo site_url('/assets/ipad/js/jquery-1.7.2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('/assets/ipad/js/jquery.jcarousel.min.js') ?>"></script>
    <script type='text/javascript' src="<?php echo site_url('/jwplayer/jwplayer.js') ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('/assets/ipad/js/jquery.nivo.slider.js') ?>"></script>

	<script type="text/javascript">
		jQuery(document).ready(function() {
    		jQuery('#myyoutube').jcarousel({
        	});
		});
		
		$(window).load(function() {
        	$('#slider').nivoSlider({
			animSpeed: 500, // Slide transition speed
        	pauseTime: 10000,						
			});
    	});
	</script>
    
    
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33792423-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?
if (isset($livemode)){
	$lmode = $livemode; 
	}else{
	$lmode =0;
	}
?>

<script>
$(document).ready(function() {
   <?
   //google analytics realtime
if($lmode == "livestream" || $lmode == "nightsat" || $lmode == "mobile"){
	$domain = str_replace('www.', '', $_SERVER['HTTP_HOST']);
echo "
 	 $('#statusrealtime').html('<iframe width=66 height=28 src=\"http://media.watnapahpong.org/live/status.php?s=".$lmode."&p=".$domain."\" id=test frameborder=0 scrolling=no></iframe>');
   var refreshId = setInterval(function() {
 	 $('#statusrealtime').html('<iframe width=66 height=28 src=\"http://media.watnapahpong.org/live/status.php?s=".$lmode."&p=".$domain."\" id=test frameborder=0 scrolling=no></iframe>');
   }, 280000);";
}
?>

   $.ajaxSetup({type: "POST",cache: false });
});
</script>
</head>
<body>
	<?php echo Template::_yield(); ?>
</body>
</html>
