<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo config_item('site.title'); ?></title>
<link href="<?php echo site_url('/assets/site/css/reset.css') ?>" media="all" rel="stylesheet" type="text/css"/>
<link href="<?php echo site_url('/assets/site/css/style.css') ?>" media="all" rel="stylesheet" type="text/css"/>
<script language="javascript" src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background: no-repeat;
	background-position:top;
	background-color: #0E3828;
	background-image: url('/assets/site/background/bg<? echo  rand(1, 14);?>.jpg');
}
-->
</style>
</head>

<body >
	<?php echo Template::_yield(); ?>
</body>
</html>
