<!doctype html>
<html lang="en">
	<?php $min = ENVIRONMENT == "production" ? "-min" : ""; ?>	
    <head><?php require_once("head.php"); ?></head>
	<body>
		<?php require_once("mainmenu.php"); ?>
		<?php require_once("header.php"); ?>
		<?php require_once("iconlib.php"); ?>

		<div id="content_wraper"> 
			<?php echo Template::message(); ?>
			<?php echo isset($content) ? $content : Template::_yield(); ?>
		</div>
		<?php require_once("footer.php"); ?>
	</body>
</html>

      