<style>
</style>
<div class="custom-wrapper pure-g" id="menu">
	<div class="pure-u-1 pure-u-lg-1-5">
		<div class="pure-menu">
			<a href="/" class="pure-menu-heading custom-brand"><?= config_item('site.title'); ?></a>
			<a href="#" class="custom-toggle" id="toggle"><s class="bar"></s><s class="bar"></s></a>
		</div>
	</div>
	<div class="pure-u-1 pure-u-lg-3-5">
		<div class="pure-menu pure-menu-horizontal custom-can-transform">
			<ul class="pure-menu-list">
			    <li class="pure-menu-item"><a class="pure-menu-link" href="<?= site_url('audio') ?>"    ><?= lang('home_menu_audio') ?></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="<?= site_url('book') ?>"     ><?= lang('home_menu_book') ?></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="<?= site_url('video') ?>"     ><?= lang('home_menu_video') ?></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="<?= site_url('files') ?>"     ><?= lang('home_menu_files') ?></a></li>
			    <li class="pure-menu-item"><a class="pure-menu-link" href="http://webboard.watnapp.com" ><?= lang('home_menu_webboard') ?></a></li>
			    <li class="pure-menu-item"><a class="pure-menu-link" href="http://faq.watnapp.com"      ><?= lang('home_menu_faqs') ?></a></li>
			    <li class="pure-menu-item"><a class="pure-menu-link" href="<?= site_url('contact') ?>"  ><?= lang('home_menu_contact') ?></a></li>
			</ul>
		</div>
	</div>
	<div class="pure-u-1 pure-u-lg-1-5">
		<div class="pure-menu pure-menu-horizontal custom-menu-3 custom-can-transform">
			<ul class="pure-menu-list">
				<li class="pure-menu-item"><a href="/language/th" class="pure-menu-link">ไทย</a></li>
				<li class="pure-menu-item"><a href="/language/en" class="pure-menu-link">English</a></li>
			</ul>
		</div>
	</div>
</div>

<script>
(function (window, document) {
	var menu = document.getElementById('menu'),
		WINDOW_CHANGE_EVENT = ('onorientationchange' in window) ? 'orientationchange':'resize';

	function toggleHorizontal() {
		[].forEach.call(
			document.getElementById('menu').querySelectorAll('.custom-can-transform'),
			function(el){
				el.classList.toggle('pure-menu-horizontal');
			}
		);
	};

	function toggleMenu() {
		console.log("TOGGLE");
		// set timeout so that the panel has a chance to roll up
		// before the menu switches states
		if (menu.classList.contains('open')) {
			setTimeout(toggleHorizontal, 500);
		}
		else {
			toggleHorizontal();
		}
		menu.classList.toggle('open');
		document.getElementById('toggle').classList.toggle('x');
	};

	function closeMenu() {
		if (menu.classList.contains('open')) {
			toggleMenu();
		}
	}

	document.getElementById('toggle').addEventListener('click', function (e) {
		toggleMenu();
	});

	window.addEventListener(WINDOW_CHANGE_EVENT, closeMenu);
})(this, this.document);

</script>
