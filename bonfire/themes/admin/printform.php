<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>กองทุนบุญที่ดินฯ:ใบนำฝากธนาคาร:<? echo $todays; ?></title>
<style type="text/css">
* {
	margin:0;
	padding:0;
font-family:AngsanaUPC, "Angsana New";
	font-size:16pt;
}
html {
	font-family:AngsanaUPC, "Angsana New";
	font-size:16pt;
	color:#000000;
}
body {
	font-family:AngsanaUPC, "Angsana New";
	font-size:16pt;
	padding:0;
	margin:0;
	color:#000000;
}
.headTitle {
	font-size:16pt;
	font-weight:bold;
	text-transform:uppercase;
}
.headerTitle01 {
	border:1px solid #333333;
	border-left:2px solid #000;
	border-bottom-width:2px;
	border-top-width:2px;
	font-size:16pt;
}
.headerTitle01_r {
	border:1px solid #333333;
	border-left:2px solid #000;
	border-right:2px solid #000;
	border-bottom-width:2px;
	border-top-width:2px;
	font-size:16pt;
}
/* สำหรับช่องกรอกข้อมูล  */
.box_data1 {
font-family:AngsanaUPC, "Angsana New";
	font-size:16pt;
	border:0px dotted #333333;
	border-bottom-width:1px;
}
/* กำหนดเส้นบรรทัดซ้าย  และด้านล่าง */
.left_bottom {
	border-left:2px solid #000;
	border-bottom:1px solid #000;
}
/* กำหนดเส้นบรรทัดซ้าย ขวา และด้านล่าง */
.left_right_bottom {
	border-left:2px solid #000;
	border-bottom:1px solid #000;
	border-right:2px solid #000;
}
/* สร้างช่องสี่เหลี่ยมสำหรับเช็คเลือก */
.chk_box {
	display:block;
	width:10px;
	height:10px;
	overflow:hidden;
	border:1px solid #000;
}
/* css ส่วนสำหรับการแบ่งหน้าข้อมูลสำหรับการพิมพ์ */
@media all
{
	.page-break	{ display:none; }
	.page-break-no{ display:none; }
}
@media print
{
	.page-break	{ display:block;height:1px; page-break-before:always; }
	.page-break-no{ display:block;height:1px; page-break-after:avoid; }	
}
</style>

</head>

<body>
    			<?php echo Template::_yield(); ?>
</body>
</html>

