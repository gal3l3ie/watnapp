<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH."libraries/opencloud/php-opencloud.php");

class Cloud {
	protected static $ci;

	public function __construct()
	{
		self::$ci =& get_instance();
		self::init();
	}

	public static function init()
	{
	}

    public static function connect() {
        $mysecret = array(
            'username' => CLOUD_USERNAME,
            'apiKey' => CLOUD_API_KEY
        );

        $conn = new \OpenCloud\Rackspace(RACKSPACE_US, $mysecret);
        $conn->SetTimeouts(60, 60, 60);
        return $conn;
    }

    public static function add_file_general($container, $path_prefix, $file_path, $name) {
        $name = trim($name);
        $name = preg_replace("/[\"'\\.\\s\\^\\/]+/u", "_", $name);
        $name = rand(1000,9999).'-'.$name."-".date("Ymdhis");
        $conn = &self::connect();
        $ostore = $conn->ObjectStore('cloudFiles', 'DFW', 'publicURL');
        $container = $ostore->Container($container);
        try {
            $obj_ = $container->DataObject($name);
            $obj_->Delete();
        } catch(OpenCloud\Base\Exceptions\ObjFetchError $e) {
        }
        $myMedia = $container->DataObject();
        $myMedia->Create(array('name' => $name),
                               FCPATH.'/'.$path_prefix.'/'.$file_path);
        $publicURL = $myMedia->PublicURL();
        return $publicURL;
    }

    public static function add_media($file_path, $name) {
        return self::add_file_general('watnapp_media', 'media', $file_path, $name);
    }

    public static function add_cover($file_path, $name) {
        return self::add_file_general('watnapp_cover', 'media/cover', $file_path, $name);
    }

    public static function add_small_cover($file_path, $name) {
        return self::add_file_general('watnapp_small_cover', 
            'media/smallcover', $file_path, $name);
    }

    public static function add_album_cover($file_path, $name) {
        return self::add_file_general('watnapp_album_cover', 
            'media/album/cover', $file_path, $name);
    }

    public static function add_small_album_cover($file_path, $name) {
        return self::add_file_general('watnapp_small_album_cover', 
            'media/album/smallcover', $file_path, $name);
    }

    public static function add_audio($file_path, $name) 
    {
        return self::add_file_general('watnapp_media_audio', 
            'media/audio', $file_path, $name);
    }

    public static function add_slider($file_path, $name) 
    {
        return self::add_file_general('watnapp_media_slider', 
            'media/slider', $file_path, $name);
    }
}
