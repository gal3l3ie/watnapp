<?php 
class Categoryb extends DataMapper {
	var $has_many = array('book');
	var $validation = array(
			'name' => array(
				'label' => 'ชื่อของหมวด'
			)
	);
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}
}