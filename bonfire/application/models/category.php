<?php 
class Category extends DataMapper {
	var $has_many = array('song','media','tag');
	var $validation = array(
			'name' => array(
				'label' => 'ชื่อของหมวด'
			)
	);
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}
}